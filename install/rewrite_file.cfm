<cfoutput>

	<cfset project_name_dot = proj.name & "."/>
	<cfif multiple_client>
		<cfset project_name_dot = proj.name & ".global."/>
	</cfif>
	<!--- rename project-name in index.cfm and application.cfc --->
	<cffile action="read" file="#proj_loc#/index.cfm" variable="_index"/>
	<cffile action="read" file="#proj_loc#/Application.cfc" variable="_app"/>
	<cffile action="read" file="#proj_loc#/login.cfm" variable="_login"/>
	<cffile action="read" file="#proj_loc#/awaf/config.cfm" variable="_config"/>
	<!---cffile action="read" file="#proj_loc#/awaf/global_function.cfm" variable="_gfunc"/--->
	<cfset _index = replaceNoCase(_index, "{{project-name}}", project_name_dot, "all")/>
	<cfset _login = replaceNoCase(_login, "{{project-name}}", project_name_dot, "all")/>
	<cfset _config = replaceNoCase(_config, "{{project-name}}", project_name_dot, "all")/>
	<cfset _index = replaceNoCase(_index, "{{project-desc}}", proj.desc, "all")/>
	<cfset _login = replaceNoCase(_login, "{{project-desc}}", proj.desc, "all")/>
	<cfset _config = replaceNoCase(_config, "{{project-dsn}}", proj.dsn, "all")/>
	<cfset _app = replaceNoCase(_app, "{{project-name}}", project_name_dot, "all")/>
	<cfset _app = replaceNoCase(_app, "/awaf/source/", "awaf/", "all")/>
	<cfset _app = replaceNoCase(_app, "../source/", "awaf/", "all")/>
	<cfset _app = replaceNoCase(_app, "../template/", project_name_dot, "all")/>
	<cfset _app = replaceNoCase(_app, "/template/", "/" & project_name_dot & "/", "all")/>
	<cfset _app = replaceNoCase(_app, "awaf.source", project_name_dot & ".awaf", "all")/>

	<cffile action="write" file="#proj_loc#/index.cfm" output="#_index#"/>
	<cffile action="write" file="#proj_loc#/login.cfm" output="#_login#"/>
	<cffile action="write" file="#proj_loc#/Application.cfc" output="#_app#"/>
	<cffile action="write" file="#proj_loc#/awaf/config.cfm" output="#_config#"/>
	<li><code>index.cfm</code> re-write....</li>
	<li><code>Application.cfc</code> re-write....</li>
	<li><code>config.cfm</code> re-write....</li>
	<li><code>global_function.cfm</code> re-write....</li>

	<!--- re-write controllers and model files "template."--->
	<cfset file_loc = "
		Application.cfc,
		awaf/com/Controller.cfc,
		controllers/settings/Role.cfc,
		controllers/settings/RolePermission.cfc,
		controllers/settings/User.cfc,
		controllers/settings/UserLogin.cfc,
		controllers/host/Module.cfc,
		controllers/host/Page.cfc,
		controllers/Home.cfc,
		models/settings/Role.cfc,
		models/settings/RolePermission.cfc,
		models/settings/User.cfc,
		models/settings/UserLogin.cfc,
		models/settings/UserRole.cfc,
		awaf/global_function.cfm,
		awaf/config.cfm,
		models/host/Model.cfc,
		models/host/ModelField.cfc,
		models/host/ModelX.cfc,
		models/host/Module.cfc,
		models/host/Page.cfc,
		settings.cfm
	"/>

	<cfloop array="#proj.clients#" item="_client">
		<cfset client_loc = proj.root & _client & "/"/>
		<cfset directoryCopy("#dir_loc#awaf/client-template/", "#client_loc#/", true)/>
		<li>copy #dir_loc#awaf/client-template/ to #client_loc#/</li>
	</cfloop>

	<cfloop List="#file_loc#" item="file_name">

		<cfset file_name = trim(file_name)/>
		<cffile action="read" file="#proj_loc#/#file_name#" variable="_v_cont"/>
		<cfset _v_cont = replaceNoCase(_v_cont, "template.", project_name_dot, "all")/>
		<cffile action="write" file="#proj_loc#/#file_name#" output="#_v_cont#"/>
		<li><code>global/#file_name#</code> re-write....</li>

		<!--- rewrite the clients folders --->
		<cfif !FindNoCase("awaf/com", file_name)>
			<ul>
				<cfloop array="#proj.clients#" item="_client">
					<cfset client_loc = proj.root & _client & "/"/>
					<cffile action="read" file="#client_loc#/#file_name#" variable="_v_cont"/>
					<cfset _v_cont = replaceNoCase(_v_cont, "{{project-name}}", proj.name, "all")/>
					<cfset _v_cont = replaceNoCase(_v_cont, "{{client-name}}", _client, "all")/>
					<cfset _v_cont = trim(_v_cont)/>
					<cffile action="write" file="#client_loc#/#file_name#" output="#_v_cont#"/>
					<li><code>#_client#/#file_name#</code> re-write....</li>
				</cfloop>
			</ul>
		</cfif>


	</cfloop>

</cfoutput>
