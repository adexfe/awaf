
<!--- create tables --->
<cfif structKeyExists(proj, "dsn")>
	<cfquery datasource="#proj.dsn#">
		CREATE TABLE IF NOT EXISTS `model` (
		  `ModelId` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
		  `ModuleId` tinyint(3) unsigned NOT NULL,
		  `Name` varchar(30) NOT NULL DEFAULT '',
		  PRIMARY KEY (`ModelId`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	</cfquery>
	<cfquery datasource="#proj.dsn#">
		CREATE TABLE IF NOT EXISTS `model_field` (
		  `ModelFieldId` int(10) unsigned NOT NULL AUTO_INCREMENT,
		  `ModelId` tinyint(3) unsigned NOT NULL,
		  `Name` varchar(255) NOT NULL DEFAULT '',
		  `Type` varchar(10) NOT NULL DEFAULT 'text',
		  `Alias` varchar(100) DEFAULT NULL,
		  `Display` varchar(10) NOT NULL DEFAULT 'Single',
		  PRIMARY KEY (`ModelFieldId`),
		  KEY `ModelId` (`ModelId`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	</cfquery>
	<cfquery datasource="#proj.dsn#">
		CREATE TABLE IF NOT EXISTS `module` (
		  `ModuleId` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
		  `Name` varchar(30) NOT NULL DEFAULT '',
		  `Icon` varchar(30) CHARACTER SET ascii DEFAULT NULL,
		  `Position` tinyint(4) NOT NULL,
		  `URL` varchar(100) DEFAULT NULL,
		  `ViewBy` enum('Client','Staff') NOT NULL DEFAULT 'Staff',
		  `MenuBar` enum('Yes','No') NOT NULL DEFAULT 'Yes',
		  `ForcePageReload` enum('Yes','No') DEFAULT 'No',
		  PRIMARY KEY (`ModuleId`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	</cfquery>
	<cfquery datasource="#proj.dsn#">
		CREATE TABLE IF NOT EXISTS `page` (
		  `PageId` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
		  `ModuleId` tinyint(4) unsigned DEFAULT NULL,
		  `Name` varchar(100) NOT NULL DEFAULT '',
		  `URL` varchar(100) DEFAULT '',
		  `Position` tinyint(4) DEFAULT NULL,
		  `ShowInMenu` enum('Yes','No') CHARACTER SET ascii NOT NULL DEFAULT 'No',
		  `Icon` varchar(25) CHARACTER SET ascii DEFAULT NULL,
		  PRIMARY KEY (`PageId`),
		  KEY `ModuleId` (`ModuleId`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	</cfquery>
	<cfquery datasource="#proj.dsn#">
		CREATE TABLE IF NOT EXISTS `role` (
		  `RoleId` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
		  `Name` varchar(30) CHARACTER SET ascii NOT NULL DEFAULT '',
		  `Type` enum('Staff','Client') NOT NULL DEFAULT 'Staff',
		  `Hierarchy` tinyint(3) unsigned DEFAULT NULL,
		  PRIMARY KEY (`RoleId`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	</cfquery>
	<cfquery datasource="#proj.dsn#">
		CREATE TABLE IF NOT EXISTS `role_permission` (
		  `RolePermissionId` int(10) unsigned NOT NULL AUTO_INCREMENT,
		  `RoleId` mediumint(8) unsigned NOT NULL,
		  `PageId` mediumint(8) unsigned DEFAULT NULL,
		  PRIMARY KEY (`RolePermissionId`),
		  UNIQUE KEY `Role_Page_Module` (`RoleId`,`PageId`),
		  KEY `PageId` (`PageId`),
		  CONSTRAINT `role_permission_ibfk_1` FOREIGN KEY (`RoleId`) REFERENCES `role` (`RoleId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
		  CONSTRAINT `role_permission_ibfk_2` FOREIGN KEY (`PageId`) REFERENCES `page` (`PageId`) ON DELETE NO ACTION ON UPDATE NO ACTION
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	</cfquery>
	<cfquery datasource="#proj.dsn#">
		CREATE TABLE IF NOT EXISTS `user` (
		  `UserId` int(10) unsigned NOT NULL AUTO_INCREMENT,
		  `CompanyId` tinyint(3) unsigned DEFAULT NULL,
		  `Surname` varchar(60) DEFAULT NULL,
		  `OtherNames` varchar(100) DEFAULT NULL,
		  `Email` varchar(100) DEFAULT NULL,
		  `PersonalEmail` varchar(100) DEFAULT NULL,
		  `UserType` enum('Staff','Client') NOT NULL DEFAULT 'Staff',
		  `Exit` enum('Yes','No') NOT NULL DEFAULT 'No',
		  `Signature` varchar(50) DEFAULT NULL,
		  `Gender` char(7) DEFAULT NULL,
		  PRIMARY KEY (`UserId`),
		  KEY `CompanyId` (`CompanyId`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	</cfquery>
	<cfquery datasource="#proj.dsn#">
		CREATE TABLE IF NOT EXISTS `user_login` (
		  `LoginId` int(10) unsigned NOT NULL AUTO_INCREMENT,
		  `UserId` int(10) unsigned NOT NULL,
		  `PasswordKey` varchar(32) DEFAULT NULL,
		  PRIMARY KEY (`LoginId`),
		  UNIQUE KEY `EmployeeId` (`UserId`),
		  CONSTRAINT `user_login_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `user` (`UserId`) ON DELETE NO ACTION ON UPDATE NO ACTION
		) ENGINE=InnoDB DEFAULT CHARSET=ascii;
	</cfquery>
	<cfquery datasource="#proj.dsn#">
		CREATE TABLE IF NOT EXISTS `user_role` (
		  `UserRoleId` int(10) unsigned NOT NULL AUTO_INCREMENT,
		  `RoleId` mediumint(8) unsigned NOT NULL,
		  `UserId` int(10) unsigned NOT NULL,
		  PRIMARY KEY (`UserRoleId`),
		  UNIQUE KEY `RoleId` (`RoleId`,`UserId`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	</cfquery>
	<cfquery datasource="#proj.dsn#">

		CREATE TABLE IF NOT EXISTS `file` (
		  `FileId` int(10) unsigned NOT NULL AUTO_INCREMENT,
		  `ModelId` tinyint(10) unsigned NOT NULL,
		  `Key` int(11) NOT NULL,
		  `File` varchar(255) DEFAULT NULL,
		  `Size` bigint(20) DEFAULT NULL,
		  `Date` datetime NOT NULL,
		  `UploadedByUserId` int(10) unsigned NOT NULL,
		  `Tag` varchar(200) DEFAULT NULL,
		  PRIMARY KEY (`FileId`),
		  KEY `ModelId` (`ModelId`),
		  CONSTRAINT `file_ibfk_1` FOREIGN KEY (`ModelId`) REFERENCES `model` (`ModelId`) ON DELETE NO ACTION ON UPDATE NO ACTION
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	</cfquery>

	<li><code>model</code> table created</li>
	<li><code>model_field</code> table created</li>
	<li><code>module</code> table created</li>
	<li><code>page</code> table created</li>
	<li><code>role</code> table created</li>
	<li><code>role_permission</code> table created</li>
	<li><code>user</code> table created</li>
	<li><code>user_login</code> table created</li>
	<li><code>user_role</code> table created</li>
	<li><code>file</code> table created</li>

	<cfquery datasource="#proj.dsn#">
		INSERT IGNORE INTO `user` (`UserId`, `CompanyId`, `Surname`, `OtherNames`, `Email`, `PersonalEmail`, `UserType`, `Exit`, `Signature`)
		VALUES
		(1, null, 'Host', 'Account', 'host@demo.com', 'personal@email.com', 'Staff', 'No', NULL);
	</cfquery>

	<cfquery datasource="#proj.dsn#">
		INSERT IGNORE INTO `user_login` (`LoginId`, `UserId`, `PasswordKey`)
		VALUES
			(1, 1, '#hmac("123", 1)#');
	</cfquery>

	<!--- create login data : 7 -> 2 setting, 8 -> 3 --->
	<cfquery datasource="#proj.dsn#">
		INSERT IGNORE INTO `module` (`ModuleId`, `Name`, `Icon`, `Position`, `URL`, `ViewBy`, `MenuBar`, `ForcePageReload`)
		VALUES
			(1,'Home','fa fa-home',1,'home.welcome','Staff','Yes','No'),
			(2,'Settings','fa fa-cog',20,'settings.view','Staff','Yes','No'),
			(3,'Host','fa fa-code',21,NULL,'Staff','Yes','No');
	</cfquery>
	<li>data for <code>module</code> table created</li>

	<cfquery datasource="#proj.dsn#">
		INSERT IGNORE INTO `page` (`PageId`, `ModuleId`, `Name`, `URL`, `Position`, `ShowInMenu`, `Icon`)
		VALUES
			(1, 3, 'Modules', 'host.module.list', 1, 'Yes', 'fa fa-sitemap'),
			(2, 2, 'Staff Roles', 'settings.role.staff_list', 40, 'No', 'fa fa-unlock-alt'),
			(3, 3, 'View Pages', 'host.module.view_pages', 1, 'No', ''),
			(4, 3, 'Edit Page', 'host.module.edit_page', 1, 'No', NULL),
			(5, 3, 'New Page', 'host.module.new_page', 1, 'No', NULL),
			(6, 2, 'View Permissions', 'settings.role.view_permissions', 41, 'No', ''),
			(7, 2, 'New Permissions', 'settings.role.new_permission', 41, 'No', ''),
			(8, 2, 'Users', 'settings.user.list', 35, 'No', 'fa fa-users'),
			(9, 2, 'New User', 'settings.user.new', 36, 'No', ''),
			(10, 2, 'Update User', 'settings.user.edit', 36, 'No', ''),
			(11, 2, 'Deactivate User', 'settings.user.deactivate', 36, 'No', ''),
			(12, 2, 'Activate User', 'settings.user.activate', 36, 'No', ''),
			(13, 2, 'New Role', 'settings.role.new', 40, 'No', ''),
			(14, 2, 'Edit Role', 'settings.role.edit', 40, 'No', ''),
			(15, 2, 'View User', 'settings.user.view', 36, 'No', ''),
			(16, 2, 'Remove Role', 'settings.user.RemoveRole', 35, 'No', ''),
			(17, 2, 'Reset Password', 'settings.user.ResetPassword', 36, 'No', ''),
			(18, 2, 'Add Role to User', 'settings.user.add_role', 36, 'No', ''),
			(19, 9, 'User profile', 'settings.user.profile', 1, 'No', ''),
			(20, 1, 'Home', 'home.welcome', NULL, 'No', '');
	</cfquery>
	<li>data for <code>page</code> table created</li>

	<cfquery datasource="#proj.dsn#">
		INSERT IGNORE INTO  `role` (`RoleId`, `Name`, `Type`)
		VALUES (1, 'Host', 'Staff');
	</cfquery>

	<cfquery datasource="#proj.dsn#">
		INSERT IGNORE INTO `role_permission` (`RolePermissionId`, `RoleId`, `PageId`)
		VALUES
			(1, 1, 1),
			(2, 1, 2),
			(3, 1, 3),
			(4, 1, 4),
			(5, 1, 5),
			(6, 1, 6),
			(7, 1, 7),
			(8, 1, 8),
			(9, 1, 9),
			(10, 1, 10),
			(11, 1, 11),
			(12, 1, 12),
			(13, 1, 13),
			(14, 1, 14),
			(15, 1, 15),
			(16, 1, 16),
			(17, 1, 17),
			(18, 1, 18),
			(19, 1, 19),
			(20, 1, 20);

	</cfquery>

	<cfquery datasource="#proj.dsn#">
		INSERT IGNORE INTO `user_role` (`UserRoleId`, `RoleId`, `UserId`)
		VALUES
			(1, 1, 1);
	</cfquery>


</cfif>
