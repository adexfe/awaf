<cfscript>

	this.name = '{{project-name}}';
	this.datasource = '{{project-dsn}}';
	this.applicatiofficelimeout = createTimeSpan(0,10,0,0);
	this.clientManagement = true;
	this.clientStorage = 'Registry';
	this.setClientCookies = true;
	this.SetDomainCookies = true;
	this.sessionManagement = true;
	this.sessiontimeout = createTimeSpan(0,5,0,0);

</cfscript>
