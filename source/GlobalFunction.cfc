/**
*
* @file
* @author
* @description
*
*/

component output="false" displayname=""  {

	public string function getRandomVariable()	{

		return 'x' & createuniqueid();
	}

	public string function replaceCarriageReturn(required string txt)	{

		return replace(arguments.txt,Chr(10),'<br/>','all');
	}

	public boolean function canSeeSubsidiaries()	{

		var rt = false;

		if(findNoCase("secure.SeeSubsidiaries", request.user.pageurls))	{

			rt = true;
		}

		return rt;

	}

	public string function NaturalDateFormat(required any d)	{

		var rt = '';
		if(isdate(arguments.d))	{
			rt = '<sup>th</sup>';
			switch(right(day(arguments.d),1))	{
				case 1:
					rt = '<sup>st</sup>';
				break;
				case 2:
					rt = '<sup>n*</sup>';
				break;
				case 3:
					rt = '<sup>r*</sup>';
				break;
			}
			if(listFind('11,12,13,14,15,16,17,18,19,20', day(arguments.d)))	{
				rt = '<sup>th</sup>';
			}
			rt = dateformat(arguments.d,'d#rt# mmm yyyy');
			rt = replace(rt,'*','d');
		}
		return rt;
	}

	public string function DateFormat1(required any date)	{

		return datetimeformat(arguments.date,'mmm d, yyyy h:mm tt');
	}

	public string function DateFormat2(required any date)	{

		return datetimeformat(arguments.date,'mmm d, yyyy');
	}

	public string function moneyFormat(required any d )	{
		var rt = '';
		if(isNumeric(arguments.d))	{
			rt = Numberformat(arguments.d,'9,999.99');
		}

		return rt;
	}


	/*
		leading zeros in fromt of a number e.g 004, 005 | 010, 011, 012
	*/
	public string function zeroFill(required numeric num, numeric totalLen = 2)	{

		return prependZeros(arguments.num,arguments.totalLen);
	}
	/*
		leading zeros in fromt of a number e.g 004, 005 | 010, 011, 012
	*/
	public string function prependZeros(required numeric num, numeric totalLen = 2)	{

		var number_len = len(arguments.num);
		var number_len_rem =  arguments.totalLen - number_len;

		switch(number_len_rem){
			case 1:
				rt = "0" & arguments.num;
			break;
			case 2:
				rt = "00" & arguments.num;
			break;
			case 3:
				rt = "000" & arguments.num;
			break;
			case 4:
				rt = "0000" & arguments.num;
			break;
			default:
				rt = arguments.num;
			break;
		}
		return rt;
	}

	public string function NumberToWords(required numeric amount, required string currency_name, required string currency_unit)	{


		return createObject("component","officelime.global.awaf.com.Util.Finance.NumberToWords").init(NumberFormat(arguments.Amount,'999.99'), arguments.currency_name, arguments.currency_unit).Convert();
	}

	public string function timeAfter(required date dateThen, date rightNow = now()) 	{


		return timeAgo(dateThen, rightNow, "after");
	}

	public string function shortHandNumber(required number number)	{

		arguments.number = listFirst(arguments.number,'.');

		switch(arguments.number.len()){
			case 4:
				num = left(arguments.number,1) & "K";
			break;
			case 5:
				num = left(arguments.number,2) & "K";
			break;
			case 6:
				num = left(arguments.number,3) & "K";
			break;
			case 7:
				num = left(arguments.number,1) & "M";
			break;
			case 8:
				num = left(arguments.number,2) & "M";
			break;
			case 9:
				num = left(arguments.number,3) & "M";
			break;
			case 10:
				num = left(arguments.number,1) & "B";
			break;
			default:
				num = arguments.number;
			break;
		}

		return num;

	}

	public string function timeAgo(required date dateThen, date rightNow = now(), string time_frame = "ago") 	{
		var result = "";
		var i = "";
		//var arguments.rightNow = Now();
		Do 	{
			i = dateDiff('yyyy',arguments.dateThen,arguments.rightNow);
			if(i GTE 2)	{
				result = "#i# years #arguments.time_frame#";
				break;
			}
			else if (i EQ 1)	{
				result = "#i# year #arguments.time_frame#";
				break;
			}
			i = dateDiff('m',arguments.dateThen,arguments.rightNow);
			if(i GTE 2)	{
				result = "#i# months #arguments.time_frame#";
				break;
			}
			else if (i EQ 1)	{
				result = "#i# month #arguments.time_frame#";
				break;
			}

			i = dateDiff('d',arguments.dateThen,arguments.rightNow);
			if(i GTE 2){
				result = "#i# days #arguments.time_frame#";
				break;
			}
			else if (i EQ 1)	{
				result = "#i# day #arguments.time_frame#";
				break;
			}

			i = dateDiff('h',arguments.dateThen,arguments.rightNow);
			if(i GTE 2)	{
				result = "#i# hours #arguments.time_frame#";
				break;
			}
			else if (i EQ 1)	{
				result = "#i# hour #arguments.time_frame#";
				break;
			}

			i = dateDiff('n',arguments.dateThen,arguments.rightNow);
			if(i GTE 2)	{
				result = "#i# minutes #arguments.time_frame#";
				break;
			}
			else if (i EQ 1)	{
				result = "#i# minute #arguments.time_frame#";
				break;
			}

			i = dateDiff('s',arguments.dateThen,arguments.rightNow);
			if(i GTE 2)	{
				result = "#i# seconds #arguments.time_frame#";
				break;
			}
			else if (i EQ 1)	{
				result = "#i# second #arguments.time_frame#";
				break;}
			else 	{
				result = "less than 1 second #arguments.time_frame#";
				break;
			}
		}
		While (0 eq 0);

		return result;
	}

	//@return x years y months
 	public string function TimeIn(required numeric m)	{

		temp = "";

		if(arguments.m neq 0)	{

			y = arguments.m \ 12;
			mon = abs((y * 12) - arguments.m);

			if (y gt 0)	{
				if(y eq 1) 	{
					temp = "#y# Year";
				}
				else {
					temp = "#y# Years";
				}
			}

			if (mon eq 1) 	{
				temp = temp & " #mon# Month";
			}
			else if(mon gt 1) 	{
				temp = temp & " #mon# Months";
			}
		}

		return trim(temp);
	}
}