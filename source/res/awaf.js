function openURL(_url)	{
	var win = window.open(_url, '_blank');
	win.focus();
}
function randomString() {
	var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
	var string_length = 8;
	var randomstring = '';
	for (var i=0; i<string_length; i++) {
		var rnum = Math.floor(Math.random() * chars.length);
		randomstring += chars.substring(rnum,rnum+1);
	}
	return '_' + randomstring;
}

/*
	@container 	: where to render the alert {default is page-alert}
	@options 	: options to use to configure the alert
*/
function pageAlert(container, options)	{

	if(options==undefined)	{options={};}
	if(options.icon==undefined)	{options.icon = 'warning45';}
	if(options.title==undefined)	{options.title = 'Alert';}
	if(options.style==undefined)	{options.style = 'info';}
	if(options.message==undefined)	{options.message = '';}

	if(container === '')	{
		container = $('#page-alert');
	}
	else 	{
		container = $('#'+container);
	}
	var alert_id = randomString();
	var alert_wraper = 			  "<div class='alert-wrap in ' style='display: none;' id='" + alert_id + "'>";
	alert_wraper = alert_wraper + " <div class='alert alert-"  + options.style + "' role='alert'>";
	alert_wraper = alert_wraper + " 	<div class='media'>";
	alert_wraper = alert_wraper + " 		<div class='pull-left'>";
	alert_wraper = alert_wraper + " 			<img class='media-object' width='40px' src='assets/img/alert/" + options.icon + ".png'/>";
	alert_wraper = alert_wraper + " 		</div>";
	alert_wraper = alert_wraper + " 	<div class='media-body'>";
	alert_wraper = alert_wraper + " 	<button class='close' type='button' onclick=hide('" +  alert_id + "')><i class='fa fa-times-circle'></i></button>";
	alert_wraper = alert_wraper + " 		<h4 class='alert-title'>" + options.title + "</h4>";
	alert_wraper = alert_wraper + " 		<p class='alert-message'>" + options.message + "</p>";
	alert_wraper = alert_wraper + " 	</div>";
	alert_wraper = alert_wraper + " </div>";
	alert_wraper = alert_wraper + "</div>";
	//var alert_wraper = jQuery(alert_wraper);
	container.prepend(alert_wraper);

 	$('#'+alert_id+' .alert-message').html(options.message);
	$("#"+alert_id).show('blind');
	var countdown;


	$("#"+alert_id).on('mouseenter', function() {
	    clearTimeout(countdown);
	})
	$("#"+alert_id).on('mouseleave', function() {
		countdown = setTimeout(function() {
			hide(alert_id);
		}, 10000);
	})

	countdown = setTimeout(function() {
		hide(alert_id);
	}, 10000);

	scrollToTop();

}

function hide(x)	{

	$('#'+x).hide('blind',100);

}

function showModal(url, options)	{

	if(options==undefined)	{options={};}
	if(options.title==undefined)	{options.title = 'Dialog';}
	if(options.param==undefined)	{options.param = '';}
	if(options.backdrop==undefined)	{options.backdrop = false;}

	url = url.replace("?!=", "");
	var page_id = url.replaceAll(".", "_");
	page_id = page_id.replace("@", "_");
	var contl = url.split("@");

	if(contl[1]==undefined)	{
		url = contl[0];
		url2 = contl[0];
	}
	else	{
		url = contl[0] + "&key=" + contl[1];
		url2 = contl[0] + "@" + contl[1];
	}


	$('#__app_modal_title').html(options.title);
	$.ajax({
		url: view_path + contl[0].replaceAll(".","/") + '.cfm?rand=' + Math.random() + '&controller=' + url + '&' + options.param,
		cache: false,
		beforeSend: function() {
			//showProgressBar();
			// clear the content
			$('#__app_modal_body').html("<div align='center'><img src='assets/img/loading.gif'/></div><br/><br/><br/>");
		}
	}).done(function(data)	{
			$('#__app_modal_body').html(data);
	}).fail(function(xhr)	{
			$('#__app_modal_body').html("<div class='text-danger'>" + getErrorMessage(xhr) + "</div><br/><br/><br/>");
	});

	$('#__app_modal').modal({
  		backdrop:options.backdrop
	});
}
/*
OPTIONS
	changeURL 		: determine if to change the url on the brower on not
	forcePageReload : determine if to reload all the content of the page
	title 			: the title of the page
	param			: other parameter to pass to the url
	renderTo 		: the id of the element to render the content to
*/
function loadTabPage(url, options)	{

	if(options==undefined)	{options={};}
	if(options.changeURL==undefined)	{options.changeURL = true;}
	if(options.forcePageReload==undefined)	{options.forcePageReload = false;}
	if(options.param==undefined)	{options.param = '';}

	url = url.replace("?!=", "");
	var page_id = url.replaceAll(".", "_");
	page_id = page_id.replace("@", "_");
	var contl = url.split("@");

	if(contl[1]==undefined)	{
		url = contl[0];
		url2 = contl[0];
	}
	else	{
		url = contl[0] + "&key=" + contl[1];
		url2 = contl[0] + "@" + contl[1];
	}
	var module_id = contl[0].split(".");

	app_content = $('#app_content');
	//app_title = $('#app_title');

	module_content = $('#'+module_id[0]);
	render_to_page = $('#'+page_id);
	is_tab_content = false;
	//options.renderTo == undefined = from grid view and breadcrump
	//options.renderTo == content = from main menu

	if(options.renderTo!=undefined)	{
		if(options.renderTo!='content')	{
			is_tab_content = true;
			render_to_page = $('#'+options.renderTo);
		}
	}
	if(!module_content.length)	{
		app_content.append("<div id='" + module_id[0] + "' class='app_module'></div>");
		module_content = $('#'+module_id[0]);
	}
	if(!render_to_page.length)	{
		module_content.append("<div id='" + page_id + "' class='app_page'></div>");
		render_to_page = $('#'+page_id);
	}

	// create the container first, asin <div id="client_list"> then <div id="client_list$1">
	//console.log(options.forcePageReload);
	if((!render_to_page.find('div').length) || (options.forcePageReload===true))	{
		$.ajax({
			url: view_path + contl[0].replaceAll(".","/") + '.cfm?controller=' + url + '&' + options.param,
			//url: 'views/' + contl[0].replaceAll(".","/") + '.cfm?rand=' + Math.random() + '&controller=' + url + '&' + options.param,
			beforeSend: function() {
				//blockpage();
				showProgressBar();
			},
			cache: false
		}).done(function(data)	{
				render_to_page.html(data);
				//console.log(is_tab_content);
				//if(is_tab_content===false)	{showSelectPage(module_content, render_to_page);}
				if(options.changeURL===true)	{
					// build defailt container like in the index page to house [data]
					//window.history.pushState(null, null, '?!=' + url2);
					//window.history.pushState({html: app_content[0].innerHTML}, "", '?!=' + url2);
				}
				//if(options.title!=undefined)	{app_title.html(options.title);}
		}).fail(function(xhr)	{
				showError(xhr);
		}).always(function()	{
			hideProgressBar();
		});
	}
	else	{
		//if(is_tab_content===false)	{showSelectPage(module_content, render_to_page);}
		//app_title.html(options.title);
		if(options.changeURL)	{
			// build defailt container like in the index page to house [data]
			//window.history.pushState(null, null, '?!=' + url2);
			//window.history.pushState({html: app_content[0].innerHTML}, "", '?!=' + url2);
		}
	}
}

/**
 * [scrollToAnchor scroll the page to an anchor. for this to work the a element has to have an anchor class
 * <a class='anchor' name='some_name'"/>]
 * @param  {[string]} anchor_name [the name of the anchor as used in the view template]
 */
function scrollToAnchor(anchor_name)	{
	if(anchor_name == "")	{
		scrollToTop();
	}
	else 	{
   	var aTag = $("a.anchor[name='"+ anchor_name +"']");
    	$('html,body').animate({scrollTop: aTag.offset().top},'slow');
   }
}

/*
OPTIONS
	changeURL 		: determine if to change the url on the browser on not
	forcePageReload : determine if to reload all the content of the page
	title 			: the title of the page
	param			: other parameter to pass to the url
	renderTo 		: the id of the element to render the content to
*/
function loadPage(url, options)	{
	//alert(url);
	if(options==undefined)					{options={};}
	if(options.title==undefined)			{options.title = '';}
	if(options.handle==undefined)			{options.handle = null}				else	{/*console.log(options.handle)*/}
	if(options.changeURL==undefined)		{options.changeURL = true;}
	//if(options.renderTo==undefined)		{options.renderTo = 'content';}
	if(options.forcePageReload==undefined)	{options.forcePageReload = false;}
	if(options.param==undefined)			{options.param = '';}

	url = url.replace("?!=", "");
	var page_id = url.replaceAll(".", "_");
	page_id = page_id.replace("@", "_");
	var pg_split = page_id.split("#");
	page_id = pg_split[0];
	var scroll_to = "";
	if(pg_split.length ==2)	{
		scroll_to = pg_split[1];
	}

	var contl = url.split("@");

	if(contl[1]==undefined)	{
		url = contl[0];
		url2 = contl[0];
	}
	else	{
		url = contl[0] + "&key=" + contl[1];
		url2 = contl[0] + "@" + contl[1];
	}
	var module_id = contl[0].split(".");

	app_content = $('#app_content');
	//app_title = $('#app_title');
	//app_history = $('#__app_history ul');

	module_content = $('#'+module_id[0]);
	render_to_page = $('#'+page_id);
	is_tab_content = false;
	//options.renderTo == undefined = from grid view and breadcrumb
	//options.renderTo == content = from main menu
	//console.log(options.renderTo );
	if(options.renderTo!=undefined)	{
		if(options.renderTo!='content')	{
			is_tab_content = true;
			render_to_page = $('#'+options.renderTo);
		}
	}
	//alert(options.renderTo);
	if(!module_content.length)	{
		app_content.append("<div id='" + module_id[0] + "' class='app_module'></div>");
		module_content = $('#'+module_id[0]);
	}
	if(!render_to_page.length)	{
		module_content.append("<div id='" + page_id + "' class='app_page'></div>");
		render_to_page = $('#'+page_id);
	}

	// create the container first, asin <div id="client_list"> then <div id="client_list$1">
	//console.log(options.forcePageReload);
	if((!render_to_page.find('div').length) || (options.forcePageReload===true))	{
		options.title = options.title.replace('_',' ');
		$.ajax({
			url: view_path + contl[0].replaceAll(".","/") + '.cfm?controller=' + url + '&' + options.param + '&title='+options.title,
			//url: 'views/' + contl[0].replaceAll(".","/") + '.cfm?rand=' + Math.random() + '&controller=' + url + '&' + options.param,
			beforeSend: function() {
				showProgressBar();
			},
			cache: false
		}).done(function(data)	{
				render_to_page.html(data);
				if(is_tab_content===false)	{showSelectPage(module_content, render_to_page);}
				if(options.changeURL===true)	{
					// build default container like in the index page to house [data]
					window.history.pushState(null, null, '?!=' + url2);

					scrollToAnchor(scroll_to);

				}

		}).fail(function(xhr)	{
			showError(xhr);
		}).always(function(d)	{
			hideProgressBar();
		});
	}
	else	{
		if(is_tab_content===false)	{showSelectPage(module_content, render_to_page);}
		//app_title.html(options.title);
		if(options.changeURL)	{
			// build defailt container like in the index page to house [data]
			window.history.pushState(null, null, '?!=' + url2);
		}
	}
}

/*function appHistoryScrollRight()	{
	var leftPos = app_history.width();
	app_history.animate({scrollLeft: leftPos + 2000}, 800);
	//console.log(leftPos);
}*/
function showProgressBar()	{

	$('#awaf_progress_bar').append("<span class='overlay'></span><i class='fa fa-spinner fa-pulse fa-4x'></i>");

}

function hideProgressBar()	{

	$('#awaf_progress_bar').html('');

}

function showSelectPage(m, p)	{
	$('.app_module').addClass('hide');
	$('.app_page').addClass('hide');
	m.removeClass('hide');
	p.removeClass('hide');
	$('#app_content').scrollTop(0);
}

function showError(xhr)	{

	var errorPage = $(xhr.responseText), msg = "", err_msg_label, detail_msg = "", stacktrace_msg = "";

	var tbl = errorPage.find("#-lucee-err");

	if(tbl[0]==undefined)	{
		// loop through prevObject and
		tbl.prevObject.each(function()	{
			if(this.id=='-lucee-err')	{
				tbl = $(this);
			}
		});
	}

	if(tbl.find("tr:nth-child(2) td:last")[0]!=undefined)	{
		msg = tbl.find("tr:nth-child(2) td:last")[0].innerHTML;

		// check the third row. it might be detail or stacktrace
		err_msg_label = tbl.find("tr:nth-child(3) td:first")[0].innerHTML;
		if(err_msg_label == 'Detail')	{
			detail_msg = '<hr/>'+tbl.find("tr:nth-child(3) td:last")[0].innerHTML;
		}
		else	{
			stacktrace_msg = "<div class='awaf_deveoper'>" + '<hr/>'+tbl.find("tr:nth-child(3) td:last")[0].innerHTML + '</div>';
		}

		/*$.growl.default_options.type = 'danger';
		$.growl({
			message: msg + detail_msg + stacktrace_msg,
			icon: 'fa fa-exclamation-triangle',
			title: 'Error'
		});*/

		//var err = $.parseJSON(xhr.responseText);
		errorNotice(msg);// + detail_msg + stacktrace_msg);
	}

	//console.log(msg);
}

function getErrorMessage(xhr)	{

	var errorPage = $(xhr.responseText), msg = ""
	var tbl = errorPage.find("#-lucee-err");

	if(tbl[0]==undefined)	{
		// loop through prevObject and
		tbl.prevObject.each(function()	{
			if(this.id=='-lucee-err')	{
				tbl = $(this);
			}
		});
	}

	if(tbl.find("tr:nth-child(2) td:last")[0]!=undefined)	{
		msg = tbl.find("tr:nth-child(2) td:last")[0].innerHTML;
	}

	return msg

}

function ajaxRequest(url, btn, donefn)	{
	if(donefn==undefined)	{donefn=function(){};}
	$.ajax({
		url: 'controllers/' + url + '&rand=' + Math.random(),
		cache: false,
        beforeSend:function()   {
            btn.className += ' disabled';
            showProgressBar();
        },
	}).done(function(data)	{
		donefn(data);
    }).fail(function(xhr)	{
		showError(xhr);
	}).always(function(data) {
        btn.className = btn.className.replace(' disabled','');
        hideProgressBar();
    });
}

function ajaxCall(url, donefn, errorfn)	{
	if(donefn==undefined)	{donefn=function(){};}
	if(errorfn==undefined)	{errorfn=function(){};}
	$.ajax({
		url: 'controllers/' + url + '&rand=' + Math.random(),
        beforeSend:function()   {
            showProgressBar();
        },
		cache: false,
	}).done(function(data)	{
		donefn(data);
    }).fail(function(xhr)	{
		showError(xhr);
		errorfn(xhr);
	}).always(function(data) {hideProgressBar();});
}


function itemCreatedNotice(msg)	{
	/*$.growl.default_options.type = 'success';
	//$.growl.default_options.position = {from: 'top', align: 'right'};
	$.growl({
		message: msg,
		icon: 'fa fa-save',
		title: 'Data Saved'
	});*/


	pageAlert('', {
		title: 'Data Saved',
		icon : 'save-file',
		message : msg,
		style : 'success'
	});
}

/*function warningNotice(msg)	{
	$.growl.default_options.type = 'warning';
	$.growl({
		message: msg,
		icon: 'fa fa-exclamation-circle',
		title: 'Warning'
	});
}*/

function errorNotice(msg)	{
	// excape quotes
	var msg_ = msg.replaceAll("'","");
	msg_ = msg_.replaceAll('"',"");
	msg_ = msg_.replaceAll('\\',"/");
	msg = msg.replaceAll('&lt;','<');
	msg = msg.replaceAll('&gt;','>');

	$.growl.default_options.type = 'danger';
	$.growl({
		message: msg,
		icon: 'fa fa-exclamation-triangle',
		title: ''
	});

	//console.log(msg);

	/*scrollToTop();
	pageAlert('', {
		title: 'Error',
		icon : 'caution',
		message : msg,
		style : 'danger'
	});*/
}

function scrollToTop()	{
	$("html, body").animate({ scrollTop: 0 }, "fast");
}

function itemSaveNotice(msg)	{
	$.growl.default_options.type = 'success';
	$.growl({
		message: msg,
		icon: 'fa fa-save',
		title: ''
	});
}

function createGravatar(email, size, class_name)	{

	return "<img src='https://www.gravatar.com/avatar/" + md5(email) + '.jpg?d=mm&s=' + size + "' class='" + class_name + "'/>";

}

$(document).ready(function()	{
	$('a.link').click(function(e)	{
		e.preventDefault();

		//var opt = {"renderTo": e.target.attributes['target'].nodeValue, "title": e.target.attributes['title'].nodeValue};
		var _force = false;
		var atr = e.target.attributes;
		if(atr['force'] != undefined)	{
			if(atr['force'].value == "true")	{
				_force = true;
			}
		}
		var opt = {
			"renderTo": atr['target'].value,
			"param": "title="+atr['title'].value,
			"forcePageReload": _force
		};

		loadPage(atr['href'].value, opt);
		// this is used in the mobile view to remove the menu from display
		//$("div.left").toggleClass("mobile-sidebar");
		//$("div.right").toggleClass("mobile-content");

      $('.navbar-nav li').removeClass('active');
      $(this).parent().addClass('active');
      $("#main-nav").removeClass("in");

	});

window.onpopstate = function(e){
	//console.log(e.target.document);
    /*if(e.state){

        document.getElementById("content").innerHTML = e.state.html;
        document.title = e.state.pageTitle;
    }*/
};

	$.growl.default_options = {
	    ele: "body",
	    type: "info",
	    allow_dismiss: true,
	    position: {
	        from: "top",
	        align: "right"
	    },
	    offset: 30,
	    spacing: 10,
	    z_index: 999999999,
	    fade_in: 400,
	    delay: 10000,
	    pause_on_mouseover: true,
	    onGrowlShow: null,
	    onGrowlShown: null,
	    onGrowlClose: null,
	    onGrowlClosed: null,
	    template: {
	        icon_type: 'class',
	        container: '<div class="col-xs-7 col-sm-4 col-md-3 alert">',
	        dismiss: '<a data-dismiss="alert" aria-hidden="true" class="close"><i class="fa fa-times-circle"></i></a>',//'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
	        title: '<strong>',
	        title_divider: '<br/>',
	        message: ''
	    }
	};
});

window.onpopstate = function(e){
    if(e.state){
        document.getElementById("app_content").innerHTML = e.state.html;
        //document.title = e.state.pageTitle;
    }
};

String.prototype.replaceAll = function (find, replace) {
    var str = this;
    return str.replace(new RegExp(find.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), 'g'), replace);
};

// Extend the default Number object with a formatMoney() method:
// usage: someVar.formatMoney(decimalPlaces, symbol, thousandsSeparator, decimalSeparator)
// defaults: (2, "$", ",", ".")
Number.prototype.formatMoney = function(places, symbol, thousand, decimal) {
	places = !isNaN(places = Math.abs(places)) ? places : 2;
	symbol = symbol !== undefined ? symbol : "";
	thousand = thousand || ",";
	decimal = decimal || ".";
	var number = this,
	    negative = number < 0 ? "-" : "",
	    i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
	    j = (j = i.length) > 3 ? j % 3 : 0;
	return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
};

function moneyFormat(number)  {
    var r = 0.00;
    if ($.isNumeric(number))  {
    	number = Number(number);
        r = number.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
    }
    return r;
}

function x__gridremoveTR(grid_id, tr_n)	{

	tr_n = tr_n+1;
	$('#'+grid_id + ' tbody tr:nth-child('+tr_n+')').addClass('animated rollOut');
	//console.log($('#'+grid_id + ' tbody tr:nth-child('+tr_n+')'));

}