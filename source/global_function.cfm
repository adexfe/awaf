<cfscript>

	public any function model(required string modl)	{

		return createObject("component","template.models." & modl).init();
	}

	public any function controller(required string cntr)	{

		arguments.cntr = replace(arguments.cntr,'_','','all');
		return createObject("component", "template.controllers." & arguments.cntr).init();
	}

	public string function ajaxLink1(required string url, string renderTo)	{

		var str = arguments.url;
		str = "javascript:loadPage('#str#',{renderTo:'#arguments.renderTo#'})";
		return str;
	}

	public string function generateLink(required string url, string renderTo)	{

		var str = arguments.url;
		str = "javascript:loadPage('#str#',{renderTo:'#arguments.renderTo#'})";
		return str;
	}

	public boolean function isProductionServer()	{
		var rt = false;
		if(application.awaf.mode==0)	{
			rt = true;
		}
		return rt;
	}

	public string function encryptString(required string str)	{

		return encrypt(arguments.str, application.awaf.secretkey,'AES','Hex');
	}

	public string function decryptString(required string str)	{

		return decrypt(arguments.str, application.awaf.secretkey, 'AES', 'Hex');
	}

	public string function wrapItem(required string whatToWrap, required string class, string elementToUse = 'span')	{

		return "<" & arguments.elementToUse & " class=\'" & arguments.class & "\'>'+" & arguments.whatToWrap & "+'</" & arguments.elementToUse & ">";

	}

</cfscript>
