<cfoutput>

<!---TODO:
================
	- check if there is any project using awaf yet
	- read the projects.json file to display who is using awaf, delete
---->

<div class="row">
	<div class="col-sm-7">
		<strong>Projects using AWAF</strong>
		<hr/>
	<!--- get projects --->
	<cfset file_loc = expandPath("")/>
	<cffile action="read" file="#file_loc#/projects.json" variable="projs"/>
	<cfset projs = deserializeJSON(projs)/>

	<table class="table table-hover">
		<thead>
			<tr>
				<th>Project</th>
				<th>Clients</th>
				<th>Tags</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<cfset i = 0/>
			<cfloop array="#projs.projects#" item="proj">
				<cfset i++/>
				<tr>
					<td>
						#proj.name#
						<cfif structKeyExists(proj,"desc")>
							<br/><small>#proj.desc#</small>
						</cfif>
					</td>
					<td>
						<cfif structKeyExists(proj,"clients")>
							<cfset arraySort(proj.clients, "text")/>
							<cfloop array="#proj.clients#" item="_client">
								<code>#_client#</code>
							</cfloop>
						</cfif>
					</td>
					<td>
						<cfif structKeyExists(proj,"tags")>
							<cfset arraySort(proj.tags, "text")/>
							<cfloop array="#proj.tags#" item="_tag">
								<code>#_tag#</code>
							</cfloop>
						</cfif>
					</td>
					<td>
						<a class="btn btn-xs btn-info" href="?page=update&project=#proj.name#">update</a>
					</td>
				</tr>
			</cfloop>
		</tbody>
	</table>
	</div>
	<div class="col-sm-5">
	<p>AWAF project template sample</p>


<pre class="prettyprint linenums">
{
  "projects": [
    {
      "name" : "project 1",
      "desc" : "project 1 description",
      "root" : "/user/home/project 1/",
      "tags" : [
        "tag 1", "tag 2", "tag 3"
      ]
    },
    {
      "name" : "project 2",
      "desc" : "project 2 description",
      "root" : "/user/home/project 2/",
      "tags" : [
        "tag 1", "tag 2", "tag 3", "tag 4"
      ]
    }
  ]
}
</pre>
	</div>

</div>

</cfoutput>
