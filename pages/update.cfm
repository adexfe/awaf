<cfoutput>
	<cfset dir_loc = expandPath("/")/>
	<cffile action="read" file="#dir_loc#awaf/projects.json" variable="projs"/>
	<cfset projs = deserializeJSON(projs)/>

	<cfloop array="#projs.projects#" item="proj">
		<cfif proj.name eq url.project>
			Updating #proj.name#:
			<hr/>

			<ul>
				<cfif StructKeyExists(proj, "clients")>
					<cfset multiple_client = true/>
					<cfset proj_loc = proj.root & 'global/'/>
				<cfelse>
					<cfset multiple_client = false/>
					<cfset proj_loc = proj.root/>
				</cfif>
				<cfset proj_awaf_loc = proj_loc & "awaf/"/>

				<!--- create project folder --->
				<cfif !directoryExists(proj_loc)>
					<li>directory <code>#proj_loc#</code> created
						<cfdirectory action="create" directory="#proj_loc#"/>
						<cfif multiple_client>

							<cfloop array="#proj.clients#" item="_client">
								<cfset client_loc = proj.root & _client & "/"/>
								<cfif !directoryExists(client_loc)>
									<cfdirectory action="create" directory="#client_loc#"/>
									... <code>#_client#</code> directory created...<br/>
									<!--- create default folders for client from client-template dir --->
									<cfset directoryCopy("#dir_loc#awaf/client-template/", "#client_loc#", true)/>
								</cfif>
							</cfloop>
						</cfif>
					</li>
				</cfif>

				<cfset directoryCopy("#dir_loc#awaf/template/", "#proj_loc#", true)/>
				<!--- copy source --->
				<cfset directoryCopy("#dir_loc#awaf/source/", "#proj_loc#awaf/", true)/>
				<li>
					<code>#dir_loc#awaf/source/*</code> copied to <code>#proj_loc#awaf</code>
				</li>

				<!--- clear all dependencies --->
				<cfif directoryExists("#proj_loc#/assets/awaf/")>
					<cfset directoryDelete("#proj_loc#/assets/awaf/", true)/>
				</cfif>


				<!--- clean up --->
				<li>delete <code>res</code></li>
				<cfset directoryDelete("#proj_loc#/awaf/res/", true)/>

				<li>delete <code>tags</code></li>
				<cfset directoryDelete("#proj_loc#/awaf/tags/", true)/>


				<!--- check tags needed and copy dependencies --->

				<cfloop array="#proj.tags#" item="_tag">
					<cfset _tag = lcase(_tag)/>
					<cfif directoryExists("#dir_loc#awaf/source/res/#_tag#")>
						<cfset directoryCopy("#dir_loc#awaf/source/res/#_tag#", "#proj_loc#/assets/awaf/#_tag#", true)/>
						<li>copy <code>#_tag#</code> tag</li>
					</cfif>
					<cfif _tag eq "grid">
						<cfif !directoryExists("#proj_loc#/awaf/tags/grid")>
							<cfdirectory action="create" directory="#proj_loc#/awaf/tags/grid/"/>
							<cffile action="copy" destination="#proj_loc#/awaf/tags/grid/" source = "#dir_loc#awaf/source/tags/Grid/ajax.cfm" />
							<cffile action="copy" destination="#proj_loc#/awaf/tags/grid/" source = "#dir_loc#awaf/source/tags/Grid/export.cfm" />
							<li><code>grid/ajax.cfm</code> copied</li>
							<li><code>grid/export.cfm</code> copied</li>
						</cfif>
					</cfif>
					<cfif _tag eq "editable">
						<cfif !directoryExists("#proj_loc#/awaf/tags/editable")>
							<cfdirectory action="create" directory="#proj_loc#/awaf/tags/editable/"/>
							<cffile action="copy" destination="#proj_loc#/awaf/tags/editable/" source = "#dir_loc#awaf/source/tags/editable/ajax.cfm" />
							<li><code>editable/ajax.cfm</code> copied</li>
						</cfif>
					</cfif>
				</cfloop>


				<!--- create js/css script template --->

				<cfinclude template="../install/sql_script.cfm"/>


				<cfinclude template = "../install/rewrite_file.cfm">



				<li>Open project : <code><a href="https://localhost:#cgi.SERVER_PORT#/#proj.name#" target="_blank">https://localhost:#cgi.SERVER_PORT#/#proj.name#</a></code></li>
			</ul>


		</cfif>
	</cfloop>
</cfoutput>
