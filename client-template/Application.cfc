component output="true"  {

	include "awaf/config.cfm";

	structAppend(url, createObject("component", "{{project-name}}.{{client-name}}.awaf.GlobalFunction"));


	public void function onApplicationEnd(struct ApplicationScope=structNew()) {

		return;
	}

	public boolean function onApplicationStart() {

		include "../global/app_variables.cfm";
		include "settings.cfm";

		return true;
	}


	public boolean function onRequest(required string targetPage)	{

		include "awaf/on_request.cfm";

		if (request.include_page is 'views/secure_page.cfm') 	{

			if(
				(findnocase('login.cfm', arguments.targetPage)) ||
				(findnocase('index.cfm', arguments.targetPage)) ||
				(findnocase('forget.cfm', arguments.targetPage))||
				(findnocase('logout.cfm', arguments.targetPage))||
				(findnocase('ajax.cfm', arguments.targetPage))
			)
			{

				include lcase(arguments.targetPage);
			}
			else 	{

				include '../global/views/secure_page.cfm';
			}
		}
		else 	{

			try {
				include lcase(arguments.targetPage);
			}
			catch(any error) {

				if(error.type eq 'missinginclude')	{
					include '../global/' & request.include_page;
				}
				else {
					include lcase(arguments.targetPage);
				}
			}

		}

		return true;
	}

	public void function onSessionEnd(required struct SessionScope, struct ApplicationScope=structNew()) {

		return;
	}

	public void function onSessionStart() {

		return;
	}

}
