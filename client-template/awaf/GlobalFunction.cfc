
component output="false" displayname="" extends="officelime.global.awaf.GlobalFunction"  {

	// @ model description : model table table_alias
	public Model function Model(required string model_desc)	{

		var arg = getModelDefaultParam(arguments.model_desc);

		return createObject("component","officelime.arm.models." & arg.model_name).init(table_name=arg.table_name);
	}

	// @ controller description
	public Controller function _controller(required string cntr)	{

		arguments.cntr = replace(arguments.cntr,'_','','all');
		return createObject("component", "officelime.arm.controllers." & arguments.cntr).init();
	}

}