<cfscript>

	include "../../global/awaf/global_function.cfm";

	public any function controller(required string cntr)	{

		arguments.cntr = replace(arguments.cntr,'_','','all');
		return createObject("component", "officelime.arm.controllers." & arguments.cntr).init();
	}

	public any function model(required string modl)	{
		 
		return createObject("component","officelime.arm.models." & modl).init();
	}
</cfscript>