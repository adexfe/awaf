component output="false" extends="officelime.global.awaf.com.Controller"  {

	// save to file table
	remote string function upload(required numeric modelid, required numeric key, required string tag, required numeric userid) returntypeformat="json"	{
		
		var rand_path = randRange(100,999) & '/' & randRange(100,999) & '/';
		var path_to_save = '';			
		var error = '';				// error to report to the client
		var uploaded_file_list = '';// list of uploaded files to return to the client

		transaction	{

			lock timeout='100' name='file-upload' throwontimeout='true' type='exclusive'	{
				
				path_to_save = expandPath("../../../attachment/" & rand_path);

				if(!directoryExists(path_to_save))	{
					directory action="create" directory=path_to_save;
				}

				try {

					file action="upload" result="saved_file"  destination=path_to_save;
					// save file to db 
					model(application.model.FILE).saveFile(
						modelid 	: arguments.modelid,
						file_info 	: saved_file,
						file_path 	: rand_path,
						key 		: arguments.key,
						tag 		: arguments.tag,
						userid 		: arguments.userid
					);
				
				}
				catch(any exception) {
					//if (arguments.save_to_temp_first)	{
					error = exception.message;
					//}
				}
				
			}

		}

		if(error neq '')	{
			return '{"error" : ' & serializeJSON(error) & '}';
		}
		else 	{
			return '{"file_name": #serializeJSON(rand_path & saved_file.serverfile)#}';
		} 
	}

	// also delete from server
	remote void function delete(required string file_name) returntypeformat="json"	{

		lock timeout='60' name='file-upload' throwontimeout='true' type='exclusive'	{
			// check if file exist
 

			var file_to_delete = expandPath("../../../attachment/" & file_name);
			if(fileExists(file_to_delete))	{

				file action="delete" file=file_to_delete;
				model(application.model.FILE).deleteByFileName(file_name);
				
			}
 
			
		}
 
	}

	remote void function deleteFromServer(required string file_name, required numeric size) returntypeformat="json"	{

		lock timeout='60' name='file-delete' throwontimeout='true' type='exclusive'	{
			// check if file exist
			var file_to_delete = expandPath("../../../attachment/" & file_name);
			if(fileExists(file_to_delete))	{
				// remove form db
				model(application.model.FILE).deleteByFileAndSize(file_name, size);
				file action="delete" file="#file_to_delete#"; 


			}
			
		}
 
	}

}