<cfoutput>

	<!--- get module --->
	<cfset oRolePerm = model(application.model.ROLE_PERMISSION)/>
	<cfset qModule = oRolePerm.findModulesInRole( request.user.roleids )/>

	<ul class="sidebar-menu">
		<cfloop query="qModule">
			<cfif qModule.MenuBar>

				<cfset qPage = oRolePerm.findPagesByRoleAndModule(request.user.roleids, ModuleId)/>

				<li <cfif qPage.recordcount>class="treeview"</cfif>>
					<a <cfif qPage.recordcount> href="##" <cfelse> href="?!=#qModule.URL#" class="link" target="content" data-title="#qModule.Name#" <cfif qModule.Name eq "Home">forcepagereload="true"</cfif></cfif>><i class="#qModule.Icon#"></i> <span>#qModule.Name#</span><cfif qPage.recordcount> <i class="fa fa-angle-left pull-right"></i></cfif></a>
					<cfif qPage.recordcount>
						<ul class="treeview-menu">
							<cfloop query="qPage">
								<li><a href="?!=#qPage.URL#" target="content" data-title="#qPage.Name#" class="link"><i class="#qPage.Icon#"></i> <span>#qPage.Name#</span></a></li>
							</cfloop>
						</ul>
					</cfif>
				</li>
			</cfif>
		</cfloop>
	</ul>
	<div class="clear"></div>

</cfoutput>
