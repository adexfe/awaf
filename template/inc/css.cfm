<!--- load from CDN ---->

<cfif cgi.server_port eq "8888">
	<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<cfelse>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
</cfif>
<link href="assets/css/font-awesome.min.css" rel="stylesheet">
<link href="assets/css/fonts.css" rel="stylesheet">
<link rel="stylesheet" href="assets/css/AdminLTE.min.css">

<link rel="stylesheet" href="assets/css/square.css">
<link rel="stylesheet" href="assets/css/bootstrap-multiselect.css">
<link rel="stylesheet" href="assets/css/datatables/dataTables.bootstrap.css"/>
<link rel="stylesheet" href="assets/css/dropzone.css"/>
<link rel="stylesheet" href="assets/css/select2.css"/>
<link rel="stylesheet" href="assets/css/select2-bootstrap.css"/>

<link rel="stylesheet" href="assets/css/editable/bootstrap-editable.css"/>
<link rel="stylesheet" href="assets/css/bootstrap-datetimepicker.min.css"/>

<!---link rel="stylesheet" href="assets/css/style.css"/--->
<link rel="stylesheet" href="assets/css/skin-red.min.css">

<!---link rel="stylesheet" href="assets/css/awaf.css"/---->


<link rel="shortcut icon" href="favicon-128.png" type="image/png" />
<link rel="icon" type="image/png" href="favicon-128.png"  />
