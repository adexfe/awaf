<!--- load from CDN ---->

<cfif cgi.server_port eq "8888">
	<script src="assets/js/jquery-2.1.4.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
<cfelse>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</cfif>
<script src="assets/js/jquery-ui-1.10.4.custom.min.js"></script>


<script src="assets/js/bootstrap-growl.min.js"></script>
<script src="assets/js/select2.min.js"></script>

<script src="assets/js/jquery.validate.js"></script>

<script src="assets/js/icheck.min.js"></script>

<script src="assets/js/jquery.autosize.min.js"></script>

<script src="assets/js/fusioncharts/fusioncharts.js"></script>
<script src="assets/js/fusioncharts/fusioncharts.charts.js"></script>
<script src="assets/js/fusioncharts/themes/fusioncharts.theme.fint.js"></script>

<!--- editable --->
<script src="assets/js/editable/bootstrap-editable.min.js"></script>

<script src="assets/js/bootstrap-datetimepicker.min.js"></script>


<script src="assets/js/bootstrap-multiselect.min.js"></script>
<script src="assets/js/datatables/jquery.dataTables.min.js"></script>
<script src="assets/js/datatables/dataTables.bootstrap.js"></script>
<script src="assets/js/dropzone.min.js"></script>


<script src="assets/js/moment.min.js"></script>

<script src="assets/js/md5.js"></script>

<script src="assets/js/jquery.slimscroll.min.js"></script>
<script src="assets/js/fastclick.min.js"></script>

<script src="assets/js/app.js"></script>
<!---script src="assets/js/demo.js"></script--->


<script src="assets/js/awaf.js"></script>
