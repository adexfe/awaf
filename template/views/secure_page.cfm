<div class="text-center">
<br/>

  <span class="fa-stack fa-5x">
    <i class="fa fa-circle fa-stack-2x"></i>
    <i class="fa fa-warning fa-stack-1x fa-inverse"></i>
  </span>

  <h1>Page not available</h1>
  <br/>
  <h4 style=" line-height:28px;">The page you are trying to access is a elther secure or not available at the moment. <br/>Please contact the Administrator.</h4>

</div>
