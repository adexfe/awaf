<!--- control --->
<cfoutput>

<cfset tr_id = getRandomVariable()/>
<cfset tbl_id = getRandomVariable()/>
<a onclick="showModal('settings.role.new_permission@#view.roleid#')" class="text-right">New Permission</a>
<hr/>

	<table class="table remove-first-border" id="#tbl_id#">
		<cfloop query="view.Permissions">
			<tr id="#tr_id##RolePermissionId#">
				<td>
 
				<cfif PageId is ''>
					#module_Name#
				</cfif> 
					#page_Name#				</td>
				<td class="text-muted">  
				<cfif PageId is ''>
					#module_URL#
				<cfelse>
					#page_URL#
				</cfif> 
				</td>
				<td>
					<a onclick="showModal('settings.role.edit_permission@#view.Permissions.RolePermissionId#');" class=" btn-link"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
					<a data-id="#RolePermissionId#" class="btn-link delete">delete</i></a>
				</td>
			</tr>
		</cfloop>
	</table>

<script type="text/javascript">
$(document).ready(function() {

	<cfset aclick = getRandomVariable()/> 

	var #aclick# = $('###tbl_id# a.delete');

	#aclick#.on('click', function(e)	{
		var a = $(e.target);
		var aid = a.attr('data-id');
		var tr = '###tr_id#'+aid;
		ajaxRequest('settings/role.cfc?method=deletePermission&permissionid='+ aid, a, function(d)	{
			$(tr).remove();
		});
	});

});
</script> 

</cfoutput>