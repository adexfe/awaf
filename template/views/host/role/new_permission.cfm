<cfoutput> 

	<cf_xform id="#view.Id#"> 

		<cf_option name="ModuleId" id="moduleid" label="Module" type="select2" value="#ValueList(view.Modules.ModuleId)#" display="#ValueList(view.Modules.Name)#" required/>

		<input type="hidden" id="pageids" name="pageids" class="form-control"/>
 
		<input type="hidden" name="RoleId" value="#view.roleid#"/>

		<br/><br/><br/>

		<cf_fSubmit value="Add" icon="plus-circle" url="settings/Role.cfc?method=AddPermission"/>

		<br/><br/>

	</cf_xform>

 
<script type="text/javascript">
$(document).ready(function() {

	<cfset module_vn = getRandomVariable()/>
	<cfset page_vn = getRandomVariable()/>

	var #module_vn# = $('###view.Id# ##moduleid');
	var #page_vn# = $('###view.Id# ##pageids');

	#module_vn#.on('change', function(e)	{

		ajaxCall('settings/Role.cfc?method=getPagesNotInRole&roleid=#view.roleid#&moduleid='+e.val, function(d)	{
			d = $.parseJSON(d); 
			var data_ = [];
			for(var i =0;i < d.length;i++)	{
			  data_.push({'id': d[i].PAGEID, 'text': d[i].NAME});
			}
			#page_vn#.select2({
			    data:data_,multiple:true
			});
		});

	});

});
</script> 
</cfoutput>