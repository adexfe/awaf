<style>  
	.remove-first-border tr:first-child td	{
		border-top: white !important; 
	}
	.remove-first-border td:last-child	{
		text-align: right;
	}
</style>

<cfoutput>
<cf_breadcrumb pageHistory="#request.viewPageHistory#"/>

<div class="box-info animated fadeInLeft">

	<div class="row">
		<div class="col-sm-3">
			<ul class="list-group">
				<cfloop query="view.Roles">
					<a class="list-group-item" onclick="loadPage('settings.role.view_permissions@#view.Roles.RoleId#', {'renderTo':'#view.id#','changeURL':false, 'forcePageReload': true, 'param':'elementid=#view.id#'});">
						<cfif val(view.Roles.page_count)><span class="badge">#view.Roles.page_count#</span></cfif>
						#view.Roles.Name#
					</a>
				</cfloop>
			</ul>
		</div>
		<div class="col-sm-9">
			<div id="#view.id#"></div>
		</div>

	</div>
		
</div>

<script>
	
</script>
</cfoutput>