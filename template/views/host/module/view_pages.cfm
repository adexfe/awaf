<!--- control --->
<cfoutput>
<a onclick="showModal('host.module.new_page@#view.moduleid#')" class="text-right">New Page</a>
<hr/>

	<table class="table remove-first-border">
		<cfloop query="view.Pages">
			<tr>
				<td>
				<cfif !(view.Pages.ShowInMenu)>
&nbsp;&nbsp;&nbsp;&nbsp;<span class="text-muted">&mdash; #Name#</span>
				<cfelse>
					<i class="#Icon#"></i> #Name#
				</cfif></td>
				<td><a onclick="showModal('host.module.edit_page@#view.Pages.PageId#');"><i class="fa fa-edit"></i></a></td>
			</tr>
		</cfloop>
	</table>
</cfoutput>