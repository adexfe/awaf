<cfoutput> 

	<cf_xform id="#view.Id#"> 

		<cf_frow id="#view.rowId#">
			<cf_xinput name="Position" label="Position" value="#view.Page.Position#" size="col-sm-2"/>
			<cf_inputgroup name="Icon" label="Icon" value="#view.Page.Icon#" size="col-sm-4" addonprependicon="#view.Page.Icon#"/>
			<cf_xinput name="Name" label="Name" value="#view.Page.Name#" size="col-sm-6"/>
		</cf_frow>

		<cf_frow>
			<cf_option name="ShowInMenu" label="Show in menu" size="col-sm-6" type="radio" value="Yes,No" selected="#view.Page.ShowInMenu#"/>
			<cf_option name="ModuleId" label="Module" size="col-sm-6" type="select" value="#ValueList(view.Modules.ModuleId)#" display="#ValueList(view.Modules.Name)#" selected="#view.Page.ModuleId#"/>
		</cf_frow>

		<cf_frow>
			 <cf_xinput name="URL" value="#view.Page.URL#" size="col-sm-12"/>
		</cf_frow>
 
 		<input type="hidden" name="PageId" value="#view.Page.PageId#" />

		<cf_fSubmit value="Save" url="host/Module.cfc?method=Save"/>

		<br/><br/>

	</cf_xform>

 
<script type="text/javascript">
$(document).ready(function() {
	$("###view.Id# input[name='Icon']").on('keyup', function(e)	{

		$("###view.rowId# i")[0].className = e.target.value;

	})
});
</script> 
</cfoutput>