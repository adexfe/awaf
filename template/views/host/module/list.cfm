<style>  
	.remove-first-border tr:first-child td	{
		border-top: white !important; 
	}
	.remove-first-border td:last-child	{
		text-align: right;
	}
</style>

<cfoutput>
<cf_breadcrumb pageHistory="#request.viewPageHistory#"/>

<div class="box-info animated fadeInLeft">

	<div class="row">
		<div class="col-sm-3">
			<h4 class="text-center">Staff Modules</h4>
			<ul class="list-group">
				<cfloop query="view.ModuleQueryStaff">
					<a class="list-group-item" onclick="loadPage('host.module.view_pages@#view.ModuleQueryStaff.ModuleId#', {'renderTo':'#view.id#','changeURL':false, 'forcePageReload': true, 'param':'elementid=#view.id#'});">
						<cfif val(view.ModuleQueryStaff.page_count)><span class="badge">#view.ModuleQueryStaff.page_count#</span></cfif>
						#view.ModuleQueryStaff.Name#
					</a>
				</cfloop>
			</ul>
			<br/>
			<h4 class="text-center">Client Modules</h4>
			<ul class="list-group">
				<cfloop query="view.ModuleQueryClient">
					<a class="list-group-item" onclick="loadPage('host.module.view_pages@#view.ModuleQueryClient.ModuleId#', {'renderTo':'#view.id#','changeURL':false, 'forcePageReload': true, 'param':'elementid=#view.id#'});">
						<cfif val(view.ModuleQueryClient.page_count)><span class="badge">#view.ModuleQueryClient.page_count#</span></cfif>
						#view.ModuleQueryClient.Name#
					</a>
				</cfloop>
			</ul>

		</div>
		<div class="col-sm-9">
			<div id="#view.id#"></div>
		</div>

	</div>

			
</div>

<script>
	
</script>
</cfoutput>