<cfoutput>


<div class="box-info animated fadeInLeft">

	<div class="row">
		<div class="col-sm-3">
			<cf_buttons align="left">
				<cf_button title="New Role" icon="plus" modalurl="settings.role.new@staff" />
			</cf_buttons>
			<br/>
			<ul class="list-group">
				<cfloop query="view.Roles">
					<a class="list-group-item" onclick="loadPage('settings.role.view_permissions@#view.Roles.RoleId#', {'renderTo':'#view.id#','changeURL':false, 'forcePageReload': true, 'param':'elementid=#view.id#'});">
						<cfif val(view.Roles.page_count)><span class="badge">#view.Roles.page_count#</span></cfif>
						#view.Roles.Name#
					</a>
				</cfloop>
			</ul>
		</div>
		<div class="col-sm-9">
			<div id="#view.id#"></div>
		</div>

	</div>
		
</div>

<script>
	
</script>
</cfoutput>