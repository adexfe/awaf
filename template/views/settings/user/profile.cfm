

<style>
	.user .layout-main{
		padding-top: 0;
		padding-left: 20px;
	}
	.col-sm-6>.fa-ul li{margin-bottom: 6px;}
	/*.col-sm-6>.fa-ul li>i{padding-top: 2px;}*/
</style>

<cfset emp = view.employee/>
<cfset mgr = view.manager/>
<cfoutput>

	<div class="row">
		<div class="col-sm-1">

		</div>
		<div class="col-sm-10">
			<div class="row">

				<div class="col-sm-3">

					<div class="profile-avatar">
						<img src="https://www.gravatar.com/avatar/#lcase(hash(emp.user_personalEmail))#?s=275&d=mm" class="profile-avatar-img thumbnail" />
					</div>

				</div>

				<div class="col-sm-9">

		        	<h3>#emp.user_Surname# #emp.user_OtherNames#</h3>

		        	<h5 class="text-muted">#emp.job_title_Name# &mdash; #emp.job_level_Name#</h5>

					<div class="row">
						<div class="col-sm-4">
							<ul class="fa-ul">
								<li>
									<i class="fa-li"><img src="https://www.gravatar.com/avatar/#lcase(hash(mgr.user_personalEmail))#?s=20&d=mm" class="img-circle" /></i>
									#mgr.user_Surname# #mgr.user_Othernames#
									<!---cf_link url="employee.view@#mgr.employeeid#">
										#mgr.user_Surname# #mgr.user_Othernames#
									</cf_link--->
								</li>
								<cfif emp.department_name neq "">
									<li><i class="fa-li fa fa-sitemap"></i> #emp.department_name#
										<cfif emp.unit_name neq "">
											<ul class="fa-ul">
												<li><i class="fa-li fa fa-angle-double-down"></i> #emp.unit_name#</li>
											</ul>
										</cfif>
									</li>
								</cfif>
							</ul>
						</div>

						<div class="col-sm-5">
							<ul class="fa-ul">
								<li><i class="fa-li fa fa-envelope"></i> <a href="mailto:#emp.user_email#">#emp.user_email#</a></li>
								<cfif emp.address neq "">
									<li><i class="fa-li fa fa-map-marker"></i> #emp.address#</li>
								</cfif>
							</ul>
						</div>

						<div class="col-sm-3">

							<!---ul class="fa-ul">
							<cfloop query="view.user_role">
								<li>
									<i class="fa fa-key"></i> #role_Name#
									<div class="pull-right">
										<cf_buttons size="">
											<cf_button icon="times" help="Remove Employee from Role" size="btn-xs" class="close1" style="link" action="settings/User.cfc?method=RemoveRole&RoleId=#RoleId#&UserId=#emp.UserId#" redirect="employee.view@#emp.EmployeeId#"/>
										</cf_buttons>
									</div>
									<div class="clear"></div>

								</li>
								<div class="clear"></div>
							</cfloop>
							</ul--->

						</div>

					</div>


		        	<!---hr>
					<cf_buttons size="" group="" align="center">


						<cf_button url="employee.impersonate@#emp.employeeid#" title="Impersonate" icon=" icon-search1" />
						<cf_button url="employee.edit@#emp.employeeid#" title="Edit" icon=" icon-edit-user1"  />
						<cf_button
							modalurl="settings.user.add_role"
							urlparam="userid=#emp.UserId#&employeeid=#emp.EmployeeId#"
							title="Add New Role" icon=" icon-lock1"
						/>
						<cf_button
							icon="key"
							title="Reset Password"
							action="settings/User.cfc?method=ResetPassword&UserId=#emp.user_UserId#"
							responsemsg="#emp.User_Surname#\'s new login password is \'[[MSG]]\'"
						/>

						<cfif emp.ExitType eq "">

							<cf_button
								modalurl="employee.exit_employee"
								title="Exit Employee"
								icon=" icon-exit2"
								style="danger"/>

						</cfif>
					</cf_buttons>

		          <hr/--->





		  		</div>

			</div>

		    <div class="layout-main-right user" style="margin-top:0px;">

		        <cf_ttab
		        	justified="false"
		        	position="left"
		        	stacked
		        	menuClass="col-sm-3 layout-sidebar text-right"
		        	contentClass="col-sm-9  layout-main">

					<cf_titem title="Appraisals" icon="file-o" isactive >
						<cfinclude template="appraisals.cfm"/>
					</cf_titem>
					<cf_titem title="Career Profile" icon=" icon-run1" />
					<!--- if this person has subordinate ---->
					<cf_titem title="Subordinates" icon=" icon-users2" url="employee.view_subordinates"/>

		        </cf_ttab>

		    </div>
		</div>

		<div class="col-sm-1">

		</div>
	</div>

</cfoutput>