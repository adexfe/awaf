

<div class="box-info animated fadeInLeft">	

	<cf_grid model="settings.user" sortby="5" sortdirection="DESC" filter="#view.filter#">

		<cf_toolbar>
			<cf_search/>
			<cf_gbuttons>
				<cf_gbutton icon="user" title="New User" url="settings.user.new"/>
				<cfloop array="#view.buttons#" index="x" item="btn">
					<cf_gbutton icon="#btn.icon#" title="#btn.title#" url="#btn.url#" type="default" />
				</cfloop>
				<cf_gbutton icon="refresh" url="#url.current_page_url#" forcePageReload/>
			</cf_gbuttons>
		</cf_toolbar>

		<cf_columns>

			<cf_column name="UserId" hide/>
			<cf_column name="Surname" hide/>
			<cf_column name="OtherNames" caption="Name" template="oData[1]+' '+oData[2]"/>
			<cf_column name="Email"/>
			<cf_column name="UserType" caption="Type"/>

			<cfset no_ = wrapItem('oData[5]','label label-success')/>
			<cfset yes_ = wrapItem('oData[5]','label label-default')/>

			<cf_column name="Exit" caption="Exit" script="
				switch(oData[5])	{
					case 'No':
						return '#no_#';
					break;
					default: 
						return '#yes_#';
				}
			"/>

			<cf_commands>
				<cf_command title="view" url="settings.user.view"/>
			</cf_commands>

		</cf_columns>

	</cf_grid>
			
</div>

