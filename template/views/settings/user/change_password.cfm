<cfoutput>

	<cf_xform>

		<cf_xinput name="CurrentPassword" label="Current Password" required type="password"/>

		<cf_xinput name="NewPassword" label="New Password" required type="password"/>
		<cf_xinput name="ConfirmPassword" label="Confirm Password" required type="password"/>

		<input type="hidden" value="#request.user.userid#" name="userid"/>

		<div class="modal-footer">
			<cf_fSubmit value="Save" url="settings.User.cfc?method=SavePasswordChange" flashmessage="Your password was changed successfully"/>
 		</div>

	</cf_xform>


</cfoutput>