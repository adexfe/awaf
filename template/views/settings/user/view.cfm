<cfoutput>

	

	<div class="row">
		<div class="col-sm-8">
			<div class="box-info animated fadeInRight">
				
				<h2>User <span class="text-primary">###view.user.userid#</span></h2>
				
				
				<div class="additional-box">
					<cf_buttons>
						<cf_button icon="plus-circle" title="New" url="settings.user.new"/>
						<cf_button icon="retweet" title="Reset Password" action="settings/User.cfc?method=ResetPassword&UserId=#view.User.UserId#" responsemsg="#view.User.Surname#\'s new login password is \'[[MSG]]\'"/>
					</cf_buttons>
				</div> 

				<div class="row">
					<div class="col-sm-7">
						<h4>
							#view.user.Surname# #view.user.OtherNames# 
							<sup class="label label-#view.style.usertype#">#ucase(view.user.UserType)#</sup>
						</h4>
					</div>
					<div class="col-sm-5 text-right"> 
						<span class="label label-#view.style.active#">#ucase(view.IsActivetext)#</span>
					</div>
				</div>

						<!----cf_blockitem icon="user" label="Contact person" value="#view.Vendor.ContactPerson#" removeblank/---> 
 
		 
			</div>
		</div>

		<div class="col-sm-4">
			<div class="box-info animated fadeInRight">
				
				<h2>Security Information</h2>
				
				
				<div class="additional-box">
					<cf_buttons>
						<cf_button icon="plus-circle" title="Add Role" modalTitle="Add New Role to #view.User.Surname#" modalurl="settings.user.add_role" urlparam = "userid=#view.User.UserId#"/>
					</cf_buttons>
				</div> 

				<b>System Role</b>
				<ul class="fa-ul">
				<cfloop query="view.user_role">
					<li>
						<i class="fa fa-key"></i> #role_Name# 
						<cf_buttons>
							<cf_button icon="times" title="" action="settings/User.cfc?method=RemoveRole&RoleId=#RoleId#&UserId=#view.user.UserId#" redirect="settings.user.view@#view.user.userid#"/>
						</cf_buttons>

					</li>
					<div class="clear"></div>
				</cfloop>
				</ul>
				<!---b>Last logon date:</b>
		 		Comming soon...--->
			</div>
		</div>
	</div>
 

</cfoutput>