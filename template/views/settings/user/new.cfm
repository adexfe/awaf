

<div class="box-info animated fadeInLeft">

	<h2>New User</h2>

	<cf_xform>

		<cf_frow>
			<cf_xinput name="Surname" required/>
			<cf_xinput name="OtherNames" label="Other names"/>			
		</cf_frow>

		<cf_frow>
			<cf_xinput name="Email" required type="email" size="col-xs-6 col-sm-4" help="This email will be the login username"/>
			<cf_xinput name="PersonalEmail" label="Presonal Emal" help="This email will be use as gravata passport" type="email" size="col-xs-6 col-sm-5"/>
			<cf_option name="UserType" required value="Staff,Client" type="radio" size="col-xs-12 col-sm-3" label="User type"/>		
		</cf_frow>

		<hr/>

		<cf_fSubmit value="Create" url="settings/User.cfc?method=Save" clearform/>

	</cf_xform>

</div>