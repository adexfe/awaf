<cfoutput>

    <div class="layout-main-right" style="margin-top:-20px;">

        <cf_ttab justified="false" position="left" stacked menuClass="col-sm-2 layout-sidebar" contentClass="col-sm-10  layout-main">
            <!---cf_titem title="Location" icon="map-marker" url="settings.location.list"/--->
            <cf_titem title="Company Location" icon="building-o" url="settings.Company_Location.list"/>
            <cf_titem title="Appraisal Period" isActive icon="calendar-o">
                <cfinclude template="appraisal_period/list.cfm"/>
            </cf_titem>

            <cf_titem title="Objective Group" icon="list" url="settings.appraisal_objective_group.list"/>
            <cf_titem title="Global Objectives" icon="globe" url="settings.global_objective.view"/>
            <cf_titem title="Quantitative KPI Scores" icon="cube" url="settings.quantitative_kpi_score.list"/>
            <cf_titem title="Performance Factor" icon=" icon-run1" url="settings.PF.list"/>
            <cf_titem title="Department" icon="sitemap" url="settings.department.list"/>
            <cf_titem title="Job Title" url="settings.job_title.list" icon="asterisk"/>
            <cf_titem title="Current Job Grade" url="settings.job_level.list" icon="level-up"/>
            <cf_titem title="Grade Class" icon="level-up" url="settings.grade_level.list"/>
            <cf_titem title="Exit Type" url="settings.employee_exit_type.list" icon=" icon-exit2"/>
            <cf_titem title="Training" url="settings.training.list" icon="graduation-cap"/>
            <cf_titem title="Rating" url="settings.rating.list" icon=" icon-rank2"/>
            <cf_titem title="Rating Template" url="settings.rating_template.list" icon="sliders"/>
            <cf_titem title="Staff Roles" icon="unlock-alt" url="settings.role.staff_list"/>

        </cf_ttab>

    </div>

</cfoutput>