<cfscript>

	// core model
	application.model.USER_LOGIN 					= 'settings.UserLogin';
	application.model.USER_ROLE 					= 'settings.UserRole';
	application.model.USER 							= 'settings.User';

	application.model.ROLE 							= 'settings.Role';
	application.model.ROLE_PERMISSION 			= 'settings.RolePermission';

	application.model.MODULE 						= 'host.Module';
	application.model.PAGE 							= 'host.Page';

	application.datasource.main 			= this.datasource;

</cfscript>
