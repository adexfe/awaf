component extends="template.awaf.com.Controller"   {

	public struct function list()	{

		var local = {}

		local.view = {
			filter 	: "user.UserId!:1",
			buttons : [
				buildStaffUsers(), buildClientUsers()
			]
		}

		return local.view;
	}

	public struct function staffList()	{

		var local = {}

		local.view = {
			filter 	: "user.UserId!:1 AND user.UserType:[Staff]",
			buttons : [
				buildAllUsers(), buildClientUsers()
			]
		}

		return local.view;
	}

	public struct function clientList()	{

		var local = {}
		local.view = {
			filter 	: "user.UserId!:1 AND user.UserType:[Client]",
			buttons : [
				buildAllUsers(), buildStaffUsers()
			]
		}

		return local.view;
	}

	private struct function buildAllUsers()	{

		return 	struct(icon:'users', title='All Users', url:'settings.user.list');
	}

	private struct function buildClientUsers()	{

		return 	struct(icon:'list', title='Client Users', url:'settings.user.client_list');
	}

	private struct function buildStaffUsers()	{

		return 	struct(icon:'list', title='Staff Users', url:'settings.user.staff_list');
	}

	public any function view(struct data)	{

		var v = structnew();
		data.key = val(data.key);
		v.usr = model('settings.user');

		v.view.user = v.usr.findByKey(data.key);
		v.view.style.usertype = 'success';
		if(v.view.user.UserType is 'Client')	{
			v.view.style.usertype = 'danger';
		}
		v.view.IsActiveText = "Active";
		v.view.style.active = 'success';
		if(v.view.user.Exit)	{
			v.view.IsActiveText = "Not Active";
			v.view.style.active = 'default';
		}
		// get user role
		v.view.user_role = model('settings.UserRole').join('settings.Role').findByUser(data.key);

		return v.view;
	}

	public struct function new(struct data)	{
		var local = {}

		local.view = {}

		return local.view;
	}

	public any function addRole(struct data)	{

		var v = structnew();
		var role = model('settings.Role');
		data.userid = val(data.userid);
		param name="data.employeeid" default="0";
		data.employeeid = val(data.employeeid);

		v.ur = model('settings.UserRole').findAll(where='user_role.UserId='&data.userid);
		v.u = model('settings.User').findAll(where='user.UserId='&data.userid);
		v.roleids = v.ur.columnData('RoleId').toList();
		if(v.roleids is '')	{
			v.roleids = 0;
		}
		// get all role that the user does not have yet
		var h_r = role.getHighestHierarchy(request.user.roleids);
		v.role = role.findAll(
			where:'role.RoleId NOT IN (#v.roleids#) AND role.Type = "#v.u.UserType#" AND role.Hierarchy> #h_r#'
		);
		v.view.roleids = v.role.columnData('RoleId').toList('`');
		v.view.rolenames = v.role.columnData('Name').toList('`');
		v.view.userid = data.userid;
		v.view.employeeid = data.employeeid;

		return v.view;
	}


	public struct function changePassword()	{

		var local = {}

		local.view = {}

		return local.view;
	}

	remote string function save() returnformat='json'	{

		var local = {}

		local.user = model(application.model.USER).new(form).save();

		return serializejson(local.user);
	}

	remote string function savePasswordChange() returnformat='json'	{

		var local = {}
		// confirm the user password account
		local.user_login = model(application.model.USER_LOGIN).findOne(where='user_login.UserId='&form.userid);
		if(form.NewPassword eq form.ConfirmPassword)	{

			if(hmac(form.CurrentPassword, local.user_login.loginid) eq local.user_login.PasswordKey)	{

				local.user_login.PasswordKey = hmac(form.NewPassword, local.user_login.loginid);
				local.user_login.save();

			}
			else 	{

				abort "Your current password is wrong!";

			}

		}
		else 	{

			abort "Password mismatch";

		}

		return serializejson(local.user_login);
	}

	remote string function saveAddedRoles() returnformat='json'	{

		var v = structnew();
		v.user_role = model(application.model.USER_ROLE);
		loop list=form.RoleIds item="roleid"	{
			form.RoleId = roleid;
			v.user_role.new(form).save();
		}

		return serializejson(v.user_role);
	}

	remote void function RemoveRole(required numeric roleid, required numeric userid) returnformat='json'	{

		var user_role = model(application.model.USER_ROLE).deleteAll(where='UserId='&arguments.userid & ' AND RoleId='&arguments.roleid);

		return;
	}

	remote string function ResetPassword(required numeric userid) returnformat='plain'	{

		var local = structnew();
		local.gen_password = randRange(1000,9999);

		// get login id, if not found, create a new one;
		transaction {

			local.ul = model(application.model.USER_LOGIN).findOne(where='user_login.UserId='&arguments.userid);
			local.u = model(application.model.USER).findQ(arguments.userid)

			if(local.ul.loginid is 0)	{

				local.ul = local.ul.new( struct(UserId:arguments.userid) ).save();

			}

			local.ul.PasswordKey = hmac(local.gen_password, local.ul.loginid);
			local.ul.save();

		}

		// send a mail to the owner of the account
		sendMail(
			to 		: local.u.email,
			subject : 'PMS login Password',
			body 	: "

				Hi #local.u.Surname#,
				<p>Please find below your new password<br/>
				================<br/>
				#local.gen_password#<br/>
				================<br/>
				Visit #application.site.url# to login
				</p>
				Thank you

			"
		);

		return local.gen_password;
	}

	public string function forgotPassword(required string username, required string question, required string answer)	{

		var local = structnew();
		local.gen_password = randRange(1000,9999);

		if(arguments.question eq arguments.answer)	{

			local.u = model(application.model.USER).findOne(where='user.Email="'&arguments.username&'"');
			if(val(local.u.userid))	{

				local.ul = model(application.model.USER_LOGIN).findOne(where : 'user_login.UserId = ' & local.u.userid);
				local.ul = local.ul.new(
					struct(
						UserLoginId	: val(local.ul.LoginId),
						UserId 		: local.u.userid
					)
				).save();
				local.ul.PasswordKey = hmac(local.gen_password, local.ul.loginid);
				local.ul.save();

				// send a mail to the owner of the account
				sendMail(
					to 		: local.u.email,
					subject : 'PMS login Password',
					body 	: "

						Hi #local.u.Surname#,
						<p>Please find below your new password<br/>
						================<br/>
						#local.gen_password#<br/>
						================<br/>
						Visit #application.site.url# to login
						</p>
						Thank you

					"
				);

				local.msg = "Please check your email for your new password";

			}
			else 	{

				local.msg = "Sorry! We can't find your username.";

			}

		}
		else 	{

			local.msg = "Wrong number provided below, please try again";

		}

		return local.msg;
	}

	public struct function profile(struct data)	{

  		var local = {}
  		local.empid = val(arguments.data.key);

  		local.manager = model(application.model.EMPLOYEE);

  		local.view.employee = model(application.model.EMPLOYEE)
  							  .join(application.model.USER)
  							  .ljoin(application.model.COMPANY_LOCATION)
  							  .ljoin(application.model.DEPARTMENT)
  							  .ljoin(application.model.JOB_TITLE)
  							  .ljoin(application.model.JOB_LEVEL)
  							  .ljoin(application.model.UNIT).findQ(request.user.employeeid);

  		local.view.manager = local.manager.findQ(val(local.view.employee.managerid));

  		// find the role of the user / employee
  		local.view.user_role = model(application.model.USER_ROLE)
  								.join(application.model.ROLE)
  								.findByUser(local.view.employee.UserId);

  		return local.view;

	}

}
