component extends="template.awaf.com.Controller"  {

	public any function login(required string username, required string password)	{

		arguments.username = trim(lcase(arguments.username));
		var v = structnew();
		v.user_login = model(application.model.USER_LOGIN)
							.join(application.model.USER)
							.findOne(where : 'user.Email="#arguments.username#"');
		// hash the password to get the key
		v.key_ = hmac(arguments.password, v.user_login.loginid);
		v.view.IsLogin = false;
		v.view.message = 'Wrong Username or password';
		if(v.key_ eq v.user_login.PasswordKey)	{
			// get all the roles for the user
			v.userRole = model(application.model.USER_ROLE).findAll('user_role.UserId = ' & v.user_login.userid);
			v.view.RoleIds = v.userRole.columnData('RoleId').tolist();
			v.view.UserId = v.user_login.userid;
			v.view.LoginId = v.user_login.loginId;
			v.view.Email = v.user_login.user.Email;
			v.view.PersonalEmail = v.user_login.user.PersonalEmail;
			v.view.Name = v.user_login.user.Surname & ' ' & v.user_login.user.OtherNames;
			v.view.UserType = v.user_login.user.UserType;
			// get all permissions
			v.role_perm = model(application.model.ROLE_PERMISSION).findAll(where='role.RoleId IN (' & v.view.RoleIds & ')');
			v.view.PageURLs = v.role_perm.columnData('page_URL').tolist() & ',' & v.role_perm.columnData('module_URL').tolist();
			v.view.PageURLs = ArrayToList(ListToArray(v.view.PageURLs));
			// add static pages
			v.staic_pages = ',settings.user.profile,settings.user.change_password,settings.user.edit_profile';
			v.view.PageURLs = v.view.PageURLs & v.staic_pages;
			v.view.IsLogin = true;
			v.view.message = '';
	
		}

		return v.view;
	}

	public any function impersonate(required string username)	{

		arguments.username = trim(lcase(arguments.username));
		var v = structnew();
		v.user_login = model(application.model.USER_LOGIN)
						.join(application.model.USER)
						.findOne(where='user.Email="' & arguments.username & '"');
		// hash the password to get the key


			// get all the roles for the user
			v.userRole = model(application.model.USER_ROLE).findAll('user_role.UserId = ' & v.user_login.userid);
			v.view.RoleIds = v.userRole.columnData('RoleId').tolist();
			v.view.UserId = v.user_login.userid;
			v.view.LoginId = v.user_login.loginId;
			v.view.Email = v.user_login.user.Email;
			v.view.PersonalEmail = v.user_login.user.PersonalEmail;
			v.view.Name = v.user_login.user.Surname & ' ' & v.user_login.user.OtherNames;
			v.view.UserType = v.user_login.user.UserType;
			// get all permissions
			v.role_perm = model(application.model.ROLE_PERMISSION).findAll(where='role.RoleId IN (' & v.view.RoleIds & ')');
			v.view.PageURLs = v.role_perm.columnData('page_URL').tolist() & ',' & v.role_perm.columnData('module_URL').tolist();
			v.view.PageURLs = ArrayToList(ListToArray(v.view.PageURLs));
			// add static pages
			v.staic_pages = ',settings.user.profile,settings.user.change_password,settings.user.edit_profile';
			v.view.PageURLs = v.view.PageURLs & v.staic_pages;
			v.view.IsLogin = true;
			v.view.message = '';
			// if staff, get all the clientid the user can access
			//if(v.user_login.user.UserType eq 'Client')	{
			/*v.clnt = model('settings.UserClient').findByUser(v.user_login.userid);
			v.view.ClientIds = v.clnt.columnData('ClientId').toList();
			if(v.view.ClientIds is '')	{
				v.view.ClientIds = 0;
			}*/
			//}
			// get employee details
			v.employee = model(application.model.EMPLOYEE).findAllByUser(v.view.UserId);
			v.view.employeeid = val(v.employee.employeeid);
			v.view.managerid = val(v.employee.managerid);
			v.view.employee_type = v.employee.EmployeeType;

			// get company id
			var loc = model(application.model.COMPANY_LOCATION)
							.findQ(val(v.employee.CompanyLocationId));
			v.view.CompanyId = val(loc.CompanyId);
			v.view.CompanyLogo = loc.company_Logo;

		return v.view;
	}
}
