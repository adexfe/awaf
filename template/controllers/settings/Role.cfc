component extends="template.awaf.com.Controller"   {

	public any function staffList()	{

		return listRole('Staff');
	}

	public any function clientList()	{

		return listRole(application.model.CLIENT);
	}

	public void function view(){

	}

	private any function listRole(required string type)	{

		var view = structnew();
		var mRolePerm = model(application.model.ROLE_PERMISSION);
		var page_count = 'page_count';
		if(arguments.type is 'Staff')	{
			view.Roles = model(application.model.ROLE).findAllStaff();
		}
		else 	{
			view.Roles = model(application.model.ROLE).findAllClient();
		}

		view.id = getRandomVariable();

		if(!view.Roles.columnExists(page_count))	{
			view.Roles.addColumn(page_count);
			loop query=view.Roles 	{
				view.Roles.setCell(page_count, mRolePerm.findAll(where='role_permission.RoleId='&view.Roles.RoleId).Recordcount, view.Roles.currentrow);
			}
		}

		return view;
	}

	public struct function viewPermissions(required struct data)	{

		var view = structnew();
		data.key = val(data.key);
		view.roleid=data.key;
		view.permissions = model(application.model.ROLE_PERMISSION).findAll(where='role_permission.RoleId='&data.key, order='module_position,page_position');

		return view;
	}

	public struct function editpage(required struct data)	{

		var view = structnew();
		data.key = val(data.key);
		view.id = getRandomVariable();
		view.rowid = getRandomVariable();
		view.Page = model(application.model.PAGE).findByKey(data.key);
		view.Modules = model(application.model.MODULE).findAll();

		return view;
	}

	public void function new()	{}

	public any function edit(required struct data)	{

		var role = model(application.model.ROLE).findByKey(data.key);

		return role;
	}

	public struct function newPermission(required struct data)	{

		var view = structnew();
		view.roleid = val(data.key);
		view.id = getRandomVariable();
		view.rowid = getRandomVariable();
		var role = model(application.model.ROLE).findQ(view.roleid);
		view.Modules = model(application.model.MODULE).findAll(where='ViewBy="' & role.Type & '"');

		return view;
	}

	remote string function save() returnformat="json"	{

		var role = model(application.model.ROLE).new(form).save();

		return serializejson(role);
	}

	remote string function getPagesNotInRole(required numeric moduleid, required numeric roleid) returnformat="json"	{

		// get all the pages in the role
		var qRole = model(application.model.ROLE_PERMISSION).findAll(where='role_permission.RoleId=' & arguments.roleid);
		var pageIds = ArrayToList(ListToArray(qRole.columnData('PageId').toList()));
		if(pageIds is '')	{pageIds = '0'}
		var qPages = model(application.model.PAGE).findAll(where='page.ModuleId = ' & arguments.moduleid & ' AND page.PageId NOT IN (' & pageids & ')', order='page.Position');
		var aPages = arrayNew(1);

		for(x in qPages) {
			var sPage = structnew();
			sPage.PageId = x.PageId;
			sPage.Name = x.Name;

			aPages.append(sPage);
		}

		return serializejson(aPages);
	}

	remote string function addPermission() returnformat="json"	{

		var rp = model(application.model.ROLE_PERMISSION);
		if(form.PageIds is '')	{
			rp.new(form).save();
		}
		else{
			loop list=form.PageIds item="page_id"	{
				form.PageId = val(page_id);
				rp.new(form).save();
			}
		}

		return serializejson(rp);
	}

	remote void function deletePermission( required numeric permissionid)	{

		model(application.model.ROLE_PERMISSION).findByKey(arguments.permissionid).delete();

	}

}
