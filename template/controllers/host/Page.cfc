component extends="template.awaf.com.Controller"   {


	remote string function save() returnformat="json"	{

		var p = model(application.model.PAGE).new(form).save();

		return serializejson(p);
	}

}
