component extends="template.awaf.com.Controller"   {

	public any function list()	{

		var view = structnew();
		var mPage = model(application.model.PAGE);
		var page_count = 'page_count';

		view.moduleQueryStaff = model(application.model.MODULE).findAllStaff();
		view.moduleQueryClient = model(application.model.MODULE).findAllClient();
		view.id = getRandomVariable();

		if(!view.moduleQueryStaff.columnExists(page_count))	{
			view.moduleQueryStaff.addColumn(page_count);
			loop query=view.moduleQueryStaff 	{
				view.moduleQueryStaff.setCell(page_count, mPage.findAll(where='page.ModuleId='&view.moduleQueryStaff.ModuleId).Recordcount, view.moduleQueryStaff.currentrow);
			}
		}

		if(!view.moduleQueryClient.columnExists(page_count))	{
			view.moduleQueryClient.addColumn(page_count);
			loop query=view.moduleQueryClient 	{
				view.moduleQueryClient.setCell(page_count, mPage.findAll(where='page.ModuleId='&view.moduleQueryClient.ModuleId).Recordcount, view.moduleQueryClient.currentrow);
			}
		}

		return view;
	}

	public struct function viewpages(required struct data)	{

		var view = structnew();
		data.key = val(data.key);
		view.moduleid=data.key;
		view.Pages = model(application.model.PAGE).findAll(where='page.ModuleId='&data.key, order='page.Position');


		return view;
	}

	public struct function editpage(required struct data)	{

		var view = structnew();
		data.key = val(data.key);
		view.id = getRandomVariable();
		view.rowid = getRandomVariable();
		view.Page = model(application.model.PAGE).findByKey(data.key);
		view.Modules = model(application.model.MODULE).findAll();

		return view;
	}

	public struct function newpage(required struct data)	{

		var view = structnew();
		view.moduleid = val(data.key);
		view.id = getRandomVariable();
		view.rowid = getRandomVariable();
		view.Modules = model(application.model.MODULE).findAll();

		return view;
	}

	remote string function save() returnformat="json"	{

		var p = model(application.model.PAGE).new(form).save();

		return serializejson(p);
	}

}
