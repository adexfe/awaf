<cfparam name="_islogin" type="boolean" default="true">
<cfif isDefined('form.username')>

	<cfset user_login = controller(application.model.USER_LOGIN).login(form.username,form.password)/>
	<cflock scope="Session" throwontimeout="true" timeout="10" type="exclusive">
		<cfset session.user = user_login/>
	</cflock>
	<cflock scope="Session" throwontimeout="true" timeout="10" type="readonly">
		<cfset request_url = session.request_url/>
	</cflock>

	<cfset _islogin = user_login.islogin/>
	<cfif user_login.islogin>
		<cflocation url="#request_url#" addtoken="false"/>
	</cfif>

</cfif>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{project-name}} | Login</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <cfinclude template = "inc/css.cfm">

  </head>
  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <b>{{project-name}}</b>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
        <cfif !_islogin>
           <div class="alert alert-danger text-center">
             Wrong username or password
           </div>
			  <br/>
        </cfif>
        <form action="" method="post">
          <div class="form-group has-feedback">
            <input type="email" class="form-control" name="username" placeholder="Email">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" name="password" placeholder="Password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <hr/>
          <div class="row">
            <div class="col-xs-8">
              <a href="#">I forgot my password</a>
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div><!-- /.col -->
          </div>
        </form>


      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

   <cfinclude template = "inc/js.cfm">

  </body>
</html>
