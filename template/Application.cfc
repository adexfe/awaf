component output="true"  {

	include "/awaf/source/config.cfm";

	structAppend(url, createObject("component", "awaf.source.GlobalFunction"));


	public void function onApplicationEnd(struct ApplicationScope=structNew()) {

		return;
	}

	public boolean function onApplicationStart() {

		application.site.url = "https://localhost:" & cgi.SERVER_PORT & "/template/";
		include "../source/settings.cfm";
		include "app_variables.cfm";

		return true;
	}

	public boolean function onRequest(required string targetPage)	{

		include "../source/on_request.cfm";

		if (request.include_page is 'views/secure_page.cfm') 	{
			if(
				(findnocase('login.cfm', arguments.targetPage)) ||
				(findnocase('index.cfm', arguments.targetPage)) ||
				(findnocase('forget.cfm', arguments.targetPage))||
				(findnocase('logout.cfm', arguments.targetPage))||
				(findnocase('ajax.cfm', arguments.targetPage))
			)
			{

				include lcase(arguments.targetPage);
			}
			else 	{

				include 'views/secure_page.cfm';
			}
		}
		else 	{

			include lcase(arguments.targetPage);
		}

		return true;
	}

	public void function onSessionEnd(required struct SessionScope, struct ApplicationScope=structNew()) {

		return;
	}

	public void function onSessionStart() {

		return;
	}

}
