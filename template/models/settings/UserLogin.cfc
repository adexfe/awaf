component extends="template.awaf.com.Model" {

	public UserLogin function init(string table_name='')	{
		// set table 1st
		if(arguments.table_name is '')	{
			arguments.table_name ='user_login';
		}
		// call the super init()
		this = super.init(arguments.table_name);
		// call association 3rd

		return this;
	}

	public query function findByUser(required string userid)	{

		return findAll(
			where:'user_login.UserId = ' & arguments.userid
		);
	}

	public void function deleteUserLogin(required numeric userid)	{

		var usr_logins = this.findByUser(arguments.userid);
		if(usr_logins.recordcount)	{
			this.deleteAllInKey(usr_logins.columnData('LoginId').toList());
		}
	}

}
