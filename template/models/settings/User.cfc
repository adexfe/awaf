component extends="template.awaf.com.Model" {

	public User function init(string table_name='')	{
		// set table 1st
		if(arguments.table_name is '')	{
			arguments.table_name ='user';
		}
		// custom select
		variables.customSelect = "CONCAT(user.Surname,' ',user.OtherNames) AS Name"
		// call the super init()
		this = super.init(table_name=arguments.table_name, custome_select_clause=variables.customSelect );
		// call association 3rd
		//mustHaveOne("currency");
		return this;
	}

	public any function buildRelationships(boolean deep_rel = false)	{

		mustHaveOne(application.model.USER_LOGIN,'',deep_rel);

		return this;
	}

	public query function findActiveUsers()	{

		return findAll(where='user.Exit = "No"');
	}

	public query function findActiveStaff()	{

		return findAll(
			where='user.Exit = "No" AND user.UserType="Staff"');
	}

	public query function findByClient(required numeric clientid)	{

		return model('settings.UserClient').findByClient(arguments.clientid);
	}

	public string function getActiveStaffEmail()	{

		local.rt = '';
		loop query=findActiveUsers() 	{
			local.rt = listAppend(local.rt,'"' & email & '"');
		}

		return local.rt;
	}

}
