component extends="template.awaf.com.Model" {

	public UserRole function init(string table_name='')	{
		// set table 1st
		if(arguments.table_name is '')	{
			arguments.table_name ='user_role';
		}
		// call the super init()
		this = super.init(arguments.table_name);
		// call association 3rd
		//mustHaveOne("settings.user");
		hasMany("settings.Roles");

		return this;
	}

	public query function findByUser(required numeric userid)	{

		return this.findAll(
			where : 'user_role.UserId=' & arguments.userid
		);
	}

	public query function findByUserAndRole(required numeric userid, required numeric roleid)	{

		return this.findAll(
			where : 'user_role.UserId=#arguments.userid# AND user_role.RoleId=#arguments.roleid#'
		);
	}

	public void function deleteUserRole(required numeric userid)	{

		var usr_roles = this.findByUser(arguments.userid);
		if(usr_roles.recordcount)	{
			this.deleteAllInKey(usr_roles.columnData('UserRoleId').toList());
		}
	}


	public string function getEmailListByRole(required string role)	{

		var ur = this;
		ur = ur.join(application.model.ROLE)
				.join(application.model.USER);
		ur = ur.findAll(
			where : 'role.Name = "' & arguments.role & '"'
		);

		return ur.columnData('user_Email').toList();
	}

	public string function getEmailListByPermission(required string page_url)	{

		// get all permission
		var rp = model(application.model.ROLE_PERMISSION)
						.findByPage(arguments.page_url);

		var ur = this;
		ur = ur.join(application.model.ROLE)
				.join(application.model.USER);
		ur = ur.findAll(
			where : 'role.RoleId = ' & rp.RoleId
		);

		return ur.columnData('user_Email').toList();
	}
}
