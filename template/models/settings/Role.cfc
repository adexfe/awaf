component extends="template.awaf.com.Model" {

	public Role function init(string table_name='')	{
		// set table 1st
		if(arguments.table_name is '')	{
			arguments.table_name ='role';
		}
		// call the super init()
		this = super.init(arguments.table_name);
		// call association 3rd

		return this;
	}

	public query function findAllStaff()	{

		return findAll(where='role.Type="staff" AND role.Name<>"Host"');
	}

	public struct function getRoleList()	{

		var c = this.findAll(where:'role.Name<>"Host"');
 		var rt = {}

 		rt.ids = c.columnData('RoleId').toList('`');
 		rt.names = c.columnData('Name').toList('`');

 		return rt;
	}

	public numeric function getHighestHierarchy(required string role_ids)	{

		return this.min(
			field : 'Hierarchy',
			where : 'role.RoleId IN (#arguments.role_ids#)'
		);
	}

	public struct function getRoleListByHierarchy(required string role_ids)	{

		var roles = this.findAll(where : 'Hierarchy > #getHighestHierarchy(arguments.role_ids)#');

 		var rt = {}

 		rt.ids = roles.columnData('RoleId').toList('`');
 		rt.names = roles.columnData('Name').toList('`');

 		return rt;
	}

	public query function findAllClient()	{

		return findAll(where='role.Type="client"');
	}
}
