component extends="template.awaf.com.Model" {

	public RolePermission function init(string table_name='', string order='')	{
		// set table 1st
		if(arguments.table_name is '')	{
			arguments.table_name ='role_permission';
		}

		// set the way to always order the result in a query
		if(arguments.order is '')	{
			arguments.order ='module_Position,page_Position';
		}		// call the super init()
		this = super.init(arguments.table_name);
		// call association 3rd
		mustHaveOne(application.model.ROLE);
		mayHaveOne(application.model.PAGE,'', true);
		//mustHaveOne(application.model.MODULE);

		return this;
	}

	public query function findByRole(required numeric roleid)	{

		return findAll(where='role_permission.RoleId = ' & arguments.roleid);

	}

	public query function findByPage(required string page_url)	{

 		// need to join page
		return findAll(where='page.URL = "' & arguments.page_url & '"');
	}

	public query function findModulesByRole(required numeric roleid)	{

		var moduleIds = valueList(findAll(where='role_permission.RoleId = ' & arguments.roleid).ModuleId);

		return model(application.model.MODULE).findByKeyIn(moduleIds);

	}

	public query function findModulesInRole(required string roleids)	{

		var moduleIds = findAll(
			where = 'role_permission.RoleId IN (' & arguments.roleids & ')'
		).columnData('page_ModuleId').toList();

		return model(application.model.MODULE).findByKeyIn(keys = moduleIds, order = 'module.Position, module.Name');

	}

	public query function findPagesByRoleAndModule(required string roleids, required numeric moduleid)	{

 		if(arguments.roleids is '')	{
 			arguments.roleids = 0;
 		}
		var pagesIds = findAll(where='role_permission.RoleId IN (' & arguments.roleids & ') AND page.moduleId = ' & arguments.moduleid).columnData('PageId').toList();

		return model(application.model.PAGE).findByKeyIn(keys=pagesIds, where='page.ShowInMenu = "Yes"', Order='page.Position');

	}




}
