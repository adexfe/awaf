component extends="template.awaf.com.Model" {

	public Page function init(string table_name='')	{
		// set table 1st
		if(arguments.table_name is '')	{
			arguments.table_name ='page';
		}
		// call the super init()
		this = super.init(arguments.table_name);
		// call association 3rd
		mustHaveOne(application.model.MODULE);

		return this;
	}

}
