component extends="template.awaf.com.Model" {

	public Module function init(string table_name='')	{
		// set table 1st
		if(arguments.table_name is '')	{
			arguments.table_name ='module';
		}
		// call the super init()
		this = super.init(arguments.table_name);
		// call association 3rd

		return this;
	}

	public query function findAllStaff()	{

		return findAll(where='module.ViewBy="Staff"');
	}

	public query function findAllClient()	{

		return findAll(where='module.ViewBy="Client"');
	}
}
