<cfoutput>
<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>{{project-name}} : {{project-desc}}</title>
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

      <cfinclude template="inc/css.cfm"/>

   </head>
   <!-- ADD THE CLASS fixed TO GET A FIXED HEADER AND SIDEBAR LAYOUT -->
   <!-- the fixed layout is not compatible with sidebar-mini -->
   <body class="hold-transition skin-red fixed sidebar-mini">

      <!-- Modal Dialog -->
      <div class="modal custom fade" id="__app_modal" tabindex="-1" role="dialog" aria-labelledby="__app_modal_title" aria-hidden="true">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title" id="__app_modal_title"></h4>
               </div>
               <div class="modal-body" id="__app_modal_body">
                  ...
               </div>
            </div>
         </div>
      </div>

    <!-- Site wrapper -->
   <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini">#left("{{project-name}}",2)#</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg">{{project-name}}</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="##" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>

          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <li class="dropdown messages-menu">
                <a href="##" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-envelope-o"></i>
                  <span class="label label-success">4</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have 4 messages</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      <li><!-- start message -->
                        <a href="##">
                          <div class="pull-left">
                             <img class="img-circle" src="https://www.gravatar.com/avatar/#lcase(hash(request.user.personalEmail))#?s=160&d=mm"/>
                            <!---img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"--->
                          </div>
                          <h4>
                            Support Team
                            <small><i class="fa fa-clock-o"></i> 5 mins</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li><!-- end message -->
                    </ul>
                  </li>
                  <li class="footer"><a href="##">See All Messages</a></li>
                </ul>
              </li>
              <!-- Notifications: style can be found in dropdown.less -->
              <li class="dropdown notifications-menu">
                <a href="##" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-bell-o"></i>
                  <span class="label label-warning">10</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have 10 notifications</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      <li>
                        <a href="##">
                          <i class="fa fa-users text-aqua"></i> 5 new members joined today
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li class="footer"><a href="##">View all</a></li>
                </ul>
              </li>
              <!-- Tasks: style can be found in dropdown.less -->
              <li class="dropdown tasks-menu">
                <a href="##" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-flag-o"></i>
                  <span class="label label-danger">9</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have 9 tasks</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      <li><!-- Task item -->
                        <a href="##">
                          <h3>
                            Design some buttons
                            <small class="pull-right">20%</small>
                          </h3>
                          <div class="progress xs">
                            <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">20% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li><!-- end task item -->
                    </ul>
                  </li>
                  <li class="footer">
                    <a href="##">View all tasks</a>
                  </li>
                </ul>
              </li>
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="##" class="dropdown-toggle" data-toggle="dropdown">
                  <img class="user-image" src="https://www.gravatar.com/avatar/#lcase(hash(request.user.personalEmail))#?s=160&d=mm"/>
                  <span class="hidden-xs">#request.user.name#</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                     <img class="img-circle" src="https://www.gravatar.com/avatar/#lcase(hash(request.user.personalEmail))#?s=160&d=mm"/>
                    <p>
                      #request.user.name# - Web Developer
                      <small>Member since Nov. 2012</small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <li class="user-body">
                    <div class="col-xs-4 text-center">
                      <a href="##">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="##">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="##">Friends</a>
                    </div>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="##" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="logout.cfm" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <li>
                <a href="##" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>
            </ul>
          </div>

        </nav>
      </header>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">

              <img src="assets/img/logo.png" width="150px" style="margin-left: auto; margin-right:auto;display: block;"/>

          </div>
          <!-- search form -->
          <form action="##" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <cfinclude template = "inc/menu.cfm">

          <!---ul class="sidebar-menu">
            <li class="treeview">
              <a href="##">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="../../index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
                <li><a href="../../index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
              </ul>
            </li>
            <li>
              <a href="../widgets.html">
                <i class="fa fa-th"></i> <span>Widgets</span> <small class="label pull-right bg-green">new</small>
              </a>
            </li>
         </ul--->
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Fixed Layout
            <small>Blank example to the fixed layout</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="##"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="##">Layout</a></li>
            <li class="active">Fixed</li>
          </ol>
       </section>

        <!-- Main content -->
        <section class="content" id="app_content">

           <script type="text/javascript">
             /* REQUIRED *********/
             view_path = 'views/';
             /*=========*/
           </script>

             <cfinclude template="inc/js.cfm"/>

           <!-- ============================================================== -->
           <!-- Start Content here -->
           <!-- ============================================================== -->


            <cfset app_module_id = listFirst(url.controller,'.')/>
            <cfif find('@',url.controller)>
               <cfset app_page_id = replace(listFirst(url.controller,'@'),'.','_','all') & '_' & url.key/>
            <cfelse>
               <cfset app_page_id = replace(url.controller,'.','_','all')/>
            </cfif>

            <div id="#app_module_id#" class="app_module">
               <div id="#app_page_id#" class="app_page">
                     <cfinclude template="#lcase(request.include_page)#"/>
               </div>
            </div>

           <!-- ============================================================== -->
           <!-- End content here -->
           <!-- ============================================================== -->


        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <footer class="main-footer">
        <div class="pull-right">
           Copyright &copy; #year(now())# {{project-name}}</a>. All rights reserved.
        </div>
      </footer>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
          <li><a href="##control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
          <li><a href="##control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <!-- Home tab content -->
          <div class="tab-pane" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Recent Activity</h3>

            <h3 class="control-sidebar-heading">Tasks Progress</h3>

          </div><!-- /.tab-pane -->
          <!-- Stats tab content -->
          <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div><!-- /.tab-pane -->
          <!-- Settings tab content -->
          <div class="tab-pane" id="control-sidebar-settings-tab">
            <form method="post">
              <h3 class="control-sidebar-heading">General Settings</h3>


              <h3 class="control-sidebar-heading">Chat Settings</h3>

              <!-- /.form-group -->

            </form>
          </div><!-- /.tab-pane -->
        </div>
      </aside><!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!---script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script--->
    <!---script src="../../plugins/fastclick/fastclick.min.js"></script---->
    <!--- AdminLTE for demo purposes
    <script src="../../dist/js/demo.js"></script--->
  </body>
</html>
</cfoutput>
