component 	{

	/*
		@model_path: this is used in the versioning
	*/
	public Model function init(
		required string table_name = '',
		string order='',
		string custome_select_clause='',
		string model_path='',
		string datasource = application.datasource.main)	{

		this.table_name = lcase(arguments.table_name)
		this.datasource = arguments.datasource
		this.sql_join = ""
		// reolve the table name and get table alias
		variables.table_alias = listLast(this.table_name, " ")
		variables.custome_select_clause = trim(arguments.custome_select_clause)
		this.table_name = listFirst(this.table_name, " ")

		//get all columns in the model
		if(!IsDefined("application[#arguments.datasource#][#this.table_name#]")) 	{
			dbinfo name="application.#arguments.datasource#.#this.table_name#" table = this.table_name type = "columns" datasource=this.datasource;
		}

		variables.table_structure = application[arguments.datasource][this.table_name]
		variables.columns = valueList(variables.table_structure.COLUMN_NAME)

		variables.cacheTimeSpan = createTimeSpan(0, 6, 0, 0)

		// build sql select
		variables.sql_select = ""
		loop list = variables.columns item="col"	{
			variables.sql_select = listAppend(variables.sql_select, variables.table_alias & "." & col)
		}
		// get primary key
		query name="qP" dbtype="query" 	{
			echo("SELECT COLUMN_NAME FROM variables.table_structure WHERE IS_PRIMARYKEY = 'YES'");
		}

		this._pk_field = qP.COLUMN_NAME
		this.KEY = this._pk_field

		// use this to store related info not in the DB

		// relationship variables
		this._relationship.hasOne = arrayNew(1)
		this._relationship.hasMany = arrayNew(1)

		// return all queries from finders here
		variables.finder_result = ""

		//settings
		variables.versioning_is_enabled = false
		variables.order = arguments.order
		if(arguments.model_path is not '')	{
			variables.model_path = arguments.model_path & '.'
		}
		else 	{
			variables.model_path = ''
		}

		// functions
		this.log = this.$log;
		this.Avg = this.$avg
		this.Max = this.$max
		this.Min = this.$min
		this.Sum = this.$sum
		this.Count = this.$count
		this.Find = this.findByKey
		this.FindIn = this.findByKeyIn
		this.IsEmpty = this.$isEmpty
		this.keyExists = this.fieldExists

		return this
	}

	/* check if primary key value exist */
	public boolean function $isEmpty()	{

		var rt =false
		if(this.getKeyValue() is 0)	{
			rt = true
		}

		return rt
	}

	public any function getModelInfo()	{

		return this
	}

	/* check if the field exist */
	public boolean function fieldExists(required string filed_name)	{

		var rt = false
		if(listFindNocase(variables.columns, filed_name))	{
			rt = true
		}

		return rt
	}

	private void function enableVersioning()	{

		variables.versioning_is_enabled = true

	}

	/* set the fields of the object using a query or struct */
	private void function setColumns(query query, struct struct = {})	{

		if(arguments.struct.isEmpty())	{
			// use query to build the column value
			loop list=variables.columns item="col"	{

				this[col] = trim(arguments.query[col])

				setfield(col)
			}
		}
		else {
			// use struct to build the column value : this is from the method in the controller
			loop list=variables.columns item="col"	{

				// check if field exist in struct

				if(arguments.struct.keyExists(col))	{
					this[col] = trim(arguments.struct[col])

					setfield(col)
				}
				else 	{//set field that does not exist in the struct
					//this[col] = ''
				}

				//setfield(col)

			}

		}
	}

	private void function setfield(required string col)	{
		// set other details about the field
		this.fields[arguments.col] = getFieldInfo(arguments.col)
		// make fields easily accesible
		switch(this.fields[arguments.col].type) 	{
			case "cf_sql_int": case "cf_sql_decimal":
				if(this[arguments.col] is '') {
					this[arguments.col]=val(this.fields[arguments.col].default);
				}
				else if(this[arguments.col] is 'NULL')	{
					this[arguments.col]=0;
				}
				else {
					this[arguments.col] = replace(this[arguments.col],',','','all');
				}
			break;
			case "cf_sql_date":
				this[arguments.col] = dateFormat(this[arguments.col],'yyyy-mm-dd');
			break;
			case "cf_sql_datetime":
				this[arguments.col] = dateFormat(this[arguments.col],'yyyy-mm-dd') & " " & timeFormat(this[arguments.col],'hh:mm tt');
			break;
			case "cf_sql_varchar":
				this[arguments.col] = this[arguments.col] is '' ? this.fields[arguments.col].default : this[arguments.col];
				if(this[arguments.col] is 'NULL') {this[arguments.col] = '';}
			break;
		}
	}

	/* set columns from other tables join with this one */
	private void function setJoinedColumns()	{

		var fld_name = ''

		loop array=this._relationship.hasone item="relation_ship"	{
			loop list=relation_ship.sql_columns item="col"	{

				fld_name = replace( listlast(col,' '),'.','_','all' )
				this[relation_ship.table_alias][listlast(fld_name,'_')] = variables.finder_result[fld_name]

			}
			// add included table
			if(isDefined("relation_ship.include"))	{
				loop array=relation_ship.include item="inc"	{
					loop list=inc.sql_columns item="col"	{

						fld_name = replace( listlast(col,' '),'.','_','all' )
						this[relation_ship.table_alias][listlast(inc.model_name,'.')][listlast(fld_name,'_')] = variables.finder_result[fld_name]

					}
				}
			}
		}

	}

	private struct function getFieldInfo(required string column)	{
		query name="q" dbtype="query" 	{
			echo("SELECT * FROM variables.table_structure WHERE COLUMN_NAME = '#arguments.column#'")
		}
		var val_ = structNew()
		val_.default = q.NULLABLE is 0 ? q.COLUMN_DEFAULT_VALUE : "NULL";
		val_.type = getColdfusionDataType(q.TYPE_NAME);
		val_.validate = q.REMARKS;
		if(!q.IS_NULLABLE)	{
			val_.validate = listAppend(val_.validate,"required",";")
		}

		return val_;
	}

	private string function getColdfusionDataType(required string type)	{
		var rt = "";

		switch(arguments.type){
			case "INT": case "MEDIUMINT": case "BIGINT": case "SMALLINT": case "TINYINT":
				rt = "int";
			break;
			case "INT UNSIGNED": case "MEDIUMINT UNSIGNED": case "BIGINT UNSIGNED": case "SMALLINT UNSIGNED": case "TINYINT UNSIGNED":
				rt = "int";
			break;
			case "FLOAT": case "DECIMAL": case "REAL": case "DOUBLE":
				rt = "decimal";
			break;
			case "FLOAT UNSIGNED": case "DECIMAL UNSIGNED": case "REAL UNSIGNED": case "DOUBLE UNSIGNED":
				rt = "decimal";
			break;
			case "DATE":
				rt = "date"
			break;
			case 'TIMESTAMP': case 'DATETIME':
				rt = 'timestamp';
			break;
			case "TIME":
				rt = "time";
			break;
			default:
				rt="varchar";
			break;
		}
		return "cf_sql_" & rt;
	}


	private boolean function modelDataExists()	{

		// check if model have a pk field ;
		var rt = false
		if(this._pk_field eq "")	{
			rt = false
		}
		else 	{
			query name="mE" cachedwithin=variables.cacheTimeSpan datasource=this.datasource	{
				echo("
					SELECT * FROM `#this.TABLE_NAME#` WHERE #this._pk_field# = #getKeyValue()#"
				)
			}
			if(mE.recordcount)	{
				rt = true
			}
		}

		return rt
	}

	include "inc/model/log.cfm"

	include "inc/model/relationship.cfm"

	include "inc/model/finders.cfm"

	include "inc/model/update.cfm"

	include "inc/model/delete.cfm"

	include "inc/model/statistics.cfm"

	include "inc/model/history.cfm"

	include "inc/model/helper.cfm"

	include "inc/function.cfm"

	/* return the value of the primary key */
	private numeric function getKeyValue()	{

		var rt = 0
		if (isDefined('this[this._pk_field]'))	{
			rt = this[this._pk_field]
		}
		return val(rt)
	}

	public string function getColumns()	{

		return variables.columns
	}

	public string function getTableAlias()	{

		return variables.table_alias
	}

	public string function getSQLColumns()	{

		return variables.sql_select
	}

	private any function runQuery(required string sql)	{

		var rt = ''
		//abort CreateTimeSpan(0, 6, 0, 0)
		switch(left(trim(arguments.sql),6))	{
			case 'SELECT':
				query name='rt' cachedwithin=variables.cacheTimeSpan datasource=this.datasource	{
					echo(arguments.sql)
				}
			break;
		}

		return rt
	}

	// use this function to get the model name, table name and alias
	private struct function getModelDefaultParam(required string model_desc)	{

		var mod_desc = structNew()
		switch(listLen(arguments.model_desc," "))	{
			case 1: // model name only
				mod_desc.model_name = arguments.model_desc
				mod_desc.table_name =  arguments.model_desc
			break;
			case 2: // model and table
				mod_desc.model_name = mod_desc.table_name = listFirst(arguments.model_desc," ")
				mod_desc.table_name = mod_desc.table_name & " " & listLast(arguments.model_desc," ")
			break;
			case 3: // model and table and table alias
				mod_desc.model_name = listFirst(arguments.model_desc," ")
				mod_desc.table_name = listGetAt(arguments.model_desc,2, " ") & " " & listLast(arguments.model_desc," ")
			break;
		}
		if(mod_desc.model_name eq mod_desc.table_name)	{
			mod_desc.table_name = ''
		}
		mod_desc.table_name = listlast(mod_desc.table_name,'.')

		return mod_desc;
	}

	public any function buildRelationships(boolean deep_rel=false)	{

		return this
	}

	public string function toString()	{

		var _rt = ""
		loop list=variables.columns item='x'	{

			if((trim(this[x]) neq "") or (val(this[x]) neq 0))	{
				_rt = listAppend(_rt, x & ':' & serializejson(this[x]))
			}

		}

		return "{" & _rt & "}"
	}

}