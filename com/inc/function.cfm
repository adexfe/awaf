<cfscript>
	/*private struct function getModelDefaultParam(required string model_desc)	{

		var mod_desc = structNew();
		switch(listLen(arguments.model_desc," "))	{
			case 1: // model name only
				mod_desc.model_name = mod_desc.table_name = arguments.model_desc ;
				//mod_desc.table_name = '';
			break;
			case 2: // table alias
				mod_desc.model_name = mod_desc.table_name = listFirst(arguments.model_desc," ");
				mod_desc.table_name = mod_desc.table_name & " " & listLast(arguments.model_desc," ");
			break;
			case 3: // model and table and table alias
				mod_desc.model_name = listFirst(arguments.model_desc," ");
				mod_desc.table_name = listGetAt(arguments.model_desc,2, " ") & " " & listLast(arguments.model_desc," ");
			break;
		}

		return mod_desc;
	}*/

	public query function QoQ(required string select, required query dbquery, boolean group)	{

 		local.group_by = ""
		if(arguments.group == true)	{
			local.gf = ""
			local.gv = replace(arguments.select, ", ", ",", "all")
			// todo: remove alias
			loop list="#local.gv#" item="fld"	{
				local.gf = listAppend(local.gf, listFirst(fld, " "))
			}

			local.group_by = "GROUP BY " & local.gf
		}
		query name="local.query" dbtype="query"	{
			echo(
				"SELECT
					#arguments.select#
				FROM arguments.dbquery
				#local.group_by#"
			)
		}

		return local.query
	}

	// param 	@fieldname -> form.fieldnames -
	public number function maxValueOfSubItemInForm(required string fieldnames)	{

		var l = 0;

		loop list=arguments.fieldnames item='form_item'	{
			l = listAppend(l,val(listlast(form_item,'_')));
		}
		var a = listtoarray(l);

		return a.max();
	}

	public string function bracketValueList(required string value)	{

		return "[" & replacenocase(arguments.value,',','],[','all') & "]";
	}

	include "controller/email.cfm"

</cfscript>