<cfscript>

	public void function trash()	{

		transaction action='begin'	{

			var qInsert = buildInsertQuery('deleted_' & this.table_name);
			var rt = qInsert.execute();
			var q ='';
			var child_item = '';

			// move child items {has many}
			if(this._relationship.hasmany.len())	{
				loop array=this._relationship.hasmany item='sub_item'	{
					child_item = model(sub_item);
					q = child_item.findAll(where=this._pk_field & '=' & this.getKeyValue());
					loop query=q	{
						model(sub_item).findByKey(q[child_item._pk_field]).trash();
					}
				}
			}

			// delete from database
			delete();

		}

	}

	public void function Delete(string id=this.getKeyValue())	{

		try {
			query 	{
				echo('DELETE FROM `#this.table_name#` WHERE `#this._pk_field#` = "#arguments.id#"');
			}
			clearCache();
		}
		catch(Any e) {
			abort 'You can not delete this record because it is associated with other records. You have to delete those records first in order to delete this record'
		}


	}

	public void function deleteAll(required string where)	{

		query 	{
			echo('DELETE FROM `' & this.table_name & '` WHERE ' & arguments.where);
		}

		clearCache();
	}

	public void function deleteAllInKey(required string keys)	{

		if(trim(arguments.keys) != "")	{

			query 	{
				echo('DELETE FROM `#this.table_name#` WHERE #this._pk_field# IN (#arguments.keys#)')
			}

			clearCache()

		}
	}

	private any function buildInsertQuery(string table_name = this.table_name)	{

		var qInsert = new Query();

		var sql_ = "INSERT INTO " & arguments.table_name & " SET "
		// move item to tash table
		loop list=variables.columns item='col'	{
			sql_ = listAppend(sql_, col & " = :" & col, ", ");
			if(this.fields[col].default is 'NULL' and this.fields[col].type is 'cf_sql_int' and (this[col] is "" or this[col] is 0))	{
				qInsert.addParam(name = col, null='yes');
			}
			else 	{
				qInsert.addParam(name = col, value = this[col], cfsqltype = this.fields[col].type);
			}
		}
		sql_ = replace(sql_,"SET ,","SET ");

		qInsert.setSql(sql_);

		return qInsert;
	}

</cfscript>