<cfscript>

	//public void function $log(required numeric userid, required string event, string info, numeric key = 0)	{
	public void function $log(numeric key, required numeric userid, required string event, string info)	{

		var _id = getKeyValue()
		if(_id == 0)	{
			_id = arguments.key
		}
		_modelId = 7 // employee
		if(IsDefined("application.#this.table_name#.MODEL.ID"))	{
			_modelId = application[this.table_name].MODEL.ID
		}
		model(application.model.LOG).new({
			IP			: cgi.REMOTE_ADDR,
			URL			: listLast(cgi.REQUEST_URL,'?'),
			Browser 	: cgi.HTTP_USER_AGENT,
			ModelId 	: _modelId,
			Event 		: arguments.event,
			Info 		: arguments.info,
			Key 		: _id,
			UserId 		: arguments.userid
		}).save()

	}

</cfscript>