<cfscript>

	/* get list of ids and names in a table */
	public struct function GetList(string name="Name", string pk = this._pk_field, string where = '', string delimiters = '`')	{

		var rt = {
			ids 	: "",
			names 	: ""
		}

		var q = this.findAll(where : where)

		for(x in q)	{
			if(x[arguments.name]=="")	{
				v = "-"
			}
			else 	{
				v = x[arguments.name]
			}

			rt.ids = listAppend(rt.ids, x[arguments.pk], arguments.delimiters)
			rt.names = listAppend(rt.names, v, arguments.delimiters)
		}

		return rt
	}

	public query function SelectQuery(string select_clause, string where='', string group = '', string order='')	{

		return runQuery('
			#buildSelectClause(arguments.select_clause)#
			#buildWhereClause(arguments.where)#
			#buildGroupClause(arguments.group)#
			#buildOrderClause(arguments.order)#
		')

	}

	/* find by key and return query  */
	public query function findQ(required string key)	{

 		var whereC = this.getTableAlias() & "." & this._pk_field & " = " & arguments.key;
		var qtemp = runQuery(buildSelectClause() & buildWhereClause(whereC));

		return qtemp;
	}

	// get the lst record in the table
	public query function findLast(string key='', string where)	{

		if(arguments.key eq '')	{
			arguments.key = this._pk_field;
		}

		var qtemp = runQuery(
			buildSelectClause() & buildWhereClause(arguments.where) &
			"ORDER BY " & arguments.key & " DESC
			LIMIT 0,1"
		);

		return qtemp;
	}

	/*
		todo --
		implement :
		findall(
			where: 'field_name=:variable_name',
			param: {type: varchar, value: variable_name, name : 'variable_name'}
		)
	*/
	public query function findAll(string where='', string select='', string order='', string group='', string limit='', string distinct='')	{

 		if(arguments.order is '')	{
 			arguments.order = variables.order;
 		}

		var qtemp = runQuery(
			buildSelectClause(arguments.select, arguments.distinct) & buildWhereClause(arguments.where) &
			buildGroupClause(arguments.group) & buildOrderclause(arguments.order) & buildLimitClause(arguments.limit)
		)

		return qtemp;
	}

	public query function findByKeyIn(required string keys,  string where ='',string select='', string order='', string group='')	{

 		if(arguments.order is '')	{
 			arguments.order = variables.order;
 		}
 		// fix list;
 		arguments.keys = listremoveduplicates(arguments.keys);
 		arguments.keys = ArrayToList(ListToArray(arguments.keys));

 		if (arguments.keys is "")	{
 			arguments.keys = 0;
 		}
 		var where_clause = this._pk_field & ' IN (' &  arguments.keys &')';
 		if(arguments.where is not '')	{
 			where_clause = where_clause & ' AND ' & arguments.where;
 		}

		var qtemp = runQuery(
			buildSelectClause(arguments.select) & buildWhereClause(where_clause) & buildOrderclause(arguments.order)
			& buildGroupClause(arguments.group)
		);

		return qtemp;
	}

	/* find one */
	public Model function findOne(string where='')	{

		//query name="qtemp"	{
		var qtemp = runQuery(
				buildSelectClause()
				& buildWhereClause(arguments.where) & '
				LIMIT 0,1
			');
		//}

		variables.finder_result = qtemp;
		setColumns(qtemp);
		setJoinedColumns();
		// return a model object
		return duplicate(this);
	}

	/* find record by primary key */
	public Model function findByKey(required string value, string key = '')	{

		//writeDump(this.relationship);

		if(arguments.key eq '')	{
			//set the default key
			arguments.key = this._pk_field;
		}

		//query name="qtemp"	{
		var qtemp = runQuery(
				buildSelectClause() & '
				WHERE ' & arguments.key & ' = ' & val(arguments.value) & '
				LIMIT 0,1
			');
		//}
		//this.finder_result = qtemp;
		variables.finder_result = qtemp;
		setColumns(query=qtemp);
		setJoinedColumns();

		//this.x=this.relationship;
		// return a model object
		return duplicate(this);
	}

	/* build sql select clause */
	public string function buildSelectClause(string select='', string distinct='')	{

		var sql_join = '';

		if(arguments.select is '')	{
			arguments.select = variables.sql_select;
		}
		else 	{
			arguments.select = variables.sql_select & "," & arguments.select;
		}
		// add custom select from model
		if(variables.custome_select_clause is not '')	{
			arguments.select = arguments.select & ',' & variables.custome_select_clause;
		}

		if(arguments.distinct != '')	{
			arguments.select = 'DISTINCT ' & arguments.distinct & ',' & arguments.select;
		}

		// check the relationship and add it to the columns
		loop array=this._relationship.hasone item="relation_ship"	{
			arguments.select = listAppend(arguments.select, relation_ship.sql_columns);
			// set foreign key
			if(relation_ship.fkey=='')	{
				relation_ship.fkey = relation_ship.key;
			}
			// build join
			//sql_join = sql_join & " " & relation_ship.join_type & " JOIN " & relation_ship.table_name & " AS `" & relation_ship.table_alias & "` ON " & relation_ship.table_alias & "." &  relation_ship.key & " = " & variables.table_alias & "." & relation_ship.fkey;
			if(listLen(relation_ship.table_name,' ') eq 2)	{
				sql_join = sql_join & " " & relation_ship.join_type & " JOIN " & relation_ship.table_name & " ON " & relation_ship.table_alias & "." &  relation_ship.key & " = " & variables.table_alias & "." & relation_ship.fkey;
			}
			else 	{
				sql_join = sql_join & " " & relation_ship.join_type & " JOIN `" & relation_ship.table_name & "` AS `" & relation_ship.table_alias & "` ON " & relation_ship.table_alias & "." &  relation_ship.key & " = " & variables.table_alias & "." & relation_ship.fkey;
			}

			// check for include and add
			if(isDefined("relation_ship.include"))	{
				loop array=relation_ship.include item="inc"	{
					arguments.select = listAppend(arguments.select, inc.sql_columns);
					// build join
					sql_join = sql_join & " " & inc.join_type & " JOIN `" & inc.table_name & "` AS `" & inc.table_alias & "` ON " & relation_ship.table_alias & "." &  inc.fkey & " = " & inc.table_alias & "." & inc.key;

				}
			}

		}

		this.sql_join = sql_join;

		return "SELECT " & arguments.select & " FROM `" & this.TABLE_NAME & "` AS `" & variables.table_alias & "` " & sql_join;
	}

	/* get belongsto and hasmany items here */
	public query function get(required string relationship, required string table_name)	{

		//query name="qt"	{
			switch(arguments.relationship){
				case 'many':
					var rel = this._relationship.hasMany[arguments.table_name]
					if(rel.multiple_field)	{
						qt = runQuery(
							rel.sql &
							" WHERE " & rel.table_alias & "." & this._pk_field & "=" & getKeyValue()
						);
							//" WHERE " & rel.table_name & "." & this._pk_field & "=" & getKeyValue()
					}
					else 	{
						//writeDump(rel);
						qt = runQuery(
							rel.sql &
							" WHERE " & rel.table_name & "." & this._pk_field & "=" & getKeyValue()
						);
					}

				break;
				case 'belongsto':
					qt = runQuery(
						this._relationship.belongsTo[arguments.table_name].sql &
						" WHERE " &  this._pk_field & "=" & getKeyValue()
					);
				break;
			}
		//}

		return qt
	}

	/* get belongsto and hasmany items here */
	public query function $get(string select='')	{

 		var func_name = getFunctionCalledName();
 		var model_name = mid(func_name, 4, len(func_name)-3);
 		var s_model_name = singularize(model_name);
 		loop array=this._relationship.hasMany item="x"	{
 			if(listlast(x,'.') is s_model_name)	{
 				s_model_name = x;
 			}
 		}

		var qt = model(s_model_name).buildRelationships();
		var qt2 = qt.findAll(select=arguments.select, where= qt.table_name & '.' & this._pk_field & "=" & val(getKeyValue()));

		return qt2;
	}

	private string function buildWhereClause(string where)	{
		var where_clause = '';

		// replace $this with the table name
		arguments.where = replaceNoCase(arguments.where, '$this', this.getTableAlias());

		if(where neq '')	{
			where_clause = ' WHERE ' & arguments.where;
		}

		return where_clause;
	}

	private string function buildOrderclause(string order='')	{

		var order_clause = '';
		if(arguments.order is not '')	{
			order_clause = ' ORDER BY ' & arguments.order;
		}

		return order_clause;
	}

	private string function buildGroupClause(string group='')	{

		var group_clause = '';
		if(trim(arguments.group) neq '')	{
			group_clause = ' GROUP BY ' & arguments.group;
		}

		return group_clause;
	}

	private string function buildLimitClause(string limit='')	{

		var ltm='';
		if(trim(arguments.limit) neq '')	{
			ltm = ' LIMIT ' & arguments.limit
		}

		return ltm;
	}
</cfscript>