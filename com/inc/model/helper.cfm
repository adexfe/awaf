<cffunction name="clearCache" retrun="void" access="private">
	<cfargument name="filter" default="#this.table_name#" type="string"/>

	<cfif arguments.filter is "">

		<cfobjectcache action="clear"/>

	<cfelse>

		<cfobjectcache action="clear" filter="*#arguments.filter#*"/>

	</cfif>

</cffunction>

<cffunction name="clearAllCache" retrun="void" access="private">

		<cfobjectcache action="clear"/>

</cffunction>

<cffunction name="dumpdata" retrun="void" access="private">
	<cfargument name="data" type="any"/>

	<cfquery>
		INSERT INTO `dump` set
			`DATA` = <cfqueryparam cfsqltype="cf_sql_varchar" value="#serializejson(arguments.data)#"/>
	</cfquery>

</cffunction>

<cfscript>

	private string function pulralize(required string word)	{

		var last_letter = right(arguments.word, 1);
		var pl = '';

		switch(last_letter)	{
			case 'mdd':
				code;
			break;

			default:
				pl = 's';
			break;
		}

		return arguments.word & pl;
	}

	private string function singularize(required string word)	{

		var nword = arguments.word;
		if(reFindNoCase('s$', arguments.word))	{

			nword = reReplaceNoCase(arguments.word, 's$', '');
		}

		return nword;
	}

	public function isEmail(str) {

	   return (REFindNoCase("^['_a-z0-9-\+]+(\.['_a-z0-9-\+]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*\.(([a-z]{2,3})|(aero|asia|biz|cat|coop|info|museum|name|jobs|post|pro|tel|travel|mobi))$",
	arguments.str) AND len(listGetAt(arguments.str, 1, "@")) LTE 64 AND
	len(listGetAt(arguments.str, 2, "@")) LTE 255) IS 1;
	}

</cfscript>