<cfoutput>
<cfif ThisTag.ExecutionMode EQ "Start">

    <cfparam name="Attributes.TagName" type="string" default="BlockItem"/>
    <cfparam name="Attributes.label" type="string"/>
    <cfparam name="Attributes.value" type="string" default=""/>
    <cfparam name="Attributes.textClass" type="string" default=""/>
    <cfparam name="Attributes.format" type="string" default=""/> <!--- money, email --->
    <cfparam name="Attributes.prefix" type="string" default=""/>
    <cfparam name="Attributes.removeBlank" type="string" default="false"/>
    <cfparam name="Attributes.icon" type="string" default=""/>
    <cfparam name="Attributes.itemClass" type="string" default=""/>
    <cfparam name="Attributes.valueStyle" type="string" default=""/>
    <cfparam name="Attributes.lableClass" type="string" default=""/>
    <cfparam name="Attributes.iconClass" type="string" default=""/>
    <cfparam name="Attributes.suffix" type="string" default=""/>
    <cfparam name="Attributes.id" type="string" default="#GetRandomVariable()#"/>

    <cfset allow_blank = true/>
    <cfif !(len(trim(Attributes.value)))>
		<cfif Attributes.removeBlank>
			<cfset allow_blank = false/>
		</cfif>
    </cfif>

	<cfif allow_blank>

	   	<cfswitch expression="#attributes.format#">
	   		<cfcase value="address">
	   			<address id="#Attributes.id#">
					<strong class="xbitem">
						<cfif Attributes.icon is not "">
						 	 <i class="fa fa-#Attributes.icon# #Attributes.iconclass#"></i>
						</cfif>
						#Attributes.label#
					</strong>
					<br/>
					#replace(Attributes.value,chr(10),'<br/>','all')#
				</address>
	   		</cfcase>
	   		<cfcase value="email">
	   			<p id="#Attributes.id#">
	   			<strong class="xbitem">
					<cfif Attributes.icon is not "">
					 	 <i class="fa fa-#Attributes.icon# #Attributes.iconclass#"></i>
					</cfif>
	   				#Attributes.label#
	   			</strong><br>
				<cfset emailList = replace(Attributes.value,";",",","all")/>
				<cfloop list="#emailList#" item="em">
					<a href="mailto:#em#" class="#Attributes.textclass#">#em#</a><br/>
				</cfloop>
				</p>
	   		</cfcase>
	   		<cfcase value="date">
	   			<p id="#Attributes.id#">
				<strong class="xbitem">
					<cfif Attributes.icon is not "">
					 	 <i class="fa fa-#Attributes.icon# #Attributes.iconclass#"></i>
					</cfif>
					#Attributes.label#</strong><br>
				#dateFormat(Attributes.value,'dd-mmm-yyyy')#
				</p>
			</cfcase>
			<cfcase value="money">
	   			<p id="#Attributes.id#">
				<strong class="xbitem">
					<cfif Attributes.icon is not "">
					 	 <i class="fa fa-#Attributes.icon# #Attributes.iconclass#"></i>
					</cfif>
					#Attributes.label#</strong><br>
				#Attributes.prefix# #numberFormat(Attributes.value,'9,999.99')#
				</p>
			</cfcase>
	   		<cfdefaultcase>
	   			<p>
				<strong class="xbitem">
					<cfif Attributes.icon is not "">
					 	 <i class="fa fa-#Attributes.icon# #Attributes.iconclass#"></i>
					</cfif>
					<cfif Attributes.label is not "">
						#Attributes.label# <br>
					</cfif>
				</strong>
				<span id="#Attributes.id#" <cfif Attributes.itemClass !="">class="#Attributes.itemClass#"</cfif> <cfif Attributes.valueStyle != "">style="#Attributes.valueStyle#"</cfif>>#replace(Attributes.value,chr(10),'<br/>','all')# #Attributes.suffix#</span>
				</p>
			</cfdefaultcase>
	   	</cfswitch>

   	</cfif>

</cfif>
</cfoutput>