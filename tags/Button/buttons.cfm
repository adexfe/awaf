<cfoutput>
<cfif ThisTag.ExecutionMode EQ "Start">

    <cfparam name="Attributes.TagName" type="string" default="Buttons"/>
    <cfparam name="Attributes.iconprefix" type="string" default="fa fa-"/>
    <cfparam name="Attributes.buttonprefix" type="string" default="btn-"/>
    <cfparam name="Attributes.align" type="string" default="right"/>
    <cfparam name="Attributes.size" type="string" default="btn-group-sm"/> <!--- btn-group-xs --->
    <cfparam name="Attributes.group" type="string" default="btn-group"/>
    <cfparam name="Attributes.class" type="string" default=""/>

	<cfset request.buttons = ArrayNew(1)/>

<cfelse>

	<!---- === build for large display ===---->
	<div class="hidden-xs">
		<div class="toolbar-btn-action text-#Attributes.align# #Attributes.group# #Attributes.size# #Attributes.class#">
			<cfloop array="#request.buttons#" item="attr">
				<cfif attr.isDivider>
					<a class="btn" style="padding:5px;"></a>
				<cfelse>
					<a class="#attr.id# btn #Attributes.buttonprefix##attr.style# #attr.size# #attr.class#" style="#attr.cssStyle#" title="#attr.help#">
						<cfif attr.icon neq ""><i class="#Attributes.iconprefix##attr.icon#"></i></cfif>
						#attr.title#
					</a>
				</cfif>
			</cfloop>
		</div>
	</div>

	<!---- === build for mobile ===---->
	<div class="visible-xs">
		<div class="btn-group #Attributes.size# text-left">
			<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
			Action <span class="caret"></span>
			</button>
			<ul class="dropdown-menu" role="menu">
				<cfloop array="#request.buttons#" item="attr">
					<cfif attr.isDivider>
						<li class="divider"></li>
					<cfelse>
						<li>
							<a class="#attr.id# #attr.class#" title="#attr.help#">
								<cfif attr.icon neq "">
									<i class="#Attributes.iconprefix##attr.icon#"></i>
								</cfif>
								<cfif attr.title eq "">
									#attr.help#
								<cfelse>
									#attr.title#
								</cfif>
							</a>
						</li>
					</cfif>
				</cfloop>
			</ul>
		</div>
	</div>
	<!--- script --->
	<script>
		$(document).ready(function() {
			<cfloop array=#request.buttons# item="x">
				<cfif !x.isDivider>

					<cfset _btn = getRandomVariable()/>
					<cfset _change_url = ",'changeURL':#x.changeURL#"/>
					<cfset _render_to = ""/>
					<cfset _param = ",'param':'#x.urlparam#'"/>
					<cfif x.renderTo neq ''>
						<cfset _render_to = ",'renderTo':'#x.renderTo#'"/>
					</cfif>

					var #_btn# = $('.#x.id#');
					//console.log(#_btn#);
	                #_btn#.click(function()    {
	                    <cfif x.url != "">// url
		                    <cfset ntitle = replacenocase(x.title,"'","\'","all")/>
		                    <cfset nconfirm = replacenocase(x.confirm,"'","\'","all")/>

	                    	<cfif x.confirm != "">
	                    		if(confirm('#nconfirm#'))	{
	                    			loadPage('#x.url#',{'forcePageReload':#x.forcePageReload#,'title':'#ntitle#'#_render_to# #_change_url# #_param#});
	                    		}
	                    	<cfelse>
	                    		loadPage('#x.url#',{'forcePageReload':#x.forcePageReload#,'title':'#ntitle#'#_render_to# #_change_url# #_param#});
	                    	</cfif>

						</cfif>

	                    <cfif x.action != "">// action
	                    	<cfset conf_ = replacenocase(x.confirm,"'","\'","all")/>
	                    	<cfif conf_ != "">
	                    	if(confirm('#conf_#'))	{
							</cfif>
		                        $.ajax({
		                            cache: false,
		                            beforeSend:function()   {
		                                #_btn#.addClass('loading disabled');
		                            },
		                            url: 'controllers/#x.action#'
		                        }).done(function(data)  {

		                        	<cfif x.redirectURL != ''>
		                        		<cfset ntitle = replacenocase(x.title,"'","\'","all")/>
		                        		loadPage('#x.redirectURL#',{'forcePageReload':true,'title': '#ntitle#' #_render_to# #_change_url# #_param#});
		                        	</cfif>

		                        	<cfif x.flashMessage != ''>
										<cfset x.flashMessage = replace(x.flashMessage,"'","\'",'all')/>
		                        		<cfset msg_str = getRandomVariable()/>
		                        		#msg_str#="#x.flashMessage#";
		                        		#msg_str# = #msg_str#.replaceAll('[[MSG]]',data);
		                        		itemSaveNotice(#msg_str#);
		                        	</cfif>

		                        }).fail(function(xhr)  {
		                            showError(xhr);
		                            //console.log(xhr);
		                        }).always(function(data) {
		                            #_btn#.removeClass('loading disabled');
		                        });
								<cfif x.closeModal>
									$('##__app_modal').modal('hide');
								</cfif>
	                    	<cfif conf_ != "">
	                    	}
							</cfif>
	                    </cfif>
	                    <cfif x.printurl neq "">
	                    	<cfparam name="application.awaf.staff_client" default="false" type="boolean">
	                    	<cfif application.awaf.staff_client>
	                    		window.open('#application.site.url#views/inc/print/print.cfm?page=#lcase(request.user.usertype)#/#x.printurl#&&#x.urlparam#');
	                    	<cfelse>
	                    		window.open('views/inc/print/print.cfm?page=#x.printurl#&#x.urlparam#');
	                    	</cfif>
	                    	return false;
	                    </cfif>
	                    <cfif x.blankpageurl neq "">
	                    	window.open('views/#x.blankpageurl#.cfm');
	                    	return false;
	                    </cfif>
	                    <cfif x.modalurl neq "">// model url
		 					<cfset ntitle = replacenocase(x.modaltitle,"'","\'","all")/>
	                    	showModal('#x.modalurl#',{'title':'#ntitle#' #_param# #_render_to#});
	                    </cfif>
	                });
				</cfif>
			</cfloop>
		});
	</script>

</cfif>
</cfoutput>