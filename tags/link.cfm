<cfoutput>
<cfif ThisTag.ExecutionMode == "Start">

    <cfparam name="Attributes.TagName" type="string" default="Link"/>
    <cfparam name="Attributes.url" type="string"/>
    <cfparam name="Attributes.changeURL" type="boolean" default="true"/>
    <cfparam name="Attributes.renderTo" type="string" default=""/>
    <cfparam name="Attributes.urlparam" type="string" default=""/>
    <cfparam name="Attributes.redirectURLparam" type="string" default=""/>
    <cfparam name="Attributes.type" type="string" default="load"/> <!--- load || ajax, execute, modal, print, blank, todo:callback --->
    <cfparam name="Attributes.icon" type="string" default=""/>
    <cfparam name="Attributes.showIfNoAccess" type="boolean" default="false"/>
    <cfparam name="Attributes.title" type="string" default=""/>
    <cfparam name="Attributes.help" type="string" default="#Attributes.title#"/>
    <cfparam name="Attributes.modalTitle" type="string" default="#Attributes.help#"/>
    <cfparam name="Attributes.class" type="string" default=""/>
    <cfparam name="Attributes.cssStyle" type="string" default=""/> <!--- css style --->
    <cfparam name="Attributes.force" type="boolean" default="false"/>
    <cfparam name="Attributes.closeModal" type="boolean" default="false"/>

	<!--- close tag --->
	<!--- Testing git gui now --->
	<cfset nurl = Attributes.url/>

	<cfif Attributes.type == "execute" || Attributes.type == "ajax">

      <cfparam name="Attributes.confirm" type="string" default="Are you sure"/>
      <cfparam name="Attributes.redirectURL" type="string" default=""/>

		<cfset method =  listlast(listFirst(listlast(Attributes.url,'?'),'&'),'=')/>
		<cfset control = replace(listFirst(Attributes.url,'.'),'/','.')/>

		<cfset nurl = control & '.' & method/>

	</cfif>

<cfelse>
	<cfset Content = THISTAG.GeneratedContent />
	<cfset THISTAG.GeneratedContent = "" />


	<cfif listFindNoCase(request.user.pageURLs, listfirst(nurl,'@'))>

      <cfswitch expression="#Attributes.type#">
		 	<cfcase value="print">
    			<cfif Attributes.icon == "">
    				<cfset Attributes.icon = "print"/>
    			</cfif>
		 		<cfset printurl = replace(Attributes.url,'.','/','all')/>
		 		<cfset printurl = replace(printurl,'@','&key=')/>
		 		<a title="#Attributes.help#" class="#attributes.class#" style="#Attributes.cssStyle#" href="views/inc/print/print.cfm?page=#printurl#&#Attributes.urlparam#" target="_blank" >
					<cfif Attributes.icon neq ""><i class="fa fa-#Attributes.icon#"></i></cfif>
			#Content#
				</a>
         </cfcase>
         <cfcase value="modal">
         	<a title="#Attributes.help#" class="#attributes.class#" style="#Attributes.cssStyle#" href="javascript:;" onclick="showModal('#Attributes.url#', {'param':'#Attributes.urlparam#','title':'#Attributes.modalTitle#'})" >
         	<cfif Attributes.icon neq ""><i class="fa fa-#Attributes.icon#"></i></cfif>#Content#</a>
         </cfcase>
         <cfcase value="execute,ajax" delimiters=",">
      		<cfset conf_ = replacenocase(Attributes.confirm,"'","\'","all")/>
         	<a title="#Attributes.help#"
         		class="#attributes.class#"
         		style="#Attributes.cssStyle#" href="javascript:;"
         		onclick="
         		<cfif conf_ != "">if(confirm('#conf_#')){</cfif>
         			ajaxCall('#Attributes.url#&#Attributes.urlparam#', function(){
                     <cfif Attributes.redirectURL neq "">
         				   loadPage('#Attributes.redirectURL#',{'forcePageReload':true,'changeURL':#attributes.changeURL#,'param':'#Attributes.redirectURLparam#'<cfif attributes.renderTo neq "">,'renderTo':'#attributes.renderTo#'</cfif>});
                     </cfif>
                     <cfif Attributes.closeModal>$('##__app_modal').modal('hide');</cfif>
         			});
         		<cfif conf_ != "">}</cfif>
         		" >
      			<cfif Attributes.icon neq ""><i class="fa fa-#Attributes.icon#"></i></cfif>
				#Content#
			</a>
         </cfcase>
         <cfcase value="blank">
			<cfset _nurl = replace(Attributes.url,'.','/','all')/>
			<cfset _nurl = replace(_nurl,'@','.cfm?id=')/>
         	<a title="#Attributes.help#"
         		class="#attributes.class#"
         		style="#Attributes.cssStyle#"
				href="views/#_nurl#"
				target="_blank">
      			<cfif Attributes.icon neq ""><i class="fa fa-#Attributes.icon#"></i></cfif>
				#Content#
			</a>
         </cfcase>
         <cfdefaultcase>
            <a title="#Attributes.help#"
            class="#attributes.class#"
            style="#Attributes.cssStyle#" href="javascript:;"
            onclick="
				loadPage('#Attributes.url#', {'param':'#Attributes.urlparam#','title':'#Attributes.title#','forcePageReload':#Attributes.force#,'changeURL':#attributes.changeURL#<cfif attributes.renderTo neq "">,'renderTo':'#attributes.renderTo#'</cfif>});
                <cfif Attributes.closeModal>$('##__app_modal').modal('hide');</cfif>
			" >
            <cfif Attributes.icon neq ""><i class="fa fa-#Attributes.icon#"></i></cfif>#Content#</a>
         </cfdefaultcase>
      </cfswitch>
	<cfelse>
		<cfif Attributes.showIfNoAccess>
			#Content#
		</cfif>
	</cfif><!---&nbsp;--->

</cfif>
</cfoutput>