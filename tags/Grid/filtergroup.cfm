<cfoutput>
	
	<cfif ThisTag.ExecutionMode EQ "Start">
	  
	    <cfparam name="Attributes.TagName" type="string" default="FilterGroup"/>
	    <cfparam name="Attributes.title" type="string"/>

        <cfassociate basetag="cf_filters"/>
        <cfset basetag = getBaseTagData("cf_filters")/>

	    <cfparam name="Attributes.column" type="numeric" default="#basetag.Attributes.column#"/>
	    <cfif val(Attributes.column) eq 0>
	    	<cfabort showerror="Column is required"/>
	    </cfif>
 
		<cfset ArrayAppend(request.grid.filter_group, Attributes)/>

        <optgroup label="#Attributes.title#">
	
	<cfelse>

		</optgroup>

	</cfif>
	
</cfoutput>