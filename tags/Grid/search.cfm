<cfoutput>
<cfif ThisTag.ExecutionMode EQ "Start">

    <cfparam name="Attributes.TagName" type="string" default="Search"/>
    <cfparam name="Attributes.id" type="string" default="_#createUniqueId()#" />
    <cfparam name="Attributes.on" type="string" default="Enter" /> <!--- on key up/press --->

    <cfassociate basetag="cf_ToolBar" />

    <cfset request.grid.hasSearch = true/>

	<div class="col-sm-4 col-xs-6">

		<div class="search">
			<input type="text" class="form-control #Attributes.id# search_text" placeholder="Search..."/>
		</div>

	</div>

 	<cfset ArrayAppend(request.grid.toolbar,Attributes)/>


</cfif>
</cfoutput>