<!---TOFIX: Filter does not work unless the cf_search tag is present ---->
<cfoutput>
	<cfif thisTag.ExecutionMode eq 'start'>

        <cfparam name="Attributes.TagName" type="string" default="grid"/>
        <cfparam name="Attributes.model" type="string"/>
        <cfparam name="Attributes.name" type="string" default="#replace(listFirst(Attributes.model,' '),'.','_','all')#"/>
        <!---cfparam name="Attributes.name" type="string" default="#getRandomVariable()#"/--->
        <cfparam name="Attributes.key" type="string" default=""/>
        <cfparam name="Attributes.class" type="string" default=""/>
        <cfparam name="Attributes.id" type="string" default="#getRandomVariable()#"/>
        <cfset Attributes.name = Attributes.name & "_" & Attributes.id/>
        <cfparam name="Attributes.sortDirection" type="string" default="desc"/>
        <cfparam name="Attributes.sortBy" type="string" default="0"/>
        <cfparam name="Attributes.responsive" type="boolean" default="true"/>
        <cfparam name="Attributes.hover" type="boolean" default="true"/>
        <cfparam name="Attributes.condensed" type="boolean" default="false"/>
        <cfparam name="Attributes.striped" type="boolean" default="false"/>
        <cfparam name="Attributes.bordered" type="boolean" default="false"/>
        <cfparam name="Attributes.filter" type="string" default=""/>
        <cfparam name="Attributes.rows" type="numeric" default="20"/>
        <cfparam name="Attributes.export" type="string" default=""/><!--- list export, excel, pdf, html, htm --->

		<cfparam name="Attributes.groupBy" type="string" default=""/>

		<cfparam name="Attributes.showPager" type="boolean" default="true"/>
		<cfparam name="Attributes.hideToolBar" type="boolean" default="false"/>

        <cfif !attributes.showPager>
            <style>
                ###Attributes.id# .grid_footer{display:none;visibility: hidden;}
            </style>
        </cfif>

        <cfif attributes.hideToolBar>
            <style>
                ###Attributes.id# .data-table-toolbar{display:none;}
            </style>
        </cfif>

        <cfset cssopt = ""/>
        <cfif Attributes.hover> <cfset cssopt = listAppend(cssopt,"table-hover", " ")/> </cfif>
        <cfif Attributes.condensed> <cfset cssopt = listAppend(cssopt,"table-condensed", " ")/> </cfif>
        <cfif Attributes.striped> <cfset cssopt = listAppend(cssopt,"table-striped", " ")/> </cfif>
        <cfif Attributes.bordered> <cfset cssopt = listAppend(cssopt,"table-bordered", " ")/> </cfif>

        <div id="#Attributes.id#" <cfif Attributes.responsive>class="table-responsive"</cfif>>
            <table id="#Attributes.id#_tbl" class="table #cssopt# #Attributes.class#" data-page-length='#Attributes.rows#' data-order='[[ #Attributes.sortby#, "#Attributes.sortdirection#" ]]'>

        <cfset request.grid.join = ArrayNew(1)/>
        <cfset request.grid.cutomJoin = ArrayNew(1)/>
        <cfset request.grid.tag = Attributes/>
        <cfset request.grid.hasSearch = false/>
        <cfset request.grid.hasFilter = false/>

    <cfelse>

            </table>

        </div>

			<cfset col_list = ""/>
			<cfset scol_list = ""/>
			<cfset tcol_list = ""/>
			<cfset col_class = ""/>
			<cfloop array="#request.grid.columns#" item="x" index="j">
				<!--- set key --->
				<cfif Attributes.key eq "">
					<cfif j eq 1>
						<cfset Attributes.key = x.name/>
					</cfif>
				</cfif>
				<cfset col_list = listAppend(col_list, x.name)/>
				<cfset scol_list = listAppend(scol_list, x.field)/>
				<cfset tcol_list = listAppend(tcol_list, x.caption)/>
				<!--- create class string {"sClass": ""} --->
				<cfif x.class neq "">
					<cfset col_class = listAppend(col_class,'{"sClass" : "#x.class#", "aTargets" : [#j-1#]}')/>
				</cfif>
			</cfloop>

        <!--- check if grid has commad --->
        <cfset cmd = false/>
        <cfset cmd_but = cmd_but2 = ""/>
        <cfif isdefined("request.grid.Commands")>
            <cfset cmd = true/>
            <!--- build command --->

            <cfloop array="#request.grid.Commands#" item="x">
                <!--- check condition --->
                <cfset url_ = "loadPage('#x.url#',{})"/>
                <cfset cmd_but = listAppend(cmd_but,"<a class='btn btn-xs #x.buttontype#' onclick=#url_#><i class='#x.icon#' data-toggle='tooltip' title='' data-original-title='#x.title#'></i></a>"," ")/>
                <cfset cmd_but2 = listAppend(cmd_but2,"<li><a href='##'><i class='#x.icon#'></i> #x.title#</a></li>"," ")/>
            </cfloop>
            <cfset cmd_but2 = "<button type='button' class='btn btn-default btn-xs dropdown-toggle' data-toggle='dropdown'>Action <span class='caret'></span></button><ul class='dropdown-menu'>" & cmd_but2 & "</ul>"/>

        </cfif>
        <cfset cmd_but=""/>

        <!--- join model --->
        <cfset join = join_stm = select_stm = ""/>
        <cfloop array="#request.grid.join#" item="a" index="b">
            <cfset join = listAppend(join, a.type & "," & a.model & "," & a.fkey & "," & a.deep, "$")/>
        </cfloop>

        <cfif arrayLen(request.grid.cutomjoin)>
            <cfset join_stm = trim(request.grid.cutomjoin[1].join)/>
            <cfset join_stm = replace(join_stm,chr(10),' ','all')/>
            <cfset join_stm = replace(join_stm,chr(13),' ','all')/>
            <cfset select_stm = trim(request.grid.cutomjoin[1].select)/>
            <cfset select_stm = replace(select_stm,chr(10),' ','all')/>
            <cfset select_stm = replace(select_stm,chr(13),' ','all')/>
        </cfif>

        <script type="text/javascript">

            //$(document).ready(function() {
                <cfset oTable = getRandomVariable()/>
                <cfset _tags_search = getRandomVariable()/>
                //var #_tags_search# = $('###Attributes.id# input.search_text');
                //var #_tags_search# = $('.request.grid.toolbar[1].id');

                function __#oTable#()   {
                    #oTable# = $('###Attributes.id#_tbl').DataTable( {
						"autoWidth": false,
                        "columnDefs": [
                            <cfset c = -1/>
									 <!---TODO: Implement later "type": "html"--->
                            <cfloop array="#request.grid.columns#" item="x" index="j">
                                <cfset c++/>
                                {
									<cfif x.width neq "">"width": "#x.width#",</cfif>
                                    <cfif x.hide>"visible": false,</cfif>
                                    <cfif !x.sortable>"orderable": false,</cfif>

												<cfif x.editable>
													<!--- TODO: there is an error when inserting into a table with no databefore, there should be a way to refresh the grid --->
													"createdCell": function (td, cellData, row, rowData, icol) {
														var _c = cellData;
														var col = row;
														var _td = $(td);
														// check if iseditable is
														<cfif x.iseditable neq "">
															if(#x.iseditable#) 	{
														</cfif>
										      		    _td.attr("contenteditable","true");
														_td.blur(function() {
															//console.log(rowData);
															var entered_data = $(this).html();
														   if (cellData!=entered_data)	{
																<!--- find keyfield --->
																<cfset _keyfield = Attributes.key/>
																<cfif x.keyrow neq 0>
																	<cfset _keyfield = listlast(request.grid.columns[x.keyrow+1].name,'_')/>
																</cfif>
																<!--- build "update_other_fields/updatefield" is need be --->
																<cfset _updatefield = "''"/>
																<cfif x.updatefield neq "">

																	<cfloop list="#x.updatefield#" item="uf" delimiters="``">
																		<cfset _first = ""/>
																		<cfset _last = ""/>
																		<cfif listlen(uf,'=') gt 1>
																			<cfset _first = ListFirst(uf,'=')/>
																			<cfset _last = ListLast(uf,'=')/>
																		<cfelse>
																			<cfset _first = _last = uf/>
																		</cfif>
																		<cfif _updatefield eq "''">
																			<cfset _updatefield = ""/>
																		</cfif>
																		<cfset _updatefield = ListAppend(_updatefield, "'" & listlast(request.grid.columns[_first+1].name,'_') & "='+row[#_last#]", '&')/>

																	</cfloop>
																	<cfif _updatefield eq ""><cfset _updatefield = "''"/></cfif>

																</cfif>
																//console.log(_updatefield);
																x__updateGridData(_td, entered_data, '#x.name#', row[#x.keyrow#],'#_keyfield#', #_updatefield#, <cfif x.model eq "">'#Attributes.model#'<cfelse>'#x.model#'</cfif>, '#x.saveMethod#');
														   }
														});
													// check if iseditable is --- end
													<cfif x.iseditable neq "">
														}
													</cfif>
													},
												</cfif>

                                    "className": "#x.class#"
                                        <cfif x.hide> + " hidden"</cfif>
                                        <cfif x.align is not ""> + " gcell-#x.align#" </cfif>
                                        <cfif x.nowrap> + " nowrap" </cfif>,
                                    "render": function ( data, type, row) {
                                        var oData = col = row;
                                        var self = data;
                                        var rt = data;
                                        <!---cfset x.Template = replace(x.Template,'oData[', 'row[','all')/>
                                        <cfset x.script = replace(x.script,'oData[', 'row[','all')/--->
                                        <cfif x.template neq "">
                                            rt = #x.Template#;
                                        <cfelseif x.script neq "">
                                            rt = (function(){#x.script#;})();
                                        <cfelse>
                                            rt = row[#c#];
                                        </cfif>
                                        return rt;
                                    }, "targets": #c#
                                },
                            </cfloop>
                            <!--- command --->
                            <cfif cmd>
                                {
                                    "className" : "nowrap text-right", "orderable": false, "searchable": false,
                                    "render": function ( data, type, row, _object ) {
                                        var self = data;
                                        var oData = row;
                                        var col = row;
                                        var _data = '';
                                        <cfloop array="#request.grid.Commands#" item="x">
                                            <cfset x.pagetitle = replace(x.pagetitle,' ','&nbsp;','all')/>

                                            <!---TODO: workin progress --->
                                            <cfif x.jsurlparam != "">
                                                <cfset lf = listFirst(x.jsurlparam,"=")/>
                                                <cfset ll = listLast(x.jsurlparam,"=")/>
                                                <cfset param_ ="'param':'#x.urlparam#&#lf#='+#ll#"/>

                                            <cfelse>
                                                <cfset param_ ="'param':'#x.urlparam#'"/>
                                            </cfif>
                                            <cfswitch expression="#x.type#">

                                                <cfcase value="load">
                                                    <cfset murl = "#x.modalurl#@""+#x.key#+"""/>
                                                    <cfif x.modalurl == "">
                                                        <cfset murl = ""/>
                                                    </cfif>
                                                    <cfif x.renderTo eq "">
                                                        <cfset url_ = "loadPage('#x.url#@""+#x.key#+""',{'title':'#x.pagetitle#&nbsp;""+#x.PageColumn#+""','param':'#x.urlparam#','changeURL':#x.changeURL#,'forcePageReload':#x.forcePageReload#,'modalurl':'#murl#'})"/>
                                                    <cfelse>
                                                        <cfset url_ = "loadPage('#x.url#@""+#x.key#+""',{'title':'#x.pagetitle#&nbsp;""+#x.PageColumn#+""','param':'#x.urlparam#','renderTo':'#x.renderTo#','changeURL':#x.changeURL#,'forcePageReload':#x.forcePageReload#,'modalurl':'#murl#'})"/>
                                                    </cfif>
                                                </cfcase>

                                                <cfcase value="modal">
                                                    <cfset url_ = "showModal('#x.url#@""+#x.key#+""',{'title':'#x.pagetitle#&nbsp;""+#x.PageColumn#+""',#param_#})"/>
                                                </cfcase>

                                                <cfcase value="execute">

                                                    <cfset url_ = "_executecmd('#x.url#&key=""+#x.key#+""',this,""+_object.row+"")"/>

                                                </cfcase>

                                                <cfcase value="print">
                                                    <cfset n_url = "views/inc/print/print.cfm?page=" & replace(x.url,'.','/','all')/>
                                                    <cfset url_ = "openURL('#n_url#&id=""+#x.key#+""')"/>
                                                </cfcase>

                                                <cfcase value="blank">
                                                    <cfset n_url = replace(x.url,".","/","all") & ".cfm?"/>
                                                    <cfset url_ = "openURL('#n_url#id=""+#x.key#+""')"/>
                                                </cfcase>

                                            </cfswitch>

                                            if(#x.condition#)   {
                                                _data = _data + "<a title='#x.help#' class='btn btn-xs #x.buttontype#' onclick=#url_#><i class='#x.icon#'></i> #x.title#</a>";
                                            }

                                        </cfloop>
                                        return _data;
                                    },
                                    "targets" : -1
                                }
                            </cfif>
                        ],

                        "serverSide": true,
                        "lengthMenu": [5, 10, 15, 20, 30, 50, 70, 100],
                        "searching": #request.grid.hasSearch#,
                        "pagingType": "full_numbers",
                        "ajax": {
                            "url": "awaf/tags/grid/ajax.cfm?grid_name=#Attributes.name#",
                            "type": "POST"
                        }

                        <!--- save sAjaxSource inside the session for use with export.cfm, implement this for ajax.cfm ---->
                        <!---cfif attributes.export neq ""--->
                            <cflock scope="Session" type="exclusive" timeout="20">
                                <cfset session.tags.grid[Attributes.name].filter = attributes.filter/>
                                <cfset session.tags.grid[Attributes.name].model = Attributes.model/>
                                <cfset session.tags.grid[Attributes.name].join = join/>
                                <cfset session.tags.grid[Attributes.name].Columns = request.grid.columns/>
                                <cfset session.tags.grid[Attributes.name].fColumns = scol_list/>
                                <cfset session.tags.grid[Attributes.name].Captions = tcol_list/>
                                <cfset session.tags.grid[Attributes.name].key = attributes.key/>
                                <cfset session.tags.grid[Attributes.name].cmd = cmd/>
                                <cfset session.tags.grid[Attributes.name].join_stm = join_stm/>
                                <cfset session.tags.grid[Attributes.name].sel_stm = select_stm/>
                                <cfset session.tags.grid[Attributes.name].gb = Attributes.groupby/>
                            </cflock>
                        <!----/cfif--->

                    });

                    <cfif isdefined("request.grid.toolbar[1]")>

                        $(".#request.grid.toolbar[1].id#").keyup(function(e) {

                            <cfswitch expression="#request.grid.toolbar[1].on#">
                                <cfcase value="Any">
                                    #oTable#.search( this.value ).draw();
                                </cfcase>
                                <cfdefaultcase>
                                    if(e.which == 13) {
                                        #oTable#.search( this.value ).draw();
                                    }
                                </cfdefaultcase>
                            </cfswitch>
                        });

                    <cfelse>
                        // make action button 12

                    </cfif>
                }

            //});

            $(document).ready(function() {
                __#oTable#();
                <!--- for filter --->
                <cfif request.grid.hasFilter>
                    $('###Attributes.id# .grid_filter select').multiselect({
						enableCollapsibleOptGroups: #request.grid.filters[1].group#,
						maxHeight: 400,
                        buttonContainer: '<div class="btn-group-sm btn-group"/>',
                        nonSelectedText: '<i class="fa fa-filter"></i>',
                        enableHTML:true,
                        nSelectedText: ' - Filters selected!',
                        onInitialized: function(select, container) {
                            <cfinclude template = "inc_select_opt.cfm"/>
                        },
                        onChange: function(option, checked, select) {
                            <cfinclude template = "inc_select_opt.cfm"/>
                        }
                    });
                </cfif>

                <cfif isdefined("crequest.grid.toolbar[1]")>
                    #_tags_search#.tagsinput({
                        confirmKeys: [32, 13, 9],
                        maxTags: 5,
                        trimValue: true,
                        allowDuplicates: false
                    });

                    $("###attributes.id# .search .bootstrap-tagsinput input").keyup(function(e) {
                        var _x = $(#_tags_search#).val();
                        <cfswitch expression="#request.grid.toolbar[1].on#">
                            <cfcase value="Any">
                                #oTable#.search( _x ).draw();
                            </cfcase>
                            <cfdefaultcase>
                                if(e.which == 13) {
                                    #oTable#.search( _x ).draw();
                                }
                            </cfdefaultcase>
                        </cfswitch>
                    });
                </cfif>
            });

            function _executecmd(_url, _this, irow)  {
                if(confirm('Are you sure?')) {
                    ajaxRequest(_url,_this,x__gridremoveTR('#Attributes.id#_tbl',irow));
                }
            }

            function renderCell(_url, _this, irow)  {
                if(confirm('Are you sure?')) {
                    ajaxRequest(_url,_this,x__gridremoveTR('#Attributes.id#_tbl',irow));
                }
            }

        </script>

    </cfif>

</cfoutput>

<cffunction name="toggle" access="private" returntype="boolean">
    <cfargument name="b" required="true" type="boolean">

    <cfset var j = true/>
    <cfif arguments.b>
        <cfset j = false/>
    </cfif>

    <cfreturn j/>
</cffunction>