<cfoutput>
<cfif ThisTag.ExecutionMode EQ "Start">

    <cfparam name="Attributes.TagName" type="string" default="Buttons"/>
    <cfparam name="Attributes.iconprefix" type="string" default="fa fa-"/>
    <cfparam name="Attributes.buttonprefix" type="string" default="btn-"/>
    <cfparam name="Attributes.align" type="string" default="right"/>

    <cfassociate basetag="cf_ToolBar"/>

    <cfset request.grid.filters = ArrayNew(1)/>
    <cfset request.grid.buttons = ArrayNew(1)/>

	<cfif request.grid.hasSearch>

		<div class="col-sm-8 col-xs-6">

	</cfif>

	<div class="toolbar-btn-action pull-#Attributes.align#">

<!--- close tag --->
<cfelse>

	<cfloop list="#request.grid.tag.export#" item="_export_type">

		<cfif _export_type eq "html">
			<cfset _icon = "file-code-o"/>
		<cfelse>
			<cfset _icon = "file-#_export_type#-o"/>
		</cfif>
		<cfset ArrayAppend(request.grid.buttons, {
			title 			: "",
			help 			: "Export to #ucase(_export_type)#",
			url 			: "awaf/tags/grid/export.cfm?type=#_export_type#&grid_name=#request.grid.tag.name#",
			ignoreRoleCheck : true,
			icon 			: _icon,
			type 			: "external",
			changeURL		: false
		})/>

    </cfloop>

	<cfloop array="#request.grid.buttons#" item="button">

		<cfset buildLink = false/>
		<cfset _url = button.url/>
		<!--- make room for type:execute --->
		<cfif button.type eq "execute">
			<cfset method =  listlast(listfirst(listlast(button.url,'?'),'&'),'=')/>
			<cfset control = replace(listFirst(button.url,'.'),'/','.')/>
			<cfset _url = control & '.' & method/>
		</cfif>
		<!----TODO: turn the above to a global function --->
		<cfif listFindNoCase(request.user.pageURLs, listfirst(_url,'@'))>
			<cfset buildLink = true/>
		</cfif>

		<cfif button.ignoreRoleCheck>
			<cfset buildLink = true/>
		</cfif>

		<cfif buildLink>

			<cfif !isdefined('button.showOnhistoryPane')>
				<cfset button.showOnhistoryPane = "false"/>
			</cfif>
			<cfif !isdefined('button.forcePageReload')>
				<cfset button.forcePageReload = "false"/>
			</cfif>
			<cfif !isdefined('button.icon')>
				<cfset button.icon = ""/>
			</cfif>
			<cfif !isdefined('button.style')>
				<cfset button.style = "link"/>
			</cfif>
			<cfif !isdefined('button.type')>
				<cfset button.type = "external"/>
			</cfif>

			<cfswitch expression="#button.type#">

				<cfcase value="external">

					<a class="btn #Attributes.buttonprefix##button.style# btn-sm" title="#button.help#" target="_blank" href="#button.url#">
						<cfif button.icon neq "">
							<i class="#Attributes.iconprefix##button.icon#"></i>
						</cfif>
						#button.title#
					</a>

				</cfcase>
				<cfcase value="modal">

					<a class="btn #Attributes.buttonprefix##button.style# btn-sm" title="#button.help#"
						onclick="showModal('#button.url#',{'title':'#button.modaltitle#', 'param':'#button.urlparam#'});">
						<cfif button.icon neq "">
							<i class="#Attributes.iconprefix##button.icon#"></i>
						</cfif>
						#button.title#
					</a>

				</cfcase>
				<cfcase value="execute">
					<!---- redirect url --->
					<cfset donefn = "function(){}"/>
					<cfif button.redirectURL neq "">
						<cfset _render = ""/>
						<cfif button.renderTo neq "">
							<cfset _render = ",'renderTo':'#button.renderTo#'"/>
						</cfif>
						<cfset donefn = "loadPage('#button.redirectURL#',{'forcePageReload':true,'changeURL':false#_render#})"/>
					</cfif>

					<cfif trim(button.confirm) == "">

						<a class="btn #Attributes.buttonprefix##button.style# btn-sm" title="#button.help#"
						onclick="ajaxRequest('#button.url#',{'button':this,'param':'#button.urlparam#', donefn:#donefn#});">

					<cfelse>

						<a class="btn #Attributes.buttonprefix##button.style# btn-sm" title="#button.help#"
						onclick="if(confirm('#button.confirm#')){ajaxRequest('#button.url#',{'button':this,'param':'#button.urlparam#', donefn:#donefn#});}">

					</cfif>

						<cfif button.icon neq "">
							<i class="#Attributes.iconprefix##button.icon#"></i>
						</cfif>
						#button.title#

					</a>
				</cfcase>
				<cfcase value="print">
					<cfset _printurl = replace(button.url,'.','/','all')/>
					<cfset _printurl = replace(_printurl,'@','&id=')/>
					<a class="btn #Attributes.buttonprefix##button.style# btn-sm" href="views/inc/print/print.cfm?page=#_printurl#" title="#button.help#" target="_blank">
						<cfif button.icon neq "">
							<i class="#Attributes.iconprefix##button.icon#"></i>
						</cfif>
						#button.title#
					</a>
				</cfcase>
				<cfcase value="blank">
					<cfset _printurl = replace(button.url,'.','/','all')/>
					<cfset _printurl = replace(_printurl,'@','.cfm?id=')/>
					<a class="btn #Attributes.buttonprefix##button.style# btn-sm" href="views/#_printurl#" title="#button.help#" target="_blank">
						<cfif button.icon neq "">
							<i class="#Attributes.iconprefix##button.icon#"></i>
						</cfif>
						#button.title#
					</a>
				</cfcase>
				<cfdefaultcase>

					<a class="btn #Attributes.buttonprefix##button.style# btn-sm" title="#button.help#" onclick="loadPage('#button.url#',{<cfif !button.changeURL>'changeURL':false,</cfif>'forcePageReload':#button.forcePageReload#, 'param':'#button.urlparam#' <cfif button.showOnhistoryPane>,'title':'#button.title#'</cfif><cfif button.renderTo neq "">,'renderTo':'#button.renderTo#'</cfif>})">
						<cfif button.icon neq "">
							<i class="#Attributes.iconprefix##button.icon#"></i>
						</cfif>
						#button.title#
					</a>

				</cfdefaultcase>

			</cfswitch>
		</cfif>

	</cfloop>

	</div>

</cfif>
</cfoutput>