<cfoutput>

	<cfif ThisTag.ExecutionMode EQ "Start">

	    <cfparam name="Attributes.TagName" type="string" default="Filter"/>
	    <cfparam name="Attributes.title" type="string" default=""/>
	    <cfparam name="Attributes.value" type="string" default="#Attributes.title#"/>
	    <cfparam name="Attributes.selected" type="boolean" default="false"/>

	    <cftry>
    		<cfassociate basetag="cf_filtergroup"/>
	    	<cfset basetag = getBaseTagData("cf_filtergroup")/>

    		<cfcatch type="any">

    			<cfassociate basetag="cf_filters"/>
	    		<cfset basetag = getBaseTagData("cf_filters")/>

    		</cfcatch>
	    </cftry>

		<option value="#basetag.Attributes.column#`#Attributes.value#" <cfif Attributes.selected>selected</cfif>>#Attributes.title#</option>

	</cfif>

</cfoutput>