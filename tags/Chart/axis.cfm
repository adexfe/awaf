<cfoutput>

	<cfif ThisTag.ExecutionMode EQ "Start">
	  
	    <cfparam name="Attributes.TagName" type="string" default="categories"/>
	    <cfparam name="Attributes.title" type="string" />
	    <cfparam name="Attributes.title_position" type="string" default="left"/>
	    <cfparam name="Attributes.seriesname" type="string" default="#Attributes.title#" />
	    <cfparam name="Attributes.numberPrefix" type="string" default="" /> 
	    <cfparam name="Attributes.color" type="string" default="" />  

	    
	        {
	            "title": "#Attributes.title#",
	            "titlepos": "#Attributes.title_position#",
	            <cfif attributes.title_position eq "right">
	            	"axisonleft": "0",
	            </cfif>
	            "tickwidth": "10",
	            "divlineisdashed": "1",
	            "numberPrefix":"#Attributes.numberPrefix#",
	            <cfif Attributes.color neq "">
	            	"color": "#Attributes.color#",
	            </cfif>
	            "numDivLines":"2",
	            "dataset": [ 
	<cfelse>
		<cfset Content = THISTAG.GeneratedContent />
		<cfset THISTAG.GeneratedContent = "" />
		 
							#Content#
			 
 
	            ]
	        },
	 	 
	</cfif> 

</cfoutput>