<cfoutput>

	<cfif ThisTag.ExecutionMode EQ "Start">

	    <cfparam name="Attributes.TagName" type="string" default="categories"/>
	    <cfparam name="Attributes.seriesname" type="string"/>
	    <cfparam name="Attributes.color" type="string" default="" />
	    <cfparam name="Attributes.renderAs" type="string" default="" /> <!---- line , area --->
	    <cfparam name="Attributes.parentYAxis" type="string" default="" /> <!---- P , S --->
	    <cfparam name="Attributes.showValues" type="string" default="0" />

            {
                "seriesname": "#Attributes.seriesname#",
                "showValues" : "#Attributes.showValues#",
                <cfif Attributes.color neq "">
                	"color":"#Attributes.color#",
                </cfif>
                <cfif Attributes.renderAs neq "">
                	"renderAs":"#Attributes.renderAs#",
                </cfif>
                <cfif Attributes.parentYAxis neq "">
                	"parentYAxis":"#Attributes.parentYAxis#",
                </cfif>

                "data": [
	<cfelse>
		<cfset Content = THISTAG.GeneratedContent />
		<cfset THISTAG.GeneratedContent = "" />

					#Content#

      		]
      	},

	</cfif>

</cfoutput>
