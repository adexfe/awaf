<cfoutput>

	<cfif thisTag.ExecutionMode eq 'start'>

			<cfparam name="Attributes.TagName" type="string" default="chart"/>
			<cfparam name="Attributes.type" type="string"/><!--- pie2d,doughnut2d,pie3d, MultiAxisLine,StackedColumn3DlineDY --->
			<cfparam name="Attributes.renderAt" type="string" default="#getRandomVariable()#"/>
			<cfparam name="Attributes.width" type="string" default="100%"/>
			<cfparam name="Attributes.align" type="string" default=""/>
			<cfparam name="Attributes.height" type="string" default="100%"/>
			<cfparam name="Attributes.Caption" type="string" default=""/>
			<cfparam name="Attributes.bgcolor" type="string" default="ffffff"/>
			<cfparam name="Attributes.bgAlpha" type="string" default="100"/>
			<cfparam name="Attributes.canvasBgColor" type="string" default="#Attributes.bgcolor#"/>
			<cfparam name="Attributes.captionFontColor" type="string" default=""/>
			<cfparam name="Attributes.captionAlignment" type="string" default=""/>
			<cfparam name="Attributes.captionFontBold" type="boolean" default="true"/>
			<cfparam name="Attributes.paletteColors" type="string" default=""/>
			<cfparam name="Attributes.baseFontColor" type="string" default="#Attributes.captionFontColor#"/>
			<cfparam name="Attributes.toolTipBgColor" type="string" default="#listlast(Attributes.paletteColors)#"/>
			<cfparam name="Attributes.theme" type="string" default="fint"/>
			<cfparam name="Attributes.class" type="string" default=""/>
			<cfparam name="Attributes.numDivLines" type="numeric" default="4"/>
			<cfparam name="Attributes.showDivLineValues" type="numeric" default="1"/>
			<cfparam name="Attributes.showLimits" type="numeric" default="1"/>
			<cfparam name="Attributes.showYAxisValues" type="numeric" default="1"/>

			<cfparam name="Attributes.labelDisplay" type="string" default="wrap"/>
			<cfparam name="Attributes.slantLabels" type="numeric" default="1"/>

			<cfparam name="Attributes.yAxisMaxValue" type="numeric" default="0" />
			<cfparam name="Attributes.yAxisMinValue" type="numeric" default="0" />
			<cfparam name="Attributes.pYAxisMinValue" type="numeric" default="0" />
			<cfparam name="Attributes.pYAxisMaxValue" type="numeric" default="0" />
			<cfparam name="Attributes.sYAxisMinValue" type="numeric" default="0" />
			<cfparam name="Attributes.sYAxisMaxValue" type="numeric" default="0" />

			<cfparam name="Attributes.pYAxisName" type="string" default="" />
			<cfparam name="Attributes.sYAxisName" type="string" default="" />



        <cfparam name="Attributes.xAxisName" type="string" default="" />
        <cfparam name="Attributes.xAxisNameFontColor" type="string" default="#Attributes.captionFontColor#"/>
        <cfparam name="Attributes.xAxisNameFontBold" type="string" default="#Attributes.captionFontBold#"/>
        <cfparam name="Attributes.divlinecolor" type="string" default="cccccc"/>

        <cfparam name="Attributes.showvalues" type="numeric" default="1"/>
        <cfparam name="Attributes.showLabels" type="numeric" default="1"/>
        <cfparam name="Attributes.showPercentValues" type="numeric" default="1"/>

        <cfparam name="Attributes.showlegend" type="numeric" default="0"/>
        <cfparam name="Attributes.legendshadow" type="numeric" default="0"/>
        <cfparam name="Attributes.showXAxisValues" type="numeric" default="0"/>


        <!---- pie ---->
        <cfparam name="Attributes.enablesmartlabels" type="numeric" default="1"/>
        <!------------->
        <cfswitch expression="#Attributes.Type#">
            <cfcase value="MultiAxisLine" delimiters=",">

            </cfcase>
            <cfcase value="StackedColumn3DlineDY,mscombidy2d">
                <cfparam name="Attributes.sNumberPrefix" type="string" default="" />
                <cfparam name="Attributes.sNumberSuffix" type="string" default="" />
            </cfcase>
        </cfswitch>

		  <cfparam name="Attributes.NumberPrefix" type="string" default="" />

        <cfset fusionid = getRandomVariable()/>

        <div id="#Attributes.renderAt#" class="#Attributes.class#" <cfif Attributes.align neq ""> align="#Attributes.align#" </cfif>></div>

        <script type="text/javascript">

     FusionCharts.ready(function(){

         var #fusionid# = new FusionCharts({
            "type": "#Attributes.type#",
            "renderAt": "#Attributes.renderAt#",
            "width": "#Attributes.width#",
            "height": "#Attributes.height#",
            "dataFormat": "json",
            "dataSource": {
                "chart": {
						<cfif Attributes.yAxisMaxValue gt 0>"yAxisMaxValue":"#Attributes.yAxisMaxValue#",</cfif>
						<cfif Attributes.yAxisMinValue gt 0>"yAxisMinValue":"#Attributes.yAxisMinValue#",</cfif>
						<cfif Attributes.pYAxisMinValue gt 0>"pYAxisMinValue":"#Attributes.pYAxisMinValue#",</cfif>
						<cfif Attributes.pYAxisMaxValue gt 0>"pYAxisMaxValue":"#Attributes.pYAxisMaxValue#",</cfif>
						<cfif Attributes.sYAxisMaxValue gt 0>"sYAxisMaxValue":"#Attributes.sYAxisMaxValue#",</cfif>
						<cfif Attributes.sYAxisMinValue gt 0>"sYAxisMinValue":"#Attributes.sYAxisMinValue#",</cfif>
						<cfif Attributes.NumberPrefix neq "">"NumberPrefix":"#Attributes.NumberPrefix#",</cfif>
						<cfif Attributes.theme neq "">"theme":"#Attributes.theme#",</cfif>
                  <cfif Attributes.captionFontColor neq "">"captionFontColor": "#Attributes.captionFontColor#",</cfif>
                    <cfif !Attributes.captionFontBold>
                        "captionFontBold" : "0",
                    </cfif>
                    <cfif Attributes.captionAlignment neq "">
                    </cfif>
                    <cfif Attributes.captionAlignment neq "">
                        "captionAlignment": "#Attributes.captionAlignment#",
                    </cfif>
                    <cfif Attributes.paletteColors neq "">
                        "paletteColors": "#Attributes.paletteColors#",
                    </cfif>
                    <cfif Attributes.baseFontColor neq "">
                        "baseFontColor":"#Attributes.baseFontColor#",
                    </cfif>
                    <cfif Attributes.toolTipBgColor neq "">
                        "toolTipBgColor":"#Attributes.toolTipBgColor#",
                        "toolTipBorderColor":"#Attributes.toolTipBgColor#",
                    </cfif>
                    "showLimits":"#Attributes.showLimits#","showYAxisValues":"#Attributes.showYAxisValues#",
                    "caption": "#Attributes.Caption#", "bgcolor": "#Attributes.bgcolor#", "use3dlighting": "0",
                    "showborder": "0", "showshadow": "0", "legendbgcolor": "##CCC",
                    "legendshadow": "#Attributes.legendshadow#", "legendborderalpha": "0",
                    "showLabels" : "#Attributes.showLabels#",
                    "labeldisplay": "#Attributes.labelDisplay#", "slantLabels":"#Attributes.slantLabels#", "showcanvasborder": "0", "showvalues": "#Attributes.showvalues#", "showlegend": "#Attributes.showlegend#",
                    "legendborder": "0", "legendposition": "bottom", "bgAlpha":"#Attributes.bgAlpha#",
                    "canvasBgColor":"#Attributes.canvasBgColor#", "setAdaptiveYMin":"1", "showDivLineValues":"#Attributes.showDivLineValues#",
                    <!--- pie ---->
            <cfswitch expression="#Attributes.Type#">
                <cfcase value="pie2d,doughnut2d,pie3d" delimiters=",">

                    "showpercentvalues": "#attributes.showPercentValues#",
                    "showplotborder": "0", "startingAngle":"0", "enablesmartlabels": "#Attributes.enablesmartlabels#"

                </cfcase>

                <cfcase value="bar2d">

                    "usePlotGradientColor": "0",
                    "plotBorderAlpha": "10",
                    "placeValuesInside": "1",
                    "valueFontColor": "##ffffff",
                    "showAxisLines": "1",
                    "axisLineAlpha": "25",
                    "divLineAlpha": "10",

                </cfcase>

                <cfcase value="MultiAxisLine,">
                    "showalternatehgridcolor": "0", "divlinecolor": "#attributes.divlinecolor#",
                    "linethickness": "1", "anchorRadius":2,
                </cfcase>

                <cfcase value="StackedColumn3DlineDY,mscombidy2d" >
                    <cfif attributes.pYAxisName neq "">"pYAxisName": "#attributes.pYAxisName#",</cfif> "sYAxisName": "#attributes.sYAxisName#", "sNumberSuffix": "#attributes.sNumberSuffix#","sNumberPrefix": "#attributes.sNumberPrefix#",
                </cfcase>

            </cfswitch>

            <cfswitch expression="#Attributes.Type#">
                <cfcase value="mscolumn2d,stackedcolumn2d,stackedcolumn3d,MultiAxisLine" delimiters=",">
                    "numDivLines":"#Attributes.numDivLines#",
                    "xAxisName" : "#attributes.xAxisName#",
                    <cfif Attributes.xAxisNameFontColor neq "">
                        "xAxisNameFontColor" : "#Attributes.xAxisNameFontColor#",
                    </cfif>
                    <cfif !Attributes.xAxisNameFontBold >
                        "xAxisNameFontBold" : "0",
                    </cfif>
                    <!---"divLineColor" : "efefef",--->
                </cfcase>
            </cfswitch>

                }

    <cfelse>

            }

        });

        #fusionid#.render();
    });

        </script>

    </cfif>

</cfoutput>
