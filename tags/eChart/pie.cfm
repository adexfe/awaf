<cfoutput>

	<cfif thisTag.ExecutionMode eq 'start'>

		<cfparam name="Attributes.TagName" type="string" default="Pie"/>
		<cfparam name="Attributes.name" type="string"/>
		<cfparam name="Attributes.datasource" type="any"/>

		<!--- pie --->
		<cfparam name="Attributes.center" type="string" default=""/>
		<cfparam name="Attributes.radius" type="string" default=""/>
		<cfparam name="Attributes.roseType" type="string" default=""/><!---- area, radius --->
		<cfparam name="Attributes.labelPosition" type="string" default=""/>

		<cfset cf_series = getBaseTagData("cf_series").Attributes/>

		{
			name:'#Attributes.name#',
			tooltip : {
				trigger: 'item',
				formatter: function(params)	{
					return params.seriesName + '<br/>' + params.name + ' : ' + moneyFormat(params.value) + ' (' + params.percent + '%)';
				}
			},
			<cfif Attributes.center neq "">center: [#Attributes.center#],</cfif>
			<cfif Attributes.radius neq "">radius: [#Attributes.radius#],</cfif>
			<cfif Attributes.labelPosition neq "">
            label: {
                normal: {
                    position: '#Attributes.labelPosition#'
                }
            },
			</cfif>
			type: 'pie',
			<cfif Attributes.roseType neq "">roseType:'#Attributes.roseType#',</cfif>
			data:[

				<cfif isquery(Attributes.datasource)>

				<cfelseif IsArray(Attributes.datasource)>
					<cfloop array="#Attributes.datasource#" item="x">
						{value:#x.value#, name:'#x.name#'},
					</cfloop>
				<cfelse>
					'#replace(Attributes.datasource,",","','","all")#'
				</cfif>

			]
		},

	</cfif>

</cfoutput>