<cfoutput>

	<cfif thisTag.ExecutionMode eq 'start'>

		<cfparam name="Attributes.TagName" type="string" default="legend"/>
		<cfparam name="Attributes.value" type="string" default=""/>
		<cfparam name="Attributes.orient" type="string" default=""/> 		<!--- horizontal, vertical --->
		<cfparam name="Attributes.left" type="string" default=""/>
		<cfparam name="Attributes.top" type="string" default=""/>

		<cfset cf_echart = getBaseTagData("cf_echart").Attributes/>

		legend: {
			<cfif Attributes.orient neq "">orient: '#Attributes.orient#',</cfif>
			<cfif Attributes.left neq "">left: '#Attributes.left#',</cfif>
			<cfif Attributes.top neq "">top: '#Attributes.top#',</cfif>
			data:['#replace(Attributes.value,",","','","all")#']
		},

		</cfif>

</cfoutput>