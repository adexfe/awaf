<cfoutput>

	<cfif thisTag.ExecutionMode eq 'start'>

		<cfparam name="Attributes.TagName" type="string" default="echart"/>
		<cfparam name="Attributes.id" type="string" default="#getRandomVariable()#"/>
		<cfparam name="Attributes.width" type="string" default="100%"/>
		<cfparam name="Attributes.align" type="string" default=""/>
		<cfparam name="Attributes.height" type="string" default="200px"/>
		<cfparam name="Attributes.right" type="string" default=""/>
		<cfparam name="Attributes.left" type="string" default=""/>
		<cfparam name="Attributes.Title" type="string" default=""/>
		<cfparam name="Attributes.SubTitle" type="string" default=""/>
		<cfparam name="Attributes.TitleAlign" type="string" default="center"/>
		<cfparam name="Attributes.class" type="string" default=""/>
		<cfparam name="Attributes.onClick" type="string" default=""/><!--- js code here --->

		<cfparam name="Attributes.type" type="string" default="mix"/>	<!--- the type of chart to render --->

		<cfparam name="Attributes.showToolbox" type="boolean" default="true"/>

		<div id="#Attributes.id#" class="#Attributes.class#" <cfif Attributes.align != ""> align="#Attributes.align#" </cfif> style="width:#Attributes.width#; height:#Attributes.height#"></div>

		<cfset echartid = getRandomVariable()/>
		<cfset opt_var = getRandomVariable()/>

		<cfparam name="request.tag.echart.to_resize" default=""/>

		<cfset request.tag.echart.to_resize = ListAppend(request.tag.echart.to_resize, "#echartid#.resize()", ";")/>

		<script type="text/javascript">

			var #echartid# = echarts.init(document.getElementById('#Attributes.id#'),'default');

	<cfelse>

		<cfset Content = THISTAG.GeneratedContent />
		<cfset THISTAG.GeneratedContent = "" />

			var #opt_var# = {
				grid : {
					<cfif Attributes.left != "">left:'#Attributes.left#',</cfif>
					<cfif Attributes.right != "">right:'#Attributes.right#'</cfif>
				},
				<cfif Attributes.Title != "" || Attributes.SubTitle != "">
				title: [
					{
						text: '#Attributes.Title#',
						<cfif Attributes.SubTitle != "">subtext: '#Attributes.SubTitle#',</cfif>
						left: '#Attributes.TitleAlign#'
					}
				],
				</cfif>
				tooltip : {
					<cfswitch expression="#attributes.type#">
						<cfcase value="mix">
							trigger: 'axis',
						</cfcase>
						<cfcase value="pie">
							trigger: 'item',
						</cfcase>
					</cfswitch>
					axisPointer:{
						show: true,
						type:'shadow',
						shadowStyle : {
							color: 'rgba(150,150,150,0.1)'
						}
					}
				},

				<cfswitch expression="#attributes.type#">
					<cfcase value="mix">
						calculable : true,
					</cfcase>
				</cfswitch>

				#Content#

			}

			#echartid#.setOption(#opt_var#);
			//$(window).resize(function(e) {
			//});
			window.onresize = function() {
				#request.tag.echart.to_resize#
			};
			<cfif attributes.OnClick != "">
				#echartid#.on('click', function(e) 	{
					#attributes.OnClick#
				});
			</cfif>


		</script>

	</cfif>

</cfoutput>