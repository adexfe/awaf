<cfoutput>
	<cfif thisTag.ExecutionMode eq 'start'>

		<cfparam name="Attributes.TagName" type="string" default="Form"/>
		<cfparam name="Attributes.current_url" type="string" default="#url.current_page_url#"/>
		<cfparam name="Attributes.action" type="string" default=""/>
		<cfparam name="Attributes.useplaceholder" type="boolean" default="false"/>
		<cfparam name="Attributes.id" type="string" default="#getRandomVariable()#"/>
		<cfparam name="Attributes.alertPages" type="string" default=""/><!--- use this to display alert when data has changes --->
		<cfparam name="Attributes.containnerid" type="string" default="app_content"/>
		<cfparam name="Attributes.class" type="string" default=""/>

		<cfset request.form.input = ArrayNew(1)/>
		<cfset request.form.textarea = ArrayNew(1)/>
		<cfset request.form.button = ArrayNew(1)/>
		<cfset request.form.submit = ArrayNew(1)/>
		<cfset request.form.radio = ArrayNew(1)/>
		<cfset request.form.check = ArrayNew(1)/>
		<cfset request.form.select = ArrayNew(1)/>
		<cfset request.form.select2 = ArrayNew(1)/>
		<cfset request.form.tag = ArrayNew(1)/>
		<cfset request.form.datePicker = ArrayNew(1)/>
		<cfset request.form.time = ArrayNew(1)/>
		<cfset request.form.file = ArrayNew(1)/>

		<form id="#Attributes.id#" method="post" enctype="multipart/form-data" action="#Attributes.action#" class="#Attributes.class#">

	<cfelse>

		</form>

		<script type="text/javascript">
			$(document).ready(function() {

				// form id
				<cfset formId = getRandomVariable()/>
				#formId# = $('###Attributes.id#');
				<cfif arraylen(request.form.radio) or arraylen(request.form.check)>
					$('###Attributes.id# input.icheck').iCheck({checkboxClass: 'icheckbox_square', radioClass: 'iradio_square'});
				</cfif>
				<cfloop array="#request.form.input#" item="mn">
					<cfif isDefined("mn.type") and mn.type eq "money">
						$('###Attributes.id# input.tag-xmoney').maskMoney();
						$('###Attributes.id# input.tag-xmoney').maskMoney('mask');
						<cfbreak>
					</cfif>
				</cfloop>

				<cfset date_picker_len = request.form.datePicker.len()/>

				// confirmation


				// start validation
				#formId#.validate({
					onsubmit: true,
					debug: true,
					rules: {
						<cfset i = 0/>
						<cfloop array="#request.form.datePicker#" item="attr">
							<cfset i++/>
							'#attr.name#': {
								required: #attr.required#,
								date: true
							} <cfif i is not date_picker_len>,</cfif>
						</cfloop>
					},
					errorPlacement: function(error, element)  {
						//console.log(error);
						if (element.is("input.addon"))    {
							error.appendTo(element.parent().parent());
						}
						else if(element.is(".icheck"))   {
							error.appendTo(element.parent().parent().parent());
						}
						else    {
							error.appendTo(element.parent());
						}

					},

					submitHandler: function(e) {
						<cfset thisForm = getRandomVariable()/>
						<cfset sel_button = getRandomVariable()/>
						<cfset clearform = getRandomVariable()/>
						<cfset flashmsg = getRandomVariable()/>
						<cfset rdi = getRandomVariable()/>
						<cfset confirm = getRandomVariable()/>
						<cfset rdi_target = getRandomVariable()/>
						<cfset submit_buttons = getRandomVariable()/>
						<cfset cls_modal_as = getRandomVariable()/>

						var #thisForm# = $(e);
						var #sel_button# = $('###Attributes.id# ##'+#thisForm#.attr('button-submitted'));
						var #clearform# = #sel_button#.attr('clear-form');
						var #flashmsg# = #sel_button#.attr('flash-msg');
						var #rdi# = #sel_button#.attr('redirect-url');
						var #confirm# = #sel_button#.attr('confirm');
						var #rdi_target# = #sel_button#.attr('redirect-target');
						var #cls_modal_as# = #sel_button#.attr('close-modal-after-save');

						// get all buttons on the form
						#submit_buttons# = $('###Attributes.id# button[type="submit"]');

						// un mask money;
						<cfloop array="#request.form.input#" item="mn">
							<cfif isdefined("mn.type") and mn.type eq "money">
								document.getElementById("#mn.id#").value=$('###Attributes.id#  ###mn.id#').maskMoney('unmasked')[0];
							</cfif>
						</cfloop>

						$.ajax({
							cache: false,
							beforeSend:function()   {

								#submit_buttons#.addClass('disabled');
								#sel_button#.addClass('loading');

							},
							url: #formId#.attr('action'),
							type: 'POST',
							data: #formId#.serialize()
						}).done(function(data)  {
							try{
								data = $.parseJSON(data);
							} catch (exception) {
								data.KEY = 'ID';
								data.ID = 0;
							}
							if(#flashmsg# != '')    {
								itemCreatedNotice(#flashmsg#);// + '. ID: ' + data[data.KEY]);
							}
							//console.log(data[data['KEY']]);
							// determine what to do from the button clicked
							if(#clearform#=='true')   {#thisForm#[0].reset();}
							//alert(#rdi#);
							if(#rdi#!='')   {
								<cfset curl = replace(Attributes.current_url,'.','_','all')/>
								<cfset curl = replace(curl,'@','_')/>
								if (data != undefined) {
									#rdi# = #rdi#.replace('@key','@'+data[data['KEY']]);
								}
								if(#rdi_target#!="") 	{
									<!--- implement target page ---->
									if($('##' + #rdi_target#).length>0)	{
										loadPage(#rdi#, {'forcePageReload':true, 'changeURL': false, 'renderTo': #rdi_target#, 'param':'target='+#rdi_target#});
									}
									else 	{
										console.log('Warning: target element id #rdi_target# does not exist');
									}
								}
								else 	{
									loadPage(#rdi#, {'forcePageReload':true, 'param':'target='+#rdi_target#});
								}
								$('###curl#').remove();
							// find all open app_page using this data and display a notice message
							}
							<!--- cfset id_ = ''/>
							<cfloop list="#attributes.alertPages#" item="id">
								<cfset id_ = listAppend(id_,"[id*='#id#']")/>
							</cfloop>
							//$('###attributes.containnerid#').find("#id_#").prepend("hi");--->

							if(#cls_modal_as# == 'true')    {
								$('##__app_modal').modal('hide');
							}

						}).fail(function(xhr)  {

							showError(xhr);

						}).always(function(data) {

							#submit_buttons#.removeClass('disabled loading');

						});
					}

				});

				<cfloop array="#request.form.submit#" item="x" index="i">
					$('###x.id#').click(function()    {
						//alert('testing click function');
						#formId#.attr('action','controllers/#x.url#');
						// add something to the form to know the button that was clicked
						#formId#.attr('button-submitted','#x.id#');
						// redirect
						#formId#.attr('redirect-url','#x.redirectURL#');
					});
				</cfloop>
				<cfloop array="#request.form.tag#" item="x" index="i">
					$("###Attributes.id# ###x.id#").select2({
						<cfset _c = replace(x.value,',','","','all')/>
						tags:["#_c#"],
						tokenSeparators: [","]
						<cfif x.maximumSelectionSize>
							,maximumSelectionSize: #x.maximumSelectionSize#
						</cfif>
					});
				</cfloop>
				// loop through select options if any then activate the select2 component
				<cfloop array="#request.form.select2#" item="x" index="i">
					$("###Attributes.id# ###x.id#").select2({
						placeholder: '#x.placeholder#',
						<cfif !x.URL.isempty()>
							initSelection : function (element, callback) {
								var vl = element.val().split(":"),data = {id: vl[0], text: vl[1]}; callback(data);
							},
							<cfif x.multiple>
								multiple:true,
							</cfif>
							<cfif x.Tagging>
								tokenSeparators: [","," "],
								createSearchChoice: function(term, data) {
									if ($(data).filter(function() {return this.text.localeCompare(term) === 0;}).length === 0) { return {id: term,text: term};}
								},
							</cfif>
							minimumInputLength: 1,
							ajax: {dataType: "json",
								url: "controllers/#x.URL#",
								data: function (term, page) {  return {q: term, page_limit: 10,page: page};},
								results: function (data) {return {results: data};}
							}
						</cfif>
					});
				</cfloop>
				<cfif date_picker_len>
					$('###Attributes.id# .datePicker').datePicker();
				</cfif>

			});
		</script>

	</cfif>

</cfoutput>