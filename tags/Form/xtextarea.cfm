<cfoutput>
<cfif ThisTag.ExecutionMode EQ "Start">

    <cfparam name="Attributes.TagName" type="string" default="Textarea"/>
    <cfparam name="Attributes.required" type="boolean" default="false"/>
    <cfparam name="Attributes.name" type="string"/>
    <cfparam name="Attributes.value" type="string" default=""/>
    <cfparam name="Attributes.help" type="string" default=""/>
    <cfparam name="Attributes.label" type="string" default="#Attributes.name#"/>
    <cfparam name="Attributes.size" type="string" default="col-sm-6"/>
    <cfparam name="Attributes.id" type="string" default="#getRandomVariable()#"/>
    <cfparam name="Attributes.rows" type="numeric" default="3"/>
    <cfparam name="Attributes.style" type="string" default=""/>
    <cfparam name="Attributes.editor" type="string" default="Trumbowyg"/> <!--- Trumbowyg, froala --->
    <cfparam name="Attributes.html" type="boolean" default="false"/>


 	<cfset ArrayAppend(request.form.textarea,Attributes)/>

 	<cfset cf_xform = getBaseTagData("cf_xform").Attributes/>

 	<cftry>

 		<cfset cf_row = getBaseTagData("cf_frow").ATTRIBUTES.TagName/>
 		<cfcatch type="any">
			<cfset cf_row = ""/>
 		</cfcatch>

 	</cftry>


<cfelse>

	<cfif cf_row eq "">
		<div class="form-group">
	<cfelse>
		<div class="#Attributes.size# pad-top-10">
	</cfif>

		<cfif !cf_xform.useplaceholder>
			<label for="#Attributes.id#">#Attributes.label#<cfif Attributes.required> <span class="red">*</span></cfif> </label>
		</cfif>
		<div class="#Attributes.id#">
			<textarea
				<cfif Attributes.style neq "">style="#Attributes.style#"</cfif>
				class="form-control " rows="#Attributes.rows#" name="#Attributes.name#" id="#Attributes.id#"
				<cfif cf_xform.useplaceholder> placeholder="#Attributes.label#" </cfif>
				<cfif Attributes.required> required </cfif>
			>#attributes.value#</textarea>
		</div>
		<cfif Attributes.help neq "">
			<small class="help-block">#Attributes.help#</small>
		</cfif>

	<cfif cf_row eq "">
		</div>
	<cfelse>
		</div>
	</cfif>

	<cfif Attributes.html>
		<cfswitch expression="#Attributes.editor#">
			<cfcase value="Trumbowyg">

				<style>
					.#Attributes.id# .trumbowyg-editor, .#Attributes.id# .trumbowyg-textarea, .#Attributes.id# .trumbowyg-box{
						min-height: #Attributes.rows*10#px !important;
					}
				</style>
				<script>
					$('###Attributes.id#').trumbowyg({
						btns: [['bold', 'italic'], ['link'],['insertImage'],['unorderedList', 'orderedList']]
					});
				</script>

			</cfcase>
			<cfcase value="froala">
				<style>
					.#Attributes.id# .fr-element{min-height: #Attributes.rows*20#px !important;}
				</style>
				<script>
					$(function() {
						$('###Attributes.id#').froalaEditor({
							toolbarButtons: ['bold', 'italic', 'underline', '|','align','|','formatOL','formatUL','outdent', 'indent', 'clearFormatting', 'insertTable', 'html'],
							enter: $.FroalaEditor.ENTER_BR,
							toolbarSticky: false
						});
					});
				</script>

			</cfcase>
		</cfswitch>

	</cfif>
</cfif>
</cfoutput>