<cfoutput>
<cfif ThisTag.ExecutionMode EQ "Start">

    <cfparam name="Attributes.TagName" type="string" default="Input"/>
    <cfparam name="Attributes.type" type="string" default="text"/><!--- number, text, date --->
    <cfparam name="Attributes.name" type="string"/>
    <cfparam name="Attributes.value" type="string" default=""/>

    <cfparam name="Attributes.required" type="boolean" default="false"/>
    <cfparam name="Attributes.readonly" type="boolean" default="false"/>
    <cfparam name="Attributes.min" type="any" default="0"/> <!--- numeric/date --->
    <cfparam name="Attributes.max" type="any" default="0"/> <!--- numeric/date --->

    <cfparam name="Attributes.class" type="string" default=""/>

    <cfparam name="Attributes.help" type="string" default=""/>
    <cfparam name="Attributes.label" type="string" default="#Attributes.name#"/>
	<cfif attributes.label eq "">
		<cfset attributes.label = "&nbsp;"/>
	</cfif>
    <cfparam name="Attributes.hideLabel" type="boolean" default="false"/>
    <cfparam name="Attributes.size" type="string" default="col-sm-6"/>
    <cfparam name="Attributes.id" type="string" default="#getRandomVariable()#"/>

    <cfparam name="Attributes.allowClear" type="boolean" default="false"/><!--- for date --->

 	<!--- date- ---->

 		<cfparam name="Attributes.dateformat" type="string" default="yyyy-mm-dd"/>
 		<cfparam name="Attributes.startDate" type="string" default=""/>

		<cfif isdate(attributes.min)>
			<cfset Attributes.startDate = dateFormat(Attributes.min, 'yyyy-mm-dd')/>
		</cfif>

		<cfif isdate(attributes.max)>
			<cfset Attributes.endDate = dateFormat(Attributes.max, 'yyyy-mm-dd')/>
		</cfif>

 		<cfif Attributes.startDate != "">
 			<cfset Attributes.startDate = dateFormat(Attributes.startDate, 'yyyy-mm-dd')/>
 		</cfif>
 		<cfparam name="Attributes.endDate" type="string" default=""/>

 		<cfparam name="Attributes.startView" type="string" default="2"/>

 		<cfif Attributes.type eq "date">
 			<cfparam name="Attributes.minView" type="string" default="2"/>
 		<cfelse>
 			<cfparam name="Attributes.minView" type="string" default="0"/>
 		</cfif>
 		<!---
	 		0 or 'hour' for the hour view
			1 or 'day' for the day view
			2 or 'month' for month view (the default)
			3 or 'year' for the 12-month overview
			4 or 'decade' for the 10-year overview. Useful for date-of-birth datetimepickers.
		--->

 	<!---- datetime ----->

 		<cfparam name="Attributes.datetimeformat" type="string" default="yyyy-mm-dd H:ii P"/>


	<!-------------------->

	<!--- money ---->
    <cfparam name="Attributes.precision" type="numeric" default="2"/>
    <cfparam name="Attributes.prefix" type="string" default=""/>

	<!-------------->

    <cfif attributes.type is 'date'>
    	<cfif isdate(attributes.value)>
    		<cfset Attributes.value = dateFormat(Attributes.value,'yyyy-mm-dd')/>
    	</cfif>
    </cfif>
 	<cfset ArrayAppend(request.form.input,Attributes)/>

 	<cfset cf_xform = getBaseTagData("cf_xform").Attributes/>

 	<cftry>

 		<cfset cf_row = getBaseTagData("cf_frow").ATTRIBUTES.TagName/>
 		<cfcatch type="any">
			<cfset cf_row = ""/>
 		</cfcatch>

 	</cftry>


<cfelse>


	<cfif cf_row eq "">
		<div class="form-group">
	<cfelse>
		<div class="#Attributes.size# pad-top-10">
	</cfif>

			<cfset cls = "form-control"/>
    		<cfif Attributes.type is "date">
    			<cfset cls = ""/>
    		</cfif>

				<cfif !Attributes.hidelabel>
				<cfif !cf_xform.useplaceholder>
					<label for="#Attributes.id#">#Attributes.label# <cfif Attributes.required><span class="red">*</span></cfif></label>
				</cfif>
			</cfif>

		    <cfswitch expression="#Attributes.type#">

		    	<cfcase value="date">
	               	<div class="input-group date form_date" id="dtp#Attributes.id#" data-date="#Attributes.value#" data-date-format="#Attributes.dateformat#" data-link-field="#Attributes.id#">
	                    <input class="form-control #Attributes.class#" type="text" name="#Attributes.id#" <cfif Attributes.readonly>readonly</cfif> <cfif Attributes.required>required readonly</cfif> value="#Attributes.value#" >
	                  	<cfif Attributes.AllowClear>
	                  		<span class="input-group-addon"><span class="fa fa-times"></span></span>
	                  	</cfif>
						<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
	               	</div>
					<input type="hidden" id="#Attributes.id#" value="#Attributes.value#" name="#Attributes.name#"/>
		    	</cfcase>

		    	<cfcase value="datetime">
	                <div class="input-group date form_datetime" id="dtp#Attributes.id#" data-date="#Attributes.value#" data-date-format="#Attributes.datetimeformat#" data-link-field="#Attributes.id#">
	                    <input class="form-control #Attributes.class#" type="text" <cfif Attributes.readonly>readonly</cfif> name="#Attributes.id#" <cfif Attributes.required>required readonly</cfif> value="#Attributes.value#" >
	                	<cfif Attributes.AllowClear>
	                  		<span class="input-group-addon"><span class="fa fa-times"></span></span>
	                  	</cfif>
						<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
	                </div>
					<input type="hidden" id="#Attributes.id#" value="#Attributes.value#" name="#Attributes.name#" />
		    	</cfcase>

				<cfcase value="money">
		    		<input
						type="text"
						data-allow-zero="true"
						<cfif Attributes.prefix neq "">data-prefix="#Attributes.prefix# "</cfif>
						<cfif Attributes.readonly>readonly</cfif>
						value="#trim(numberformat(attributes.value,'9,999.9999'))#" class="#cls# tag-xmoney text-right"
						name="#Attributes.name#"
						id="#Attributes.id#"
						data-precision="#Attributes.precision#"
				    	<cfif cf_xform.useplaceholder>placeholder="#Attributes.label#"</cfif>
				    	<cfif Attributes.required>required </cfif>
						<!---TODO: min and max not supported yet because of the , in the money --->
				    	<!----cfif val(Attributes.min)>min="#Attributes.min#"</cfif>
				    	<cfif val(Attributes.max)>max="#Attributes.max#"</cfif---->
		    		/>
				</cfcase>

				<cfcase value="number">
		    		<input type="number" value="#attributes.value#" <cfif Attributes.readonly>readonly</cfif> class="#cls#  #Attributes.class#" name="#Attributes.name#" id="#Attributes.id#"
				    	<cfif cf_xform.useplaceholder>placeholder="#Attributes.label#"</cfif>
				    	<cfif Attributes.required>required </cfif>
				    	<cfif val(Attributes.min)>min="#Attributes.min#"</cfif>
				    	<cfif val(Attributes.max)>max="#Attributes.max#"</cfif>
		    		/>
				</cfcase>

		    	<cfdefaultcase>
		    		<input type="#Attributes.type#" <cfif Attributes.readonly>readonly</cfif> value="#attributes.value#" class="#cls# #Attributes.class#" name="#Attributes.name#" id="#Attributes.id#"
				    	<cfif cf_xform.useplaceholder>placeholder="#Attributes.label#"</cfif>
				    	<cfif Attributes.required>required </cfif>
				    	<cfif val(Attributes.min)>minlength="#Attributes.min#"</cfif>
				    	<cfif val(Attributes.max)>maxlength="#Attributes.max#"</cfif>
		    		/>
		    	</cfdefaultcase>
		    </cfswitch>

		    <cfif Attributes.help neq "">
		        <small class="help-block">#Attributes.help#</small>
		    </cfif>


	</div>

<cfswitch expression="#attributes.type#">

	<cfcase value="date,datetime" delimiters=",">
		<script type="text/javascript">
		    $('##dtp#Attributes.id#').datetimepicker({
		    	<cfif attributes.type eq "date">

		    	<cfelse>

		    	</cfif>
		        <cfif attributes.startDate != "">
		        	startDate : '#attributes.startDate#',
		        </cfif>
		        <cfif attributes.endDate != "">
		        	endDate : '#attributes.endDate#',
		        </cfif>
		        todayBtn:  1,
				autoclose: true,
				todayHighlight: 1,
				startView: #Attributes.startView#,
				forceParse: 0,
		        showMeridian: 1,
		        minView: #Attributes.minView#,
		        pickerPosition:'bottom-left'
		    });
		</script>
	</cfcase>

</cfswitch>

</cfif>
</cfoutput>