<cfoutput>

	<cfif ThisTag.ExecutionMode EQ "Start">

		<cfparam name="Attributes.TagName" type="string" default="comment"/>
		<cfparam name="Attributes.modelid" type="numeric"/>
		<cfparam name="Attributes.order" type="string" default="asc"/><!--- asc, desc ---->
		<cfparam name="Attributes.key" type="numeric"/>
		<cfparam name="Attributes.title" type="string" default="Comments"/>
		<cfparam name="Attributes.redirectURL" type="string" default="#url.current_page_url#"/>
		<cfparam name="Attributes.copy" type="string" default=""/>
		<cfparam name="Attributes.showcopy" type="boolean" default="true"/>
		<cfparam name="Attributes.commentrequired" type="boolean" default="true"/>
		<cfparam name="Attributes.subject" type="string"/>
		<cfparam name="Attributes.project" type="string"/><!--- this use to know where the modal is in your project where u are making use of the modal --->
		<!--- subject of the mater --- e.g. Quote 1231 --->
		<!--- TODO - escape special character --->

		<cfparam name="Attributes.sendMail" type="boolean" default="true"/>
		<cfif Attributes.sendMail>
			<cfparam name="Attributes.to" type="string" default/><!--- who to send the comment to --->
		</cfif>

		<cfset objCmt = createobject('component','#Attributes.project#.models.plugin.Comment').init().buildRelationships()/>
		<cfset comments = objCmt.findParentComment(Attributes.key, Attributes.modelid, Attributes.order)/>

		<h2><i class="fa fa-comments-o orange"></i> #Attributes.title#</h2>

		<ul class="media-list">
			<cfloop query="comments">

				<li class="media">
					<a class="pull-left">
						<img class="media-object img-circle" src="https://www.gravatar.com/avatar/#lcase(hash(comments.postedBy_PersonalEmail))#?s=40&d=mm"/>
					</a>
					<div class="media-body">
						<h4 class="media-heading">
							<a>#comments.postedBy_Surname# #left(comments.postedBy_OtherNames,1)#.</a>
							<small>#DateFormat1(comments.Created)#</small>
						</h4>
						<p>
							#replaceCarriageReturn(comments.Comment)#</p>
							<cfif comments.cc neq "">
								<div>
									copy:
									<cfloop list="#comments.cc#" item="_cc" index="i">
										<cfif i lt 7>
											<span class="label label-default" style="line-height:20px;background-color:##efe8e8;">#_cc#</span>
										<cfelse>
											<span class="label label-default" style="line-height:20px;background-color:##efe8e8;">...</span>
											<cfbreak>
										</cfif>
									</cfloop>
								</div>
							</cfif>
						</p>
						<cfif len(trim(comments.Files))>
							<i class="fa fa-paperclip"></i>
							<small>
								<cfset fname = listlast(comments.Files,'/')/>
								<cfset url_fname = urlEncodedFormat(fname)/>
								<cfset floc = left(comments.Files,abs(len(comments.Files)-len(fname)))/>
								<a target="_blank" href="attachment/comment/#floc##url_fname#">...#right(fname,45)#</a>
							</small>
						</cfif>
						<a class="pull-right btn btn-info btn-sm" onclick="showModal('plugin.comment.reply@#comments.CommentId#',{'title':'Reply #comments.postedBy_Surname#\'s Comment','backdrop':'static','param':'redirecturl=#Attributes.redirectURL#&subject=#Attributes.subject#'})">
							<!--- TODO: Escape special charaters --->
							<i class="fa fa-reply"></i> Reply
						</a>
					</div>
					<!---- get replies --->
					<cfset commentReplies = objCmt.findCommentReplies(comments.CommentId)/>
					<cfif commentReplies.recordcount>
						<ul>
							<cfloop query="commentReplies">
								<li class="media">
									<a class="pull-left">
										<img class="media-object img-circle" src="https://www.gravatar.com/avatar/#lcase(hash(commentReplies.postedBy_PersonalEmail))#?s=40&d=mm"/>
									</a>
									<div class="media-body">
										<h4 class="media-heading"><a>#commentReplies.postedBy_Surname# #left(commentReplies.postedBy_OtherNames,1)#.</a> <small>#DateFormat1(commentReplies.Created)#</small></h4>
										<p>
											#replaceCarriageReturn(commentReplies.Comment)#
											<cfif commentReplies.cc neq "">
												<div>
													copy:
													<cfloop list="#comments.cc#" item="_cc" index="i">
														<cfif i lt 7>
															<span class="label label-default" style="line-height:20px;background-color:##efe8e8;">#_cc#</span>
														<cfelse>
															<span class="label label-default" style="line-height:20px;background-color:##efe8e8;">...</span>
															<cfbreak>
														</cfif>
													</cfloop>
												</div>
											</cfif>
										</p>
									</div>
								</li>
							</cfloop>
						</ul>
					</cfif>

				</li>

			</cfloop>
			<!--- post your comment --->
			<li class="media">
				<a class="pull-left">
					<img class="media-object img-circle" src="https://www.gravatar.com/avatar/#lcase(hash(request.user.personalemail))#?s=40&d=mm"/>
				</a>
				<cfset form_id = getRandomVariable()/>
				<form id="#form_id#" action="controllers/plugin/comment.cfc?method=Post" enctype="multipart/form-data" method="post">
					<input type="hidden" name="Key" value="#Attributes.Key#"/>
					<input type="hidden" name="redirectURL" value="#Attributes.redirectURL#"/>
					<input type="hidden" name="ModelId" value="#Attributes.ModelId#"/>
					<input type="hidden" name="PostedByUserId" value="#request.user.userid#"/>
					<input type="hidden" name="Subject" value="#attributes.subject#"/>
					<cfif Attributes.sendMail>
						<input type="hidden" name="to" value="#attributes.to#"/>
						<input type="hidden" name="Copy" value="#attributes.copy#" default=""/>
					</cfif>
				<div class="media-body">
					<textarea
						class="tag-comment"
						placeholder="Enter your comment here"
						name="comment"
						<cfif attributes.commentrequired>
							required
						</cfif>
						rows="3"></textarea>
					<!---div class="row">
						<div class="col-sm-12">
							attachment
						</div>
					</div--->
					<div class="row">
						<div class="col-sm-8">
							<!---  this should be an auto suggest control {tagging} --->
							<!---input class="form-control" placeholder="CC: (email addresses)" name="cc"/--->
							<cfif Attributes.showcopy>
								<input type="hidden" name="cc" class="form-control" style="margin-top:5px;" value="#Attributes.copy#"/>
							</cfif>
							<input name="docs" type="file" style="margin-top:10px;"/>
							<input type="hidden" name="comment_file_path" value="#application.awaf.attachmentpath#comment/"/>

					 	</div>
					 	<div class="col-sm-4">
					 		<cfset submit_btn = getrandomvariable()/>
					 		<div style="margin-top:5px;">
								<button class="btn btn-primary btn-block" type="submit" id="#submit_btn#"><i class="fa fa-comment"></i> Post</button>
							</div>
						</div>
					</div>
				</div>
				<cfset submit_button = getrandomvariable()/>

				<!--- // get user email // --->
<cfscript>
	public string function getActiveStaffEmail()	{

		local.rt = createobject('component','#Attributes.project#.models.' & application.model.USER).init().getActiveStaffEmail();

		return local.rt;
	}
</cfscript>

				<script type="text/javascript">

					$(document).ready(function() {
						<cfset formid = "$('###form_id#')"/>

						<cfset cmt = "$('###form_id# textarea[name=comment]')"/>

						<cfif attributes.showcopy>
							<cfset cc = "$('###form_id# input[name=cc]')"/>
							#cc#.select2({
								tags:[
									#getActiveStaffEmail()#
								],
							    placeholder: "CC: (email addresses)",
							    allowClear: true
							});
						</cfif>


						#formid#.validate({

		                    submitHandler: function(e) {
		                        var #submit_button# = $('###submit_btn#');
								#submit_button#.addClass('disabled loading');

							  	var fileSelect = $('###form_id# input[name=docs]')[0];

								var files = fileSelect.files;
								var data_ = new FormData(#formid#[0]);

								// Loop through each of the selected files.
								for (var i = 0; i < files.length; i++) {
								  var file = files[i];

								  // Add the file to the request.
								  data_.append('docs', file);
								}


								var xhr = new XMLHttpRequest();

								xhr.open('POST', 'controllers/plugin/comment.cfc?method=Post', true);

								// Set up a handler for when the request finishes.
								xhr.onload = function () {
									#submit_button#.addClass('disabled loading');
									if (xhr.status === 200) {
										loadPage('#Attributes.redirectURL#', {'forcePageReload':true});
									}
									else {
										showError(xhr);
										#submit_button#.removeClass('disabled loading');
									}
								};

								xhr.send( data_);

		                    }
						});
					});
				</script>
				</form>
			</li>
		</ul>

		<style>.tag-comment{width: 100%}</style>
		<script type="text/javascript">
		$(document).ready(function(){
		    $('.tag-comment').autosize({append:''});
		});
		</script>
	</cfif>


</cfoutput>
