<cfoutput>
	<cfif thisTag.ExecutionMode eq 'start'>

        <cfparam name="Attributes.TagName" type="string" default="tab"/>
        <cfparam name="Attributes.iconprefix" type="string" default="fa fa-"/>
        <cfparam name="Attributes.justified" type="boolean" default="true"/>
        <cfparam name="Attributes.stacked" type="boolean" default="false"/>
        <cfparam name="Attributes.position" type="string" default="left"/> <!--- the position of the tab button left, right --->
        <cfparam name="Attributes.id" type="string" default="#getRandomVariable()#"/>
        <cfparam name="Attributes.style" type="string" default=""/>

      	<cfif attributes.stacked>
            <cfparam name="Attributes.padTop" default="true" type="boolean"/>

            <cfparam name="Attributes.padright" default="30px" type="string"/>
            <cfparam name="Attributes.padleft" default="15px" type="string"/>

            <cfparam name="Attributes.menuClass" default="col-sm-2" type="string"/>
            <cfparam name="Attributes.contentClass" default="col-sm-10" type="string"/>
            <!--- disable justified --->
            <cfset Attributes.justified = false/>

            <!---- write the css class needed --->
            <style>
                ###Attributes.id# .xstacked-menu{
                    <cfif Attributes.padTop>
                        padding-top:40px;
                    </cfif>
                    padding-right: #Attributes.padright#;
                    padding-bottom:100px;
                    padding-left:#Attributes.padleft#;
                    /*border-right: 1px solid ##ccc;*/
                    -webkit-box-shadow: inset -8px 0 15px -10px rgba(0, 0, 0, 0.2);
                    box-shadow: inset -8px 0 15px -10px rgba(0, 0, 0, 0.2);
                }
            </style>
      	</cfif>

      	<cfset request.tab_item[Attributes.id] = ArrayNew(1)/>

    <cfelse>

        <script type="text/javascript">
            $(document).ready(function() {

               $('a.#Attributes.id#').click(function (e) {

                  e.preventDefault();

                  <cfset a = getRandomVariable()/>
                  <cfset u = getRandomVariable()/>

                  var #a# = $(this), #u# = #a#.attr('data-url');

                  if(#u#!="") {
                     loadPage(#u#, {
                        'renderTo':#a#.attr('data-renderTo'),
                        'changeURL':false,
                        'param': #a#.attr('data-urlparam')});
                  	}
                  #a#.tab('show');
               });

            });

        </script>




        <div class="box-info full" id="#Attributes.id#">

            <cfif attributes.stacked>
                <div class="row">
                    <div class="#Attributes.menuClass# xstacked-menu">
            </cfif>

                        <ul class="nav <cfif attributes.stacked>nav-stacked<cfelse>nav-tabs</cfif> <cfif Attributes.justified>nav-justified</cfif> #Attributes.style#">
                            <cfloop array="#request.tab_item[Attributes.id]#" item="x">

                                    <li <cfif x.IsActive>class="active"</cfif>>
                                        <a href="###x.id#" data-toggle="tab" data-url="#x.url#" data-urlparam="#x.urlparam#" data-renderTo="#x.id#"
                                        class="#Attributes.id# <cfif x.IsActive>act</cfif>"><cfif x.icon neq "fa fa-"><i class="#x.icon#"></i> </cfif>#x.title#</a>
                                    </li>

                            </cfloop>
                        </ul>

            <cfif attributes.stacked>
                    </div>

                    <div class="#Attributes.contentClass# xstacked-content">
            </cfif>

                        <!--- tab content ---->
                        <div class="tab-content #Attributes.style#">
                            <cfloop array="#request.tab_item[Attributes.id]#" item="x" index="j">

                                    <div class="tab-pane min-height-200 <cfif x.IsActive>active</cfif>" id="#x.id#">
                                       #x.Content#
                                    </div>

                            </cfloop>
                        </div>

            <cfif attributes.stacked>
                    </div>

                </div>
            </cfif>


        </div>




    </cfif>
</cfoutput>