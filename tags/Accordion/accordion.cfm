<cfoutput>
	<cfif thisTag.ExecutionMode eq 'start'>

        <cfparam name="Attributes.TagName" type="string" default="Accordion"/>
        <cfparam name="Attributes.iconprefix" type="string" default="fa fa-"/> 
        <cfparam name="Attributes.id" type="string" default="#getRandomVariable()#"/>
        <cfparam name="Attributes.style" type="string" default="default"/>
        <cfparam name="Attributes.class" type="string" default=""/>

        <cfset request.accordion.item = ArrayNew(1)/>

    <cfelse>
 
        <script type="text/javascript">
            $(document).ready(function() { 

                $('a.#Attributes.id#').click(function (e) { 
                    e.preventDefault();
                    
                    <cfset  a = getRandomVariable()/>
                    <cfset  u = getRandomVariable()/>

                    var #a# = $(this), #u# = #a#.attr('data-url'); 
                    
                    if(#u#!="") { 
                        loadPage(#u#, {
                            'renderTo': #a#.attr('aria-controls') + ' div.panel-body',
                            'changeURL':false,
                            'param': #a#.attr('data-urlparam')}
                        );
                    }
                    //$(this).collapse('show');
                });

            });

        </script>

        <div class="panel-group awaf_accordion" id="#Attributes.id#">

            <!--- display the items --->
            <cfloop array="#request.accordion.item#" item="x">

                
                <div class="panel panel-#x.style# #Attributes.class#">

                    <!--- title --->
                    <div class="panel-heading" id="#x.id#H" role="tab">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-url="#x.url#" data-parent="###Attributes.id#" href="###x.id#" aria-expanded="true" aria-controls="#x.id#" class="#Attributes.id# <cfif !x.IsActive>collapsed</cfif>" data-urlparam="#x.urlparam#">
                               <cfif x.icon neq "">
                                   <i class="#Attributes.iconprefix##x.icon#"></i>
                               </cfif> #x.title#
                                
                            </a>
                        </h4>
                    </div>
                

                    <!--- content --->
                    <div id="#x.id#" class="panel-collapse collapse <cfif x.isActive>in</cfif>" role="tabpanel" aria-labelledby="#x.id#H">
                        <div class="panel-body">
                            #x.content#
                        </div>
                    </div>

                </div>

            </cfloop>

        </div>
 


    

    </cfif>
</cfoutput>
